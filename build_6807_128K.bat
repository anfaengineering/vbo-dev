set SHORTVER=V145
set DATESTR=191206
set FORMALSHORTVER=V145
set IT6350_SIZE=128K

if not exist bin_%IT6350_SIZE% mkdir bin_%IT6350_SIZE%
set rootdir=%cd%

cd it6350

:: cmd /c IT6805_IT6350.bat > "%rootdir%\build.log" 

cmd /c IT6807_IT6350.bat %IT6350_SIZE%
if exist rom\IT6807_b.bin move rom\IT6807_b.bin  "%rootdir%\bin_128K\IT6807_%FORMALSHORTVER%_%IT6350_SIZE%.bin"
if exist rom\IT6807_b.bin cd /d "%rootdir%"
goto done

:fail
notepad "%rootdir%\build.log"
cd "%rootdir%"

rem pause;
:done

