#****************************************************************************************
#
#****************************************************************************************

#
# Compiler
#
CC=nds32le-elf-gcc

#
# Assembler
#
AS=nds32le-elf-gcc

#
# Linker
#
Linker=nds32le-elf-gcc

#
# Parameter of compiler
#
CDirectives=-Os -Wall -Wno-strict-aliasing -Wno-uninitialized -Werror -c -fmessage-length=0

#
# LDS file
#
#LDS_NAME=docking_256K.lds
LDS_File=LDS\$(LDS_NAME)

#
# Parameter of linker
#
#LDirectives=-nostartfiles -static -T LDS\docking.lds -Wl,-Map -Wl,Misc\Map\map.txt -Os -o
LDirectives=-nostartfiles -static -T $(LDS_File) -Wl,-Map -Wl,Misc\Map\map.txt -Os -o

#
# LDS file
#
#LDS_File=LDS\docking.lds

#
# OBJ files path
#
OBJ_PATH=Misc\Obj

#//****************************************************************************************
# LIB setting for linker
#//****************************************************************************************
LIB_DIR=-Lcode\lib

LIB_NAME=\

ITE_DEFS=-D__DISABLE_UART_AUTO_CLK_GATING__\
		-DENABLE_IVT_MAP_TO_FLASH\
		$(ITE_IT6805_DEFS)\
		
ITE_IT6805_DEFS=$(ITE_IT6805_DEFS) -D_MCU_IT6350_\

#
# path of all header files
#
All_Include=code\*.h\
  		code\chip\*.h\
 		code\core\*.h\
		code\nds\include\*.h\
  		code\nds\include\*.inc\
   		code\oem\*.h\
   		code\api\*.h\
  		code\api\debug\*.h\
     	code\api\hid\*.h\
   		code\api\i2c\*.h\
   		code\api\osc\*.h\
      	code\api\usb\*.h\
   		code\api\rtos_null\hal.h

ITE_DIR=-Icode\nds\boot\
        -Icode\nds\include\
        -Icode\chip\
        -Icode\core\
        -Icode\api\
        -Icode\api\debug\
        -Icode\api\hid\
        -Icode\api\i2c\
        -Icode\api\osc\
        -Icode\api\usb\
        -Icode\api\rtos_null

#
# ec api obj files
#
OBJ_API_EC=\
	$(OBJ_PATH)\api_gpio.o\
	$(OBJ_PATH)\api_wuc.o\
	$(OBJ_PATH)\api_intc.o\
	$(OBJ_PATH)\api_etwd.o\
	$(OBJ_PATH)\api_pwm.o\
	$(OBJ_PATH)\api_i2c_slave.o	

#
# To link [api_xxx.o] if related api function be used.
#
OBJ_API_EC_Link=\
	$(OBJ_PATH)\api_gpio.o\
	$(OBJ_PATH)\api_wuc.o\
	$(OBJ_PATH)\api_intc.o\
	$(OBJ_PATH)\api_etwd.o\
	$(OBJ_PATH)\api_pwm.o\
	$(OBJ_PATH)\api_i2c_slave.o

# ---------------------------
# $(OBJ_PATH)\api_gpio.o\
# $(OBJ_PATH)\api_wuc.o\
# $(OBJ_PATH)\api_intc.o\
# $(OBJ_PATH)\api_etwd.o\
# $(OBJ_PATH)\api_i2c_slave.o\
# ---------------------------

#
# all IT6634 obj files
#
IT6805Files=\
	$(OBJ_PATH)\IO.o\
	$(OBJ_PATH)\iTE6805_I2C_RDWR.o\
	$(OBJ_PATH)\ITE6805_EDID.o\
	$(OBJ_PATH)\ITE6805_MHL_DRV.o\
	$(OBJ_PATH)\ITE6805_MHL_SYS.o\
	$(OBJ_PATH)\ITE6805_EVB_Debug.o\
        $(OBJ_PATH)\iTE6805_EQ.o\
	$(OBJ_PATH)\iTE6805_DRV.o\
	$(OBJ_PATH)\ITE6805_SYS.o\
 	$(OBJ_PATH)\IO_IT6350.o\
	$(OBJ_PATH)\IT6805_IT6350_Main.o

#IT6615Files=\
#	$(OBJ_PATH)\iTE6615_I2C_RDWR.o\
#	$(OBJ_PATH)\iTE6615_EVB_Debug.o\
#	$(OBJ_PATH)\iTE6615_EDID_PARSER.o\
#	$(OBJ_PATH)\iTE6615_DRV_TX.o\
#	$(OBJ_PATH)\iTE6615_SYS_FLOW.o\

	
#
# all oem obj files
#
OEMFiles=\
#	$(OBJ_PATH)\oem_debug.o\
	$(OBJ_PATH)\oem_main.o\
#	$(OBJ_PATH)\oem_irq.o\
#	$(OBJ_PATH)\oem_ver.o\
#	$(OBJ_PATH)\oem_smbus.o\
#	$(OBJ_PATH)\oem_init.o\
#	$(OBJ_PATH)\oem_i2c.o
	
#	$(OBJ_PATH)\oem_gpio.o\			
#	$(OBJ_PATH)\oem_led.o\	
#	$(OBJ_PATH)\oem_memory.o\
#	$(OBJ_PATH)\oem_timer.o\	



#
# all core obj files
#
COREFiles=\
	$(OBJ_PATH)\core_asm.o\
	$(OBJ_PATH)\core_irq.o\
	$(OBJ_PATH)\core_main.o\
	$(OBJ_PATH)\core_timer.o\
	$(OBJ_PATH)\core_power.o\
	$(OBJ_PATH)\core_smbus.o
	
#	$(OBJ_PATH)\core_gpio.o\
#	$(OBJ_PATH)\core_init.o\	
#	$(OBJ_PATH)\core_memory.o\
	

#
# all core api obj files
#
APIFiles=\
	$(OBJ_PATH)\debug_print.o\
	$(OBJ_PATH)\debug_ns16550.o\
	$(OBJ_PATH)\hid_common.o\
	$(OBJ_PATH)\usb_slave.o\
	$(OBJ_PATH)\hid_hal.o\
	$(OBJ_PATH)\i2c_hid.o\
	$(OBJ_PATH)\i2c.o\
 	$(OBJ_PATH)\i2c_drv.o\
	$(OBJ_PATH)\osc.o\
	$(OBJ_PATH)\hal.o

#
# all nds obj files
#
NDSFiles=\
	$(OBJ_PATH)\crt0.o\
	$(OBJ_PATH)\os_cpu_a.o

#
# all obj files
#

#	 $(IT6615Files)\
OBJS=$(OEMFiles)\
     $(IT6805Files)\
     $(COREFiles)\
     $(NDSFiles)\
     $(APIFiles)\

#****************************************************************************************
# Target : 
#****************************************************************************************
ALL:iteec.bin

#****************************************************************************************
# Hex file to bin file and obj dump.
# Syntax : 
#****************************************************************************************
iteec.bin:iteec.adx
	nds32le-elf-objcopy -S -O binary iteec.adx iteec.bin
	nds32le-elf-nm -n -l -C iteec.adx > misc/map/symbol.txt
	nds32le-elf-readelf -a iteec.adx > misc/map/readelf.txt
	nds32le-elf-objdump -x -d -C iteec.adx > misc/map/objdump.txt
	
#****************************************************************************************
# Link all obj fils
#****************************************************************************************
iteec.adx:$(OBJS)\
        $(OBJ_API_EC)\
        $(LDS_File)
        $(Linker) $(LDirectives) iteec.adx\
        $(OBJS)\
        $(OBJ_API_EC_Link)\
        $(LIB_DIR)\
        $(LIB_NAME)\


$(OBJS):$(All_Include)
$(OBJ_PATH)\usb_slave.o:code\api\hid\usb_slave_hid.c code\api\hid\hid_common_kb.c code\api\hid\hid_common_tp.c code\api\hid\hid_custom_cmd.c code\api\usb\usb_slave.c code\api\usb\usb_slave_const.c
	$(CC) $(CDirectives) $(ITE_DEFS) -o $(OBJ_PATH)\usb_slave.o code\api\usb\usb_slave.c

#****************************************************************************************
# Compile IT6805 file
##****************************************************************************************

{..\IT6805\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_IT6805_DEFS) -o $*.o $<
    
{..\IT6805\IT6805\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_IT6805_DEFS) -o $*.o $<    

#****************************************************************************************
# Compile IT6615 file
##****************************************************************************************

#{..\IT6805\}.c{$(OBJ_PATH)\}.o:
#    $(CC) $(CDirectives) $(ITE_IT6805_DEFS) -o $*.o $<
    

#{..\IT6805\IT6615\}.c{$(OBJ_PATH)\}.o:
#    $(CC) $(CDirectives) $(ITE_IT6805_DEFS) -o $*.o $<    

#****************************************************************************************
# Compile oem file
##****************************************************************************************
    
{code\oem\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) -o $*.o $<

#****************************************************************************************
# Compile core file
#****************************************************************************************
{code\core\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_DEFS) -o $*.o $<
    
#****************************************************************************************
# Compile nds file
#****************************************************************************************
$(OBJ_PATH)\crt0.o:code\nds\boot\crt0.s $(All_Include)
	$(CC) $(CDirectives) $(ITE_DEFS) -o $(OBJ_PATH)\crt0.o code\nds\boot\crt0.S

$(OBJ_PATH)\os_cpu_a.o:code\nds\boot\os_cpu_a.s $(All_Include)
	$(CC) $(CDirectives) $(ITE_DEFS) -o $(OBJ_PATH)\os_cpu_a.o code\nds\boot\os_cpu_a.S

#****************************************************************************************
# Compile api file
#****************************************************************************************
{code\api\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_DEFS) $(ITE_DIR) -o $*.o $<

{code\api\debug\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_DEFS) $(ITE_DIR) -o $*.o $<

{code\api\hid\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_DEFS) $(ITE_DIR) -o $*.o $<

{code\api\i2c\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_DEFS) $(ITE_DIR) -o $*.o $<

{code\api\osc\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_DEFS) $(ITE_DIR) -o $*.o $<

{code\api\rtos_null\}.c{$(OBJ_PATH)\}.o:
    $(CC) $(CDirectives) $(ITE_DEFS) $(ITE_DIR) -o $*.o $<

