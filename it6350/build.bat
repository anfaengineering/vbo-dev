@echo off

REM ***********************************************************************************************************
REM	Setting
REM ***********************************************************************************************************
REM -----------------------------------------------------------------------------------------------------------

IF  "%4" == ""     SET EC_ROM_SIZE=126
IF  "%4" == "256K"     SET EC_ROM_SIZE=254	
IF  "%4" == "128K"     SET EC_ROM_SIZE=126

	if (%NDS32LE%) == () set NDS32LE=..\IT6350_toolchains
	set ITEEC_OLDPATH=%PATH%
        set BUILDOLDPATH=%path%
    path= %NDS32LE%\nds32le-elf-mculib-v3m\bin;.\Tools;.\Tools\NMake;

REM -----------------------------------------------------------------------------------------------------------

REM ***********************************************************************************************************
REM	Parameter
REM ***********************************************************************************************************
REM -----------------------------------------------------------------------------------------------------------
IF  "%1" == ""     GOTO OPTIONS
IF  "%1" == "?"    GOTO OPTIONS
IF  "%1" == "/?"   GOTO OPTIONS

IF  "%1" == "clear"   GOTO clear
IF  "%1" == "CLEAR"   GOTO clear

IF  "%2" == ""	   GOTO BUILD
IF  "%2" == "+bbk"  GOTO BUILD
IF  "%2" == "+BBK"  GOTO BUILD

IF  "%2" == "all"  GOTO BUILDALL
IF  "%2" == "ALL"  GOTO BUILDALL

GOTO OPTIONS


REM -----------------------------------------------------------------------------------------------------------

REM ***********************************************************************************************************
REM	Clean build
REM ***********************************************************************************************************
:BUILDALL
del /q .\Misc\Obj\*.*

REM ***********************************************************************************************************
REM	make file
REM ***********************************************************************************************************
:BUILD
cd Code
cd OEM
if not exist BU1 goto NotExit
copy .\BU1\*.c
copy .\BU1\*.h
cd..
cd..

rem cls
REM ***********************************************************************************************************
REM	Building
REM ***********************************************************************************************************
NMAKE %MAKEFILE%

if errorlevel 1 goto errorend

::-----------------------------------------------------------
:: OK
::-----------------------------------------------------------
del /q .\code\oem\*.c
del /q .\code\oem\*.h
del /q iteec.adx

FU /SIZE %EC_ROM_SIZE% iteec.bin %1.bin FF

::-----------------------------------------------------------
:: To merge bbk bin file
::-----------------------------------------------------------
IF  "%2" == "+bbk"  GOTO mergebbk
IF  "%2" == "+BBK"  GOTO mergebbk
IF  "%3" == "+bbk"  GOTO mergebbk
IF  "%3" == "+BBK"  GOTO mergebbk
GOTO :Nomergebbk

:mergebbk
copy %1.bin bbk_temp.bin
IF  "%4" == ""  fu /ow bbk_temp.bin .\code\bbk\bbk_256k.bin with_bbk.bin 0x800
IF  "%4" == "256K"  fu /ow bbk_temp.bin .\code\bbk\bbk_256k.bin with_bbk.bin 0x800
IF  "%4" == "128K"  fu /ow bbk_temp.bin .\code\bbk\bbk_128k.bin with_bbk.bin 0x800
fu /binmodify with_bbk.bin 0x02 0x04
del /q bbk_temp.bin

::-----------------------------------------------------------
:: To caculate word checksum [start ~ end at location ]
:: checksum :           [start] [end] [word checksum location]
::-----------------------------------------------------------
IF  "%4" == ""  fu /wcsarm with_bbk.bin 2000 3F7FF 3F7FE
IF  "%4" == "256K"  fu /wcsarm with_bbk.bin 2000 3F7FF 3F7FE
IF  "%4" == "128K"  fu /wcsarm with_bbk.bin 2000 1F7FF 1F7FE
IF  "%4" == ""  fu /wcsarm with_bbk.bin 2000 1F7FF 1F7FE
IF  %EC_ROM_SIZE% == 256  fu /wcsarm with_bbk.bin 2000 3F7FF 3F7FE
rename with_bbk.bin %1_b.bin

::-----------------------------------------------------------
:: No merge bbk bin file
::-----------------------------------------------------------
:Nomergebbk
del /q iteec.bin
del /q %1.bin
move *.bin .\rom


ECHO    ********************************************************************
ECHO    *    ITE Embedded Controller Firmware Build Process                *
ECHO    *    Copyright (c) 2006-2010, ITE Tech. Inc. All Rights Reserved.  *
ECHO    ********************************************************************
ECHO.  
ECHO    Making EC bin file successfully !!!
GOTO done

:NotExit
:cls
ECHO    ********************************************************************
ECHO    *    ITE Embedded Controller Firmware Build Process                *
ECHO    *    Copyright (c) 2006-2010, ITE Tech. Inc. All Rights Reserved.  *
ECHO    ********************************************************************
ECHO.  
ECHO    Project folder isn't exit.
cd ..
cd ..
GOTO done


:OPTIONS
rem cls
ECHO    ********************************************************************
ECHO    *    ITE Embedded Controller Firmware Build Process                *
ECHO    *    Copyright (c) 2006-2010, ITE Tech. Inc. All Rights Reserved.  *
ECHO    ********************************************************************
ECHO.    
ECHO    USAGE:  build [P1] [P2] [P3]
ECHO                  P1 = The project name of OEM folder. or [clear]
ECHO                  P2 = [all] [ALL] [+bbk] [+BBK]
ECHO                  P3 = [+bbk] [+BBK]
ECHO.
GOTO done

:clear
del /q .\rom\*.*
del /q .\misc\lst\*.*
del /q .\misc\map\*.*
del /q .\misc\obj\*.*
GOTO done

::-----------------------------------------------------------
:: Fail
::-----------------------------------------------------------
:errorend
del /q .\code\oem\*.c
del /q .\code\oem\*.h

::-----------------------------------------------------------
:: Done
::-----------------------------------------------------------
:done
set path=%BUILDOLDPATH%
@echo on
