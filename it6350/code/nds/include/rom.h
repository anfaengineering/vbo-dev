#ifndef _ROM_H_
#define _ROM_H_
/* ****************************************************************************
 *
 *  Rom.h - for romlib v206.1
 *        -
 *        - Version infomation in 0x7006C ~ 0x7006F
 *
 *  Copyright (c) 2012- ITE TECH. INC.  All rights reserved.
 *
 *  Created on: 2012/12/13
 * 
 * ***************************************************************************/
#include <stddef.h>
#include <nds32_intrinsic.h>

//----------------------------------------------------------------------------
//
//  SPI flash command code
//
//----------------------------------------------------------------------------
#define SPICmd_WRSR             0x01    // Write Status Register
#define SPICmd_BYTEProgram      0x02    // To Program One Data Byte
#define SPICmd_WRDI             0x04    // Write diaable

#define SPICmd_ReadStatus       0x05    // Read Status Register
#define SPIStatus_BUSY          BIT0    // 1 = Internal Write operation is in progressn
#define SPIStatus_WEL           BIT1    // 1 = Device is memory Write enabled
#define SSTSPIStatus_AAI        BIT6    // 1 = AAI programming mode 

#define SPICmd_WREN             0x06    // Write Enable
#define SPICmd_HighSpeedRead    0x0B    // High-Speed Read
#define SPICmd_EWSR             0x50    // Enable Write Status Register
#define SPICmd_RDID             0xAB    // Read ID
#define SPICmd_DeviceID         0x9F    // Manufacture ID command
#define SPICmd_AAIWordProgram   0xAD    // Auto Address Increment Programming (word)
#define SPICmd_AAIProgram       0xAF    // Auto Address Increment Programming

#define SPICmd_SectorErase      0xD7    // ITE eFlash Sector Erase(1K bytes) Command.
#define SPICmd_Erase4KByte      0x20    // Erase 4 KByte block of memory array
#define SPICmd_Erase32KByte     0x52    // Erase 32 KByte block of memory array
#define SPICmd_Erase64KByte     0xD8    // Erase 64 KByte block of memory array

//----------------------------------------------------------------------------
//
//  SPI status setting
//
//----------------------------------------------------------------------------
#define SPIStatus_ProtectAll    0xFC
#define SPIStatus_UnlockAll     0x00
#define SPIStatus_WriteEnable   0x02

//----------------------------------------------------------------------------
//
//  SPI internal/external flash SELECTION
//
//----------------------------------------------------------------------------
#define SPI_selection_internal  0x4F
#define SPI_selection_external  0x0F

//----------------------------------------------------------------------------
//
//  CPU clock selection
//
//----------------------------------------------------------------------------
enum _CPU_CLOCK_SEL_
{
    CPU_CLOCK_8MHz = 1,
    CPU_CLOCK_16MHz,
    CPU_CLOCK_24MHz,
    CPU_CLOCK_32MHz,
    CPU_CLOCK_48MHz,
    CPU_CLOCK_64MHz,
    CPU_CLOCK_72MHz,
    CPU_CLOCK_96MHz
};

//----------------------------------------------------------------------------
//
//  Replace function name
//
//----------------------------------------------------------------------------
//  For libc
//----------------------------------------------------------------------------
#ifndef _STDLIB_H_  // {
#define abs                             std_abs
#endif  // } _STDLIB_H_

#ifndef _STRING_H_  // {
#define strncpy                         std_strncpy
#define memset                          std_memset
#define memcmp                          std_memcmp
#define memcpy(_x_,_y_,_z_)             std_memcpy((void*)(_x_),(void*)(_y_),(unsigned long)(_z_))
#endif  // } _STRING_H_

//----------------------------------------------------------------------------
// Note: Use this for correct UART baudrate
#define cpu_clock_select_ext(mode)                                      \
	do{                                                                 \
		cpu_clock_select(mode);                                         \
		UART1_LCR |= 0x80; /*LCR_BKSE*/                                 \
		if((mode == CPU_CLOCK_8MHz) || (mode == CPU_CLOCK_16MHz))       \
			UART1_SPPR = 0x00;                                          \
		else if((mode == CPU_CLOCK_32MHz) || (mode == CPU_CLOCK_64MHz)) \
			UART1_SPPR = 0x02;                                          \
		else                                                            \
			UART1_SPPR = 0x01;                                          \
		UART1_LCR &= (~0x80);                                           \
	}while(0)

// Note: Use this for reset IMMU
#define immu_reset_ext()                                                \
    do{                                                                 \
        unsigned long ulPSW = (GET_PSW() & (PSW_mskINTL | PSW_mskGIE)); \
        if(PSW_mskGIE == ulPSW)                                         \
            __nds32__gie_dis();                                         \
        /* Enter Critical Section */                                    \
        immu_reset();                                                   \
        /* Exit Critical Section */                                     \
        if(PSW_mskGIE == ulPSW)                                         \
            __nds32__gie_en();                                          \
    }while(0)

//----------------------------------------------------------------------------
//
//  Define function type
//
//----------------------------------------------------------------------------
#ifdef DEF_BUILD_ROM // {
#include "rom_e.h"
#else   // } DEF_BUILD_ROM

typedef char* (*ROM_FUN_PC_PC_CPC_UL)(char*, const char*, unsigned long);
typedef void* (*ROM_FUN_PV_PV_I_UL)(void*, int, unsigned long);
typedef void* (*ROM_FUN_PV_PV_PV_UL)(void*, void*, unsigned long);

typedef int   (*ROM_FUN_I_I)(int);
typedef int   (*ROM_FUN_I_PV_PV_UL)(void*, void*, unsigned long);

typedef void  (*ROM_FUN_V_V)(void);
typedef void  (*ROM_FUN_V_UC)(unsigned char);
typedef void  (*ROM_FUN_V_UC_UC)(unsigned char, unsigned char);
typedef void  (*ROM_FUN_V_UL_UC)(unsigned long, unsigned char);
typedef void  (*ROM_FUN_V_UC_UC_UC)(unsigned char, unsigned char, unsigned char);
typedef void  (*ROM_FUN_V_UC_PUC)(unsigned char, unsigned char*);
typedef void  (*ROM_FUN_V_UC_UC_UL)(unsigned char, unsigned char, unsigned long);
typedef void  (*ROM_FUN_V_UC_UL_PUC_UL)(unsigned char, unsigned long, unsigned char*, unsigned long);

typedef unsigned char  (*ROM_FUN_UC_UC)(unsigned char);

//----------------------------------------------------------------------------
//
//
//  Extern Function
//
//
//----------------------------------------------------------------------------
#define ROM_CODE_BASE                   (0x70000)

//-------------------------------------
//
//  Standard Function
//
//-------------------------------------
//  Prototype:
//      int std_abs(int n)
//
//  Description:
//      Absolute value.
//  
//  Parameters:
//      n: Integral value.
//
//  Return Value:
//      The absolute value of n.
//-------------------------------------
#define std_abs                                     ((ROM_FUN_I_I) ROM_CODE_BASE + 0x00CC)

//-------------------------------------
//  Prototype:
//      char* std_strncpy (char* dst, const char* src, unsigned long num)
//
//  Description:
//      Copy characters from string.
//  
//  Parameters:
//      dst: Pointer to the destination.
//      src: Pointer to the const string.
//      num: Maximum number of characters to be copied from source.
//
//  Return Value:
//      destination is returned.
//-------------------------------------
#define std_strncpy                        ((ROM_FUN_PC_PC_CPC_UL) ROM_CODE_BASE + 0x00D6)

//-------------------------------------
//  Prototype:
//      void* std_memcpy (void* dst, void* src, unsigned long num)
//
//  Description:
//      Copy block of memory.
//  
//  Parameters:
//      dst: Pointer to the destination.
//      src: Pointer to the source.
//      num: Number of bytes to copy.
//
//  Return Value:
//      destination is returned.
//-------------------------------------
#define std_memcpy                          ((ROM_FUN_PV_PV_PV_UL) ROM_CODE_BASE + 0x00F4)

//-------------------------------------
//  Prototype:
//      void* std_memset (void* ptr, int val, unsigned long num)
//
//  Description:
//      Fill block of memory.
//  
//  Parameters:
//      ptr: Pointer to the block of memory to fill.
//      val: Value to be set.
//      num: Number of bytes to be set.
//
//  Return Value:
//      ptr is returned.
//-------------------------------------
#define std_memset                           ((ROM_FUN_PV_PV_I_UL) ROM_CODE_BASE + 0x010A)

//-------------------------------------
//  Prototype:
//      int std_memcmp(void* ptr1, void* ptr2, unsigned long num)
//
//  Description:
//      Compare two blocks of memory.
//  
//  Parameters:
//      ptr1: Pointer to the block of memory to fill.
//      ptr2: Value to be set.
//      num: Number of bytes to be set.
//
//  Return Value:
//      A zero value indicates that the contents of both memory blocks are equal.
//-------------------------------------
#define std_memcmp                           ((ROM_FUN_I_PV_PV_UL) ROM_CODE_BASE + 0x011C)

//-----------------------------------------------------------
//
//  Flash
//
//-------------------------------------
//  Prototype:
//      unsigned char spi_read_status(unsigned char Select)
//
//  Description:
//      Read status (command 0x05).
//  
//  Parameters:
//      Select: Indirect Selection.
//          0x4F: internal flash
//          0x0F: external flash
//
//  Return Value:
//      Flash status.
//-------------------------------------
#define spi_read_status                           ((ROM_FUN_UC_UC) ROM_CODE_BASE + 0x013A)

//-------------------------------------
//  Prototype:
//      void spi_write_status(unsigned char Select, unsigned char WStatus, unsigned char Enable);
//
//  Description:
//      Write status (command 0x01).
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      WStatus: SPI Write Status:
//          Status to be set.
//
//      Enable: ucEnableWriteStatusReg:
//          For SST flash chip (ID: 0xBF), this value must be set to 1
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_write_status                     ((ROM_FUN_V_UC_UC_UC) ROM_CODE_BASE + 0x017A)

//-------------------------------------
//  Prototype:
//      void spi_read_id(unsigned char Select, unsigned char Buf[])
//
//  Description:
//      Read ID (command 0x9F).
//  
//  Parameters: 
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      Buf[]: SPI ID Buffer
//          Pointer to ID buffer.
//
//		Length:
//          Read ID length.
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_read_id                            ((ROM_FUN_V_UC_PUC) ROM_CODE_BASE + 0x021A)

//-------------------------------------
//  Prototype:
//      void spi_read_id_cmd_ab(unsigned char Select, unsigned char Buf[])
//
//  Description:
//      Read ID (command 0xAB).
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      Buf[]: SPI ID Buf
//          Pointer to ID buffer.
//
//		Length:
//          Read ID length.
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_read_id_cmd_ab                     ((ROM_FUN_V_UC_PUC) ROM_CODE_BASE + 0x02A4)

//-------------------------------------
//  Prototype:
//      void spi_erase(unsigned char Select, unsigned char EraseCmd, unsigned long Addr)
//
//  Description:
//      Erase flash.
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      EraseCmd: SPI Flash Erase Cmd
//          0xD7: erase 1k bytes (internal flash only).
//          0x20: erase 4k bytes.
//          0x52: erase 32k bytes.
//          0xD8: erase 64k bytes.
//
//      Addr: SPI Flash Address
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_erase                            ((ROM_FUN_V_UC_UC_UL) ROM_CODE_BASE + 0x0336)

//-------------------------------------
//  Prototype:
//      void spi_write_enable(unsigned char Select, unsigned char Enable)
//
//  Description:
//      Write enable.
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      Enable: Enable Write Status Reg. 
//              For SST flash chip (ID: 0xBF), this value must be set to 1.
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_write_enable                        ((ROM_FUN_V_UC_UC) ROM_CODE_BASE + 0x03CC)

//-------------------------------------
//  Prototype:
//      void spi_write_disable(unsigned char Select)
//
//  Description:
//      Write disable.
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_write_disable                          ((ROM_FUN_V_UC) ROM_CODE_BASE + 0x0464)

//-------------------------------------
//  Prototype:
//      void spi_write_byte(BYTE Select, DWORD Addr, BYTE Buffer[], DWORD ByteCount)
//
//  Description:
//      Write data to flash (byte program).
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      Addr: SPI Flash Address
//
//      Buffer[]: Pointer to data buffer.
//
//      ByteCount: Number of bytes to write.
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_write_byte                   ((ROM_FUN_V_UC_UL_PUC_UL) ROM_CODE_BASE + 0x04DC)

//-------------------------------------
//  Prototype:
//      void spi_write_aai_word(BYTE Select, DWORD Addr, BYTE Buffer[], DWORD ByteCount)
//
//  Description:
//      Write data to flash (AAI word program).
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      Addr: SPI Flash Address
//
//      Buffer[]: Pointer to data buffer.
//
//      ByteCount: Number of bytes to write.
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_write_aai_word               ((ROM_FUN_V_UC_UL_PUC_UL) ROM_CODE_BASE + 0x059A)

//-------------------------------------
//  Prototype:
//      void spi_write_aai(BYTE Select, DWORD Addr, BYTE Buffer[], DWORD ByteCount);
//
//  Description:
//      Write data to flash (AAI program).
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      Addr: SPI Flash Address
//
//      Buffer[]: Pointer to data buffer.
//
//      ByteCount: Number of bytes to write.
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_write_aai                    ((ROM_FUN_V_UC_UL_PUC_UL) ROM_CODE_BASE + 0x069E)

//-------------------------------------
//  Prototype:
//      void spi_ec_indirect_fast_read(BYTE Select, DWORD Addr, BYTE Buffer[], DWORD ByteCount);
//
//  Description:
//      EC-indirect fast read
//  
//  Parameters:
//      Select: Indirect Selection
//          0x4F: internal flash.
//          0x0F: external flash.
//
//      Addr: SPI Flash Address
//
//      Buffer[]: Pointer to data buffer.
//
//      ByteCount: Number of bytes to write.
//
//  Return Value:
//      None.
//-------------------------------------
#define spi_ec_indirect_fast_read        ((ROM_FUN_V_UC_UL_PUC_UL) ROM_CODE_BASE + 0x078A)

//  
//  eFlash Control
//
//-------------------------------------
//  Prototype:
//      void eflash_to_direct_map(unsigned long Addr, unsigned char Index);
//
//  Description:
//      Copy code from embedded flash to direct map SRAM.
//  
//  Parameters:
//      Addr: SPI Flash Address
//
//      Index: SRAM segment index (0~12).
//
//  Return Value:
//      None.
//-------------------------------------
#define eflash_to_direct_map                    ((ROM_FUN_V_UL_UC) ROM_CODE_BASE + 0x082C)

//-------------------------------------
//  Prototype:
//      void eflash_rescan_signature(void);
//
//  Description:
//      Rescan signature.
//  
//  Parameters:
//      None.
//
//  Return Value:
//      None.
//-------------------------------------
#define eflash_rescan_signature                     ((ROM_FUN_V_V) ROM_CODE_BASE + 0x08EE)

//
//  Direct Map
//
//-------------------------------------
//  Prototype:
//      void dm_set_vma_address(unsigned long Addr, unsigned char Index);
//
//  Description:
//      Set VMA address of the direct map segment.
//  
//  Parameters:
//      Addr: Virtual address.(CPU view)
//
//      Index: SRAM segment index (0~12).
//
//  Return Value:
//      None.
//-------------------------------------
#define dm_set_vma_address                      ((ROM_FUN_V_UL_UC) ROM_CODE_BASE + 0x0A1A)

//-------------------------------------
//  Prototype:
//      void dm_set_segment_valid(unsigned char Index);
//
//  Description:
//      Enable direct map segment.
//  
//  Parameters:
//      Index: SRAM segment index (0~12).
//
//  Return Value:
//      None.
//-------------------------------------
#define dm_set_segment_valid                       ((ROM_FUN_V_UC) ROM_CODE_BASE + 0x0A80)

//-------------------------------------
//  Prototype:
//      void dm_set_segment_invalid(unsigned char Index)
//
//  Description:
//      Disable direct map segment.
//  
//  Parameters:
//      Index: SRAM segment index (0~12).
//
//  Return Value:
//      None.
//-------------------------------------
#define dm_set_segment_invalid                     ((ROM_FUN_V_UC) ROM_CODE_BASE + 0x0AB2)

//
//  IMMU
//
//-------------------------------------
//  Prototype:
//      void immu_reset(void)
//
//  Description:
//      IMMU tag SRAM reset.
//  
//  Parameters:
//      None.
//
//  Return Value:
//      None.
//-------------------------------------
#define immu_reset                                  ((ROM_FUN_V_V) ROM_CODE_BASE + 0x0AE4)

//-------------------------------------
//  Prototype:
//      void immu_cache_size_select(unsigned char Param)
//
//  Description:
//      Select IMMU cache size.
//  
//  Parameters:
//      Param: 
//          0: 8K bytes.
//          1: 4K bytes.
//
//  Return Value:
//      None.
//-------------------------------------
#define immu_cache_size_select                     ((ROM_FUN_V_UC) ROM_CODE_BASE + 0x0B2C)

//-------------------------------------
//  Prototype:
//      void immu_cache_dma_burst_length_select(unsigned char Param)
//
//  Description:
//      Select IMMU cache DMA Burst Length.
//  
//  Parameters:
//      Param: 
//          0: 16 bytes.
//          1: 32 bytes.
//
//  Return Value:
//      None.
//-------------------------------------
#define immu_cache_dma_burst_length_select         ((ROM_FUN_V_UC) ROM_CODE_BASE + 0x0B00)

//-------------------------------------
//  Prototype:
//      unsigned char cpu_clock_select(unsigned char Mode);
//
//  Description:
//      Select CPU clock.
//  
//  Parameters:
//      Mode: 
//          CPU_CLOCK_8MHz  = 1
//          CPU_CLOCK_16MHz = 2
//          CPU_CLOCK_24MHz = 3
//          CPU_CLOCK_32MHz = 4
//          CPU_CLOCK_48MHz = 5
//          CPU_CLOCK_64MHz = 6
//          CPU_CLOCK_72MHz = 7
//          CPU_CLOCK_96MHz = 8
//
//  Return Value:
//      Mode is returned.
//-------------------------------------
#define cpu_clock_select                          ((ROM_FUN_UC_UC) ROM_CODE_BASE + 0x0E80)

#endif // } ! DEF_BUILD_ROM
//----------------------------------------------------------------------------
#endif //_ROM_H_
