#ifndef __MEM_LAYOUT_H__ // {
#define __MEM_LAYOUT_H__
// **************************************************************************
// mem_layout.h
// 
// Define memory layout in *.lds.
//
// Copyright (c) ITE, All Rights Reserved
//
// Author      : Jyunlin, Chen
// Created on  : 2012/10/19
// **************************************************************************

// **************************************************************************
//  External Symbol
// **************************************************************************
extern char __rom_code_start, __vma_data_start, _vma_dm_s, _dm_size;//, _lma_dm_s;

// **************************************************************************
//  Macro define
// **************************************************************************
#define GET_ROM_ADDR            ((unsigned long)&__rom_code_start)
#define GET_DATA_VMA_ADDR       ((unsigned long)&__vma_data_start)

#define GET_DM_S_VMA            ((unsigned long)&_vma_dm_s)
#define GET_DM_S_LMA            ((unsigned long)&_vma_dm_s)//((unsigned long)&_lma_dm_s)
#define GET_DM_SIZE             ((unsigned long)&_dm_size)

// **************************************************************************
//  Direct Map Section Define
// **************************************************************************
// RTOS API Section
#define RTOS_CODE_H             __attribute__ ((section (".RTOS_CODE")))
#define RTOS_CODE_L             __attribute__ ((section (".RTOS_CODE_LOW")))
#define RTOS_DATA               __attribute__ ((section (".RTOS_DATA")))        // Read-Write Data


// OSC API Section
#define OSC_CODE                __attribute__ ((section (".OSC_CODE")))

// Sensor API Section define
#define SENSOR_CODE             __attribute__ ((section (".SENSOR_CODE")))
#define SENSOR_DATA             __attribute__ ((section (".SENSOR_DATA")))

// HID API Section define
#define HID_CODE                __attribute__ ((section (".HID_CODE")))
#define HID_DATA                __attribute__ ((section (".HID_DATA")))

// USB API Section define
#define USB_CODE                __attribute__ ((section (".HID_CODE")))

// HID API Section define
#define I2C_CODE                __attribute__ ((section (".HID_CODE")))


#define I2C_DMA_ADD                __attribute__ ((section (".I2C_DMA_ADD")))
// **************************************************************************
#endif	// __MEM_LAYOUT_H__
