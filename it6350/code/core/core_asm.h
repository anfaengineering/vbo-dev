/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_asm.h
 * Dino Li
 * ****************************************************************************
 */

#ifndef CORE_ASM_H
#define CORE_ASM_H

/**
 * ****************************************************************************
 * insert nop instruction
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
static inline void _nop_(void)
{
    __asm__ volatile 
    (
		"nop"
    );
}

/**
 * ****************************************************************************
 * reset stack pointer
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
static inline void _init_sp_(void)
{
    __asm__ volatile 
    (
        /* ec only */
        "la	$sp, _stack_ec"
    );
}
#endif

