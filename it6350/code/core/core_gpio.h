/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_gpio.h
 * Dino Li
 * ****************************************************************************
 */

#ifndef CORE_GPIO_H
#define CORE_GPIO_H

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define ALT       		0x00
#define INPUT        	0x80
#define OUTPUT     		0x40
#define PULL_UP       	0x04
#define PULL_DW        	0x02
#define MaxD    		0x38

#define NullPin         INPUT

#endif

