/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_smbus.c
 * Dino Li
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

#if USING_SMBUS
/* 
 * ****************************************************************************
 * SMBus registers table
 * ****************************************************************************
 */
const sSMBus asSMBus[]=
{
	{ &HOCTL_A, &TRASLA_A, &HOCMD_A, &HOSTA_A, &D0REG_A, &D1REG_A,	&HOBDB_A,
        &IER1,	&ISR1,  Int_SMBUS0,   &PECERC_A},
	{ &HOCTL_B, &TRASLA_B, &HOCMD_B, &HOSTA_B, &D0REG_B, &D1REG_B,	&HOBDB_B,
        &IER1,	&ISR1,  Int_SMBUS1,   &PECERC_B},
	{ &HOCTL_C, &TRASLA_C, &HOCMD_C, &HOSTA_C, &D0REG_C, &D1REG_C,	&HOBDB_C,
        &IER2,	&ISR2,	Int_SMBUS2,   &PECERC_C},
};

const sResetSMBusS asResetSMBusS[]=
{
	{ &HOSTA_A, &HOCTL2_A, &SMBPCTL_A, &GPCRB3, &GPCRB4, &GPDRB,
        (BIT3+BIT4),   &SMBus1RecoverCunt },
	{ &HOSTA_B, &HOCTL2_B, &SMBPCTL_B, &GPCRC1, &GPCRC2, &GPDRC,
        (BIT1+BIT2),   &SMBus2RecoverCunt },
	{ &HOSTA_C, &HOCTL2_C, &SMBPCTL_C, &GPCRF6, &GPCRF7, &GPDRF,
        (BIT6+BIT7),   &SMBus3RecoverCunt },
}; 

/**
 * ****************************************************************************
 * short delay for smbus reset
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void DelayInact(void) 
{
    BYTE index;
    BYTE counter;
   for(index=0;index<200;index++)
   {
        counter++;
   }
}

/**
 * ****************************************************************************
 * The function of reseting SMbus 
 *
 * @return
 *
 * @parameter
 * Channel, 0, 1, or 2
 *
 * ****************************************************************************
 */
void ResetSMBus(BYTE Channel)
{
    SET_MASK(*asSMBus[Channel].SMBusCTL,HOCTL_KILL);
    CLEAR_MASK(*asSMBus[Channel].SMBusCTL,HOCTL_KILL);

    /* clear bits */
	*asResetSMBusS[Channel].SMBusSTA = 0xFE;

    /* SMCLK2 is located on GPC7 */
	if(CheckSMCLK2PinSwitch(Channel)==0x01)
	{
        GPCRC7 = OUTPUT;
	}
    else
    /* SMCLK2 is located on GPF6 */
    {
        *asResetSMBusS[Channel].SMBusPin0 = OUTPUT;
    }
	*asResetSMBusS[Channel].SMBusPin1 = OUTPUT;
    
	DelayInact();

    /* SMCLK2 is located on GPC7 */
    if(CheckSMCLK2PinSwitch(Channel)==0x01)
	{ 
        SET_MASK(GPDRC, BIT7);  /* clock pin */
        SET_MASK(GPDRF, BIT7);  /* data pin */
    }
    else
    /* SMCLK2 is located on GPF6 */
    {
        *asResetSMBusS[Channel].GPIOReg |= asResetSMBusS[Channel].GPIOData;
    }
	DelayInact();

    /* SMCLK2 is located on GPC7 */
    if(CheckSMCLK2PinSwitch(Channel)==0x01)
	{ 
        GPCRC7 = ALT;
    }
    /* SMCLK2 is located on GPF6 */
    else
    {
        *asResetSMBusS[Channel].SMBusPin0 = ALT;
    }
	*asResetSMBusS[Channel].SMBusPin1 = ALT;

	*asResetSMBusS[Channel].SMBusCTL2 = 0x02;
	*asResetSMBusS[Channel].SMBusPinCTL = 0x03;
	DelayInact();
	*asResetSMBusS[Channel].SMBusPinCTL = 0x07;
	*asResetSMBusS[Channel].SMBusCTL2 = 0x11;

#if 0  // 1M	
	SMB4P7USL =0x04;//0x04;//0x07;
	SMB4P0USH =0x00;//0x00;// 0x03;
	SMB300NS = 0x02;
	SMB250NS = 0x01;
	SMB25MS = 0x19;
	SMB45P3USL = 0xFF;
	SMB45P3USH = 0x03;
	SMB4P7A4P0H = 0x00;

	SCLKTS_A = 0x00;
	SCLKTS_C = 0x04;
#endif
#if 0  // 400K	
	SMB4P7USL =0x16;//0x04;//0x07;
	SMB4P0USH =0x11;//0x00;// 0x03;
	SMB300NS = 0x08;
	SMB250NS = 0x08;
	SMB25MS = 0x19;
	SMB45P3USL = 0xFF;
	SMB45P3USH = 0x03;
	SMB4P7A4P0H = 0x00;

	SCLKTS_A = 0x00;
	SCLKTS_C = 0x00;
#endif	
#if 1  // 100K	
	SMB4P7USL =0x72;//0x04;//0x07;
	SMB4P0USH =0x68;//0x00;// 0x03;
	SMB300NS = 0x08;
	SMB250NS = 0x08;
	SMB25MS = 0x19;
	SMB45P3USL = 0xFF;
	SMB45P3USH = 0x03;
	SMB4P7A4P0H = 0x00;

	SCLKTS_A = 0x00;
	SCLKTS_C = 0x00;
#endif	
}

/**
 * ****************************************************************************
 * SMbus read byte/word and write byte/word function	 
 *
 * @return
 * 1, OK. 0, fail.
 *
 * @parameter
 * Channel, 0, 1, or 2
 * Protocol
 * Addr
 * Comd
 * *Var
 * PECSupport
 *
 * ****************************************************************************
 */
BYTE bRWSMBus(
    BYTE Channel,BYTE Protocol,BYTE Addr,BYTE Comd,BYTE *Var,BYTE PECSupport)
{	
	BYTE error;
    BYTE status;
    BYTE resutl;

    if(CheckSMBusInterfaceCanbeUse(Channel, SMBus_AccessType_Fucn)==
        SMBus_CanNotUse)
    {
        /* SMBus interface can't be used. */
        resutl = FALSE;
    }
    else
    {
        /* Pre-set error */
        error = 0xEE;

        /* Pre-set result is fail */
        resutl = FALSE;

        /* Clear CRC variable */
        SMBCRC8_A=0x00;

        /* Write byte function */
	    if(Protocol&0x80)
	    {	

            /* set address with writing bit */
		    *asSMBus[Channel].SMBusADR = Addr;

            /* Set command */
	        *asSMBus[Channel].SMBusCMD=Comd;
		    Protocol&=(~0x80);

            /* set data1 */
		    *asSMBus[Channel].SMBusData0 = *Var;

            /* write word function */
		    if(Protocol==SMbusRW)
		    {

                /* set data2 */
			    *asSMBus[Channel].SMBusData1 = *(Var+0x01);
		    }
	    }
        /* Read function */
	    else
	    {
            /* set address with reading bit */
		    *asSMBus[Channel].SMBusADR=Addr|0x01;

	        /* Set command */
            *asSMBus[Channel].SMBusCMD=Comd;
	    }

	    /* clear bits */
	    *asSMBus[Channel].SMBusSTA=0xFE;

	        /* Start transaction */
	        *asSMBus[Channel].SMBusCTL=(Protocol|HOCTL_SRT);

        /* To enable 26ms time-out timer */
        Enable_ETimer_T(26);
	    while(1)
	    {
            if(Check_ETimer_T_Overflow()==ExTimerNoOverflow)
            {
                /* Read SMBus Host Status */
                status = *asSMBus[Channel].SMBusSTA;
                if(IS_MASK_SET(status,
                    (HOSTA_FINTR+HOSTA_DVER+HOSTA_BSER+
                    HOSTA_FAIL+HOSTA_NACK+HOSTA_TMOE )))
                {
                    if(IS_MASK_SET(status,
                        (HOSTA_DVER+HOSTA_BSER+HOSTA_FAIL+
                        HOSTA_NACK+HOSTA_TMOE)))
                    {
                        if(IS_MASK_SET(status, (HOSTA_BSER+HOSTA_FAIL)))
                        {
                            ResetSMBus(Channel);
                        }
                        error = 0xEE;
                    }
                    else
                    {
                        /* Only Finish Interrupt bit is set. */
                        error = 0x00;
                    }
                    break;
                }
            }
            else
            {
                ResetSMBus(Channel);
                error = 0xEE;
                break;
            }
	    }

        Stop_ETimer_T();

        /* Fail */
	    if(error == 0xEE)
	    {
		    resutl = FALSE;
	    }
        /* OK */
        else
        {
	        if (((*asSMBus[Channel].SMBusADR & 0x01) != 0x00)) 
	        {
                /* read data1 */
		        *Var = *asSMBus[Channel].SMBusData0;
		        if(Protocol==SMbusRW)
		        {
		            /* read data2 */
			        *(Var+0x01) = *asSMBus[Channel].SMBusData1;
		        }
	        }
            resutl = TRUE;
        }

        /* clear bits */
	    *asSMBus[Channel].SMBusSTA=0xFE;
    }
    
	return(resutl);
}

/**
 * ****************************************************************************
 * Write SMbus block function	 
 *
 * @return
 * 1, OK. 0, fail.
 *
 * @parameter
 * Channel, 0, 1, or 2
 * Protocol
 * Addr
 * Comd
 * *Var
 * ByteCont
 * PECsupport
 *
 * ****************************************************************************
 */
BYTE bWSMBusBlock(
    BYTE Channel,
    BYTE Protocol,
    BYTE Addr,
    BYTE Comd,
    BYTE *Var,
    BYTE ByteCont,
    BYTE PECsupport,
    BYTE I2C_EN    )
{	
	BYTE ack;
    BYTE BCTemp;
    BYTE status;

    if(CheckSMBusInterfaceCanbeUse(Channel, SMBus_AccessType_Fucn)==
        SMBus_CanNotUse)
    {
        /* SMBus interface can't be used. */
        ack = FALSE;
    }
    else
    {
        /* pre-set flag */
	    ack = FALSE;

        /* clear byte count */
	    BCTemp = 0x00;
  
	    Protocol&=(~0x80);
	if(I2C_EN)
	{
	 	*asResetSMBusS[Channel].SMBusCTL2 = 0x13;
	}
	else
	{
	 	*asResetSMBusS[Channel].SMBusCTL2 = 0x11;
	}
	    /* set address with writing flag */
	    *asSMBus[Channel].SMBusADR=Addr;

	    /* Set command */
        *asSMBus[Channel].SMBusCMD=Comd;

        /* set byte count */
	if(I2C_EN)
	{		
        	*asSMBus[Channel].SMBusData0=ByteCont;
	}
        /* sync byte count */
        BCTemp=ByteCont;
	    /* set first data */
        *asSMBus[Channel].SMBusBData=*Var;

	    /* clear bits */
        *asSMBus[Channel].SMBusSTA=0xFE;

            /* Start transaction */
            *asSMBus[Channel].SMBusCTL=(Protocol|HOCTL_SRT);


        /* To enable 26ms time-out timer */
        Enable_ETimer_T(26);

        while(1)
        {
            if(Check_ETimer_T_Overflow()==ExTimerNoOverflow)
            {
                /* Read SMBus Host Status */
                status = *asSMBus[Channel].SMBusSTA;
                if(IS_MASK_SET(status,
                    (HOSTA_BDS+HOSTA_DVER+HOSTA_BSER+HOSTA_FAIL+
                    HOSTA_NACK+HOSTA_TMOE+HOSTA_FINTR )))
                {
                    if(IS_MASK_SET(status,
                        (HOSTA_DVER+HOSTA_BSER+HOSTA_FAIL+
                        HOSTA_NACK+HOSTA_TMOE)))
                    {
                        if(IS_MASK_SET(status, (HOSTA_BSER+HOSTA_FAIL)))
                        {
                            ResetSMBus(Channel);
                        }
    	                ack = FALSE;
                        break;
                    }
                    /* Byte Done Status bit or finish flag is set. */
                    else
                    {
                        /* byte done */
                        if(IS_MASK_SET(status, HOSTA_BDS))
                        {
        	                BCTemp--;
    					    /* point to next address of variable */
        	                Var++;

        	                if(BCTemp != 0x00)
        	                {
        		                    *asSMBus[Channel].SMBusBData=*Var;
        	                }
                            /* write block transaction done */
                            else
                            {

                            }
                            /* clear bits for next byte */
                            *asSMBus[Channel].SMBusSTA=0xFC;
                        }
                        else
                        {
                            /* finish */
        	                ack = TRUE;
        	                break;
                        }
                    }
                }
            }
            else
            {
		        ResetSMBus(Channel);
		        ack = FALSE;
                break;
            }
        }
        
        Stop_ETimer_T();
        /* clear bits */
	    *asSMBus[Channel].SMBusSTA=0xFE;
    }
    
	return(ack);
} 

/**
 * ****************************************************************************
 * The function of SMbus send byte	 
 *
 * @return
 * 1, OK. 0, fail.
 *
 * @parameter
 * Channel, 0, 1, or 2
 * Addr
 * SData
 *
 * ****************************************************************************
 */
BYTE bSMBusSendByte(BYTE Channel,BYTE Addr,BYTE SData)
{
	BYTE error;
    BYTE result;
    BYTE status;

    if(CheckSMBusInterfaceCanbeUse(Channel, SMBus_AccessType_Fucn)==
        SMBus_CanNotUse)
    {
        /* SMBus interface can't be used. */
        result = FALSE;
    }
    else
    {
        /* Pre-set error */
        error = 0xEE;

        /* Pre-set result is fail */
        result = FALSE;

        /* set address with writing bit */
	    *asSMBus[Channel].SMBusADR = Addr;

        /* Set command */
	    *asSMBus[Channel].SMBusCMD = SData;

        /* clear bits */
	    *asSMBus[Channel].SMBusSTA = 0xFE;

        /* Start transaction */
	    *asSMBus[Channel].SMBusCTL = (0x04|HOCTL_SRT);

        /* To enable 26ms time-out timer */
        Enable_ETimer_T(26);

        while(1)
	    { 
            if(Check_ETimer_T_Overflow()==ExTimerNoOverflow)
            {
                /* Read SMBus Host Status */
                status = *asSMBus[Channel].SMBusSTA;
                if(IS_MASK_SET(status,
                    (HOSTA_FINTR+HOSTA_DVER+HOSTA_BSER+
                    HOSTA_FAIL+HOSTA_NACK+HOSTA_TMOE )))
                {
                    if(IS_MASK_SET(status,
                        (HOSTA_DVER+HOSTA_BSER+HOSTA_FAIL+
                        HOSTA_NACK+HOSTA_TMOE)))
                    {
                        if(IS_MASK_SET(status, (HOSTA_BSER+HOSTA_FAIL)))
                        {
                            ResetSMBus(Channel);
                        }
                        error = 0xEE;
                    }
                    /* Only Finish Interrupt bit is set. */
                    else
                    {
                        error = 0x00;
                    }
                    break;
                }
            }
            /* time-out */
            else
            {
                ResetSMBus(Channel);
                error = 0xEE;
                break;
            }
	    }

        Stop_ETimer_T();
    
	    if(error == 0xEE)
	    {
		    result = FALSE;
	    }
        else
        {
            result = TRUE;
        }

	    /* clear bits */
	    *asSMBus[Channel].SMBusSTA=0xFE;
    }
    
	return(result);
}

/**
 * ****************************************************************************
 * The function of Checking SMbus clock and data pins are both high	
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BYTE CheckSMBusFree(BYTE channel)
{
    BYTE pinstatus;

    pinstatus = *asResetSMBusS[channel].SMBusPinCTL;

    /* Check BIT0 and BIT1 */
    if((pinstatus&=0x03)==0x03)
    {
        return(SMBus_Free);
    }
    else
    {
        return(SMBus_Busy);
    }
}

/**
 * ****************************************************************************
 * The function of Checking SMbus pins are all alt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BYTE CheckSMBusInterfaceIsALT(BYTE channel)
{
    /* SMCLK2 is located on GPC7 */
    if(CheckSMCLK2PinSwitch(channel)==0x01)
    {
        if(((GPCRC7 & 0xFB)!=0x00)||((GPCRF7 & 0xFB)!=0x00))
        {
            return(SMBusPin_GPIO);
        }
        else
        {
            return(SMBusPin_ALT);
        }
    }
    else
    {
        if(((*asResetSMBusS[channel].SMBusPin0 & 0xFB)!=0x00)||
            ((*asResetSMBusS[channel].SMBusPin1 & 0xFB)!=0x00))
        {
            return(SMBusPin_GPIO);
        }
        else
        {
            return(SMBusPin_ALT);
        }
    }
}

/**
 * ****************************************************************************
 * The function of Checking SMbus pins are all alt
 *
 * @return
 * SMBus_CanUse or SMBus_CanNotUse
 *
 * @parameter
 * channel
 * accesstype
 *
 * ****************************************************************************
 */
BYTE CheckSMBusInterfaceCanbeUse(BYTE channel, BYTE accesstype)
{
    BYTE checksmbus;

    /* Pre-set smbus interface can be used */
    checksmbus = SMBus_CanUse;

    /* SMBus interface is GPIO function */
    if(CheckSMBusInterfaceIsALT(channel)==SMBusPin_GPIO)
    {
        /* Interface can't be used */
        checksmbus = SMBus_CanNotUse;
    }
    /* SMBus interface is ALT function */
    else
    {
        if(accesstype==SMBus_AccessType_Fucn)
        {
            #ifdef SMBusServiceCenterFunc
            /* Check smbus is in used or not. */
            WaitSMBusCHxFree(channel);
            #endif
        }

        if(CheckSMBusFree(channel)==SMBus_Busy)
        {
            /* Interface can't be used */
            checksmbus = SMBus_CanNotUse;
            if( (*asResetSMBusS[channel].recoverCunt)++ >SMBus_BusyRecover)
            {
                ResetSMBus(channel);
            }
        }
        else
        {
            *asResetSMBusS[channel].recoverCunt=0x00;
            /* smbus interface can be used */
            checksmbus = SMBus_CanUse;
        }
    }

    /* return result */
    return(checksmbus);
}

/**
 * ****************************************************************************
 * To init. SMBus setting
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Core_Init_SMBus(void)
{
    /*
     * Reset SMBus interface if bus clock pin or data pin is low greater than
     * SMBus_BusyRecover times.
     */
    SMBus_BusyRecover = 20;
}

/**
 * ****************************************************************************
 * To check SMCLK2 pin switch
 *
 * @return
 * 00h, SMCLK2 is located on GPF6.
 * 01h, SMCLK2 is located on GPC7.
 *
 * @parameter
 * p_channel, SMBus channel 0, 1, 2
 *
 * ****************************************************************************
 */
BYTE CheckSMCLK2PinSwitch(BYTE p_channel)
{
    BYTE status;

    /* pre-set SMCLK2 is located on GPF6. */
    status = 0x00;

    /* SMCLK2 Pin Switch (SMCLK2PS) */
    if(IS_MASK_SET(GCR7, BIT7) && (p_channel==0x02))
    {
        /* SMCLK2 is located on GPC7 */
        status = 0x01;
    }
    else
    {
        /* SMCLK2 is located on GPF6 */
        status = 0x00;
    }

    return(status);
}


/**
 * ****************************************************************************
 * I2C-compatible read command 
 *
 * @return 
 * 0, fail 
 * 1, OK 
 * 
 * @parameter 
 * p_channel, i2c channel selection        0 ~ 2 
 * p_address, device address with r/w bit 
 * p_length, read length 
 * 
 *p_data_buffer, data buffer address 
 * P_first_word_is_read_length
 *
 * ****************************************************************************
 */
BYTE bI2C_comp_Rd_Block(BYTE p_channel, BYTE p_address, WORD p_length, BYTE *p_data_buffer, BYTE P_first_word_is_read_length)
{
    	BYTE status;    
	BYTE ack;    
	BYTE l_read_length[2];    
	BYTE l_read_length_index;

	/* pre-set fail. */    
	ack = FALSE;    
	if(P_first_word_is_read_length)    
	{        
		l_read_length_index = 0x02;
	}
	
	/* illegal parameter */

	if(p_length == 0x00)
	{
		ack = FALSE;
	}
	else
	{
		/* to check interface can be used or not */
		if(CheckSMBusInterfaceCanbeUse(p_channel, SMBus_AccessType_Fucn)==SMBus_CanNotUse)
		{
			/* SMBus interface can't be used. */
			ack = FALSE;
		}
		else
		{
			/* I2C enable */
			*asResetSMBusS[p_channel].SMBusCTL2 = 0x13;
			
			/* block read address */
			*asSMBus[p_channel].SMBusADR = (p_address|0x01);

			/* only read one byte */
			if(p_length==0x01)
			{
				/* set last byte flag */
				SET_MASK(*asSMBus[p_channel].SMBusCTL,HOCTL_LABY);
			}

			/* i2c read, Start transaction */
			*asSMBus[p_channel].SMBusCTL = 0x5C;

			/* To enable 26ms time-out timer */
        			Enable_ETimer_T(26);            
			/* no time out */
			while(1)
        			{
          			if(Check_ETimer_T_Overflow()==ExTimerNoOverflow)
		              {
		              	/* Read SMBus Host Status */
					status = *asSMBus[p_channel].SMBusSTA;
					/* any status bit */
					if(IS_MASK_SET(status, (HOSTA_BDS+HOSTA_DVER+HOSTA_BSER+HOSTA_FAIL+HOSTA_NACK+HOSTA_TMOE+HOSTA_FINTR)))
					{
						/* any error bit */
						if(IS_MASK_SET(status, (HOSTA_DVER+HOSTA_BSER+HOSTA_FAIL+HOSTA_NACK+HOSTA_TMOE)))
						{
							/* bus fail or error */
							if(IS_MASK_SET(status, (HOSTA_BSER+HOSTA_FAIL)))
							{
								ResetSMBus(p_channel);
							}
							ack = FALSE;
							break;
						}
						/* Byte Done or finish Status bit is set */
						else
						{
							if(IS_MASK_SET(status,HOSTA_BDS))
							{
								/* read data */
								if(p_length)
								{
									*p_data_buffer = *asSMBus[p_channel].SMBusBData;
									 p_data_buffer++;
									 p_length--;
									 if(P_first_word_is_read_length)
									 {
									 	if(l_read_length_index)
										{
											l_read_length_index--;
											l_read_length[l_read_length_index] = *(p_data_buffer -1);
											if(l_read_length_index==0)
											{
												p_length = l_read_length[1] + (l_read_length[0]*256);
												if(p_length>2)
												{
													p_length-=2;
												}
											}
										}
									}
								} // if(p_length)              
								/* next cycle is last byte */
								if(p_length==0x01)
								{      
									/* set last byte flag */
									SET_MASK(*asSMBus[p_channel].SMBusCTL,HOCTL_LABY); 
								}
								/* clear status bits for next byte */
								*asSMBus[p_channel].SMBusSTA=0xFC;
							}
							else /* finish */
							{
								if(p_length==0)
								{
									/* i2c read success */
									ack = TRUE;
								}
								break;
							}
						}
					}// if(
				} // if(Check_ETimer_T_Overflow
				else
      				{
		    		    	ResetSMBus(p_channel);
		    		    	ack = FALSE;
            				break;
      				}
			}// while(1) 
		}
        		Stop_ETimer_T();
		/* 26ms time-out and no any status bit is set. */
		/* disable I2C mode */
		*asResetSMBusS[p_channel].SMBusCTL2 = 0x11;
		/* w/c status */
		*asSMBus[p_channel].SMBusSTA=0xFE;                
		}
	return(ack);
}
#endif
