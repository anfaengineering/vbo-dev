/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_irq.h
 * Dino Li
 * ****************************************************************************
 */

#ifndef CORE_IRQ_H
#define CORE_IRQ_H

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define HAL_DISABLE_INTERRUPTS		        (0)
#define HAL_ENABLE_INTERRUPTS		        (1)

/* 
 * ****************************************************************************
 * section for LDS
 * ****************************************************************************
 */
#define Section_General_Exception __attribute__ \
                                        ((section ("._General_Exception")))
#define Section_Interrupt_HW0 __attribute__ ((section ("._Interrupt_HW0")))
#define Section_Interrupt_HW1 __attribute__ ((section ("._Interrupt_HW1")))
#define Section_Interrupt_HW2 __attribute__ ((section ("._Interrupt_HW2")))
#define Section_Interrupt_HW3 __attribute__ ((section ("._Interrupt_HW3")))
#define Section_Interrupt_HW4 __attribute__ ((section ("._Interrupt_HW4")))
#define Section_Interrupt_HW5 __attribute__ ((section ("._Interrupt_HW5")))
#define Section_Interrupt_HW6 __attribute__ ((section ("._Interrupt_HW6")))
#define Section_Interrupt_HW7 __attribute__ ((section ("._Interrupt_HW7")))
#define Section_Interrupt_HW8 __attribute__ ((section ("._Interrupt_HW8")))
#define Section_Interrupt_HW9 __attribute__ ((section ("._Interrupt_HW9")))
#define Section_Interrupt_HW10 __attribute__ ((section ("._Interrupt_HW10")))
#define Section_Interrupt_HW11 __attribute__ ((section ("._Interrupt_HW11")))
#define Section_Interrupt_HW12 __attribute__ ((section ("._Interrupt_HW12")))
#define Section_Interrupt_HW13 __attribute__ ((section ("._Interrupt_HW13")))
#define Section_Interrupt_HW14 __attribute__ ((section ("._Interrupt_HW14")))
#define Section_Interrupt_HW15 __attribute__ ((section ("._Interrupt_HW15")))
#define Section_Interrupt_SW0 __attribute__ ((section ("._Interrupt_SW0")))
#define Section_Isr_Int1 __attribute__ ((section ("._Isr_Int1")))
#define Section_Isr_dummy __attribute__ ((section ("._Isr_Int_dummy")))
#define ISR_RODATA __attribute__ ((section ("._ISR_RODATA")))
#define ISR_CODE __attribute__ ((section ("._ISR_CODE")))
#define ISR_CODE_OEM __attribute__ ((section ("._ISR_CODE_OEM")))  


#define _CONST_AC __attribute__ ((section ("._AC_RODATA ")))
#define _CONST_16C __attribute__ ((section ("._16C_RODATA ")))
#define _CONST_3B8 __attribute__ ((section ("._3B8_RODATA ")))
#define _CONST_FW_VERSION __attribute__ ((section ("._OEM_VERSION_RODATA ")))
/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void DisableAllInterrupt(void);
extern void EnableAllInterrupt(void);
extern void InitEnableInterrupt(void);
extern void EnableModuleInterrupt(void);
extern void EnableGlobalInt(void);
extern void DisableGlobalInt(void);
extern void EnableExternalInt(void);
inline unsigned int hal_global_int_ctl(int int_op);

#endif

