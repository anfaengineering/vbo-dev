/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_irq.c
 * Dino Li
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"


void (*IT6350_A0_IRQ)(void) = NULL;
void (*IT6350_A1_IRQ)(void) = NULL;
void (*IT6350_A4_IRQ)(void) = NULL;
void (*IT6350_A5_IRQ)(void) = NULL;
void (*IT6350_A6_IRQ)(void) = NULL;
void (*IT6350_B0_IRQ)(void) = NULL;
void (*IT6350_B1_IRQ)(void) = NULL;
void (*IT6350_B3_IRQ)(void) = NULL;
void (*IT6350_B4_IRQ)(void) = NULL;
void (*IT6350_C3_IRQ)(void) = NULL;
void (*IT6350_C5_IRQ)(void) = NULL;
void (*IT6350_E0_IRQ)(void) = NULL;
void (*IT6350_E4_IRQ)(void) = NULL;
void (*IT6350_F6_IRQ)(void) = NULL;
void (*IT6350_F7_IRQ)(void) = NULL;
void (*IT6350_G2_IRQ)(void) = NULL;
void (*IT6350_G6_IRQ)(void) = NULL;
void (*IT6350_I0_IRQ)(void) = NULL;
void (*IT6350_I1_IRQ)(void) = NULL;
void (*IT6350_I2_IRQ)(void) = NULL;
void (*IT6350_I3_IRQ)(void) = NULL;
void (*IT6350_I4_IRQ)(void) = NULL;
void (*IT6350_I5_IRQ)(void) = NULL;
void (*IT6350_J6_IRQ)(void) = NULL;
void (*IT6350_E7_IRQ)(void) = NULL;
void (*IT6350_SSPI_IRQ)(void) = NULL;
void (*IT6350_UART1_IRQ)(volatile unsigned char *pucLSR, volatile unsigned char *pucRcvData) = NULL;

NS16550_t mUartCtl = (struct NS16550*)REG_UART1_BASE;

/**
 * ****************************************************************************
 * WKINTAD (WKINTA or WKINTD)
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT5_WKINTAD(void)
{
	WUC_Disable_WUx_Interrupt(WU46);
	if(IT6350_E7_IRQ != NULL){
		IT6350_E7_IRQ();
	}
}
/**
 * ****************************************************************************
 * SMBus A Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT9_SMBusA(void)
{
	ISR1 = Int_SMBUS0;

#if (EN_I2C_SLAVE == TRUE)
	/* slave address 1. */
	I2C_Slave_A_ISR_Address_x();
#endif	
}

/**
 * ****************************************************************************
 * SMBus B Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT10_SMBusB(void)
{
    ISR1 = Int_SMBUS1;

#if (EN_I2C_SLAVE == TRUE)
    //I2C_Slave_B_ISR();
    /* W/C isr[x] */


    /* match slave address 2. */
//    if(IS_MASK_SET(SLSTA_B, MSLA2))
//    {
        /* slave address 2. */
//        I2C_Slave_B_ISR_Address_x(_I2C_Match_Slave_Address2);
//    }
//    else
    {
        /* slave address 1. */
        I2C_Slave_B_ISR_Address_1();
    }     
#endif	
}

/**
 * ****************************************************************************
 * WKO[25]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT14_WKO25(void)
{
	WUC_Disable_WUx_Interrupt(WU25);
	if(IT6350_E4_IRQ != NULL){
		IT6350_E4_IRQ();
	}
}

/**
 * ****************************************************************************
 * USB Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT23_Null(void)
{
#if EN_FULL_USB
    hal_usb_irq();
#else
    
    IER2 = 0;
    ISR2 = 0x80;
           
    GPDRG |= 0x20;
    PMER2 = 0;	
    //while(1);
    
    RSTC3 = 0x80;    
        	 	
    ECReg(SC_CONTROL_REG) = 0x08;
    DelayXms(1000);
    
     /* Reset slave */
    REG_WRITE_8BIT(HOST_SLAVE_CONTROL_REG, BIT1);
    DelayXms(1);
    REG_WRITE_8BIT(PORT0_MISC_CONTROL_REGISTER,
    REG_READ_8BIT(PORT0_MISC_CONTROL_REGISTER)&(~BIT4));
    REG_WRITE_8BIT(PORT1_MISC_CONTROL_REGISTER,
    REG_READ_8BIT(PORT1_MISC_CONTROL_REGISTER)&(~BIT4));

    /* Reset slave */
    REG_WRITE_8BIT(HOST_SLAVE_CONTROL_REG, 0x00);   
    
	 		 
    /* set flag(main code to bbk), only for Bx IC */
    MCCR1 |= (0x2A << 2);
    /* jump to BBK section */
    __asm__ volatile 
   (
	    "j __bbk_begin"
    );	

#endif	
}

/**
 * ****************************************************************************
 * SSPI Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT37_SSPI(void)
{
	if(IT6350_SSPI_IRQ != NULL){
		IT6350_SSPI_IRQ();
	}
	ISR4 = BIT5;	
}

/**
 * ****************************************************************************
 * UART1 Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT38_UART1(void)
{
	if(IT6350_UART1_IRQ != NULL){
		IT6350_UART1_IRQ(&mUartCtl->lsr, &mUartCtl->rbrthrdlb);
	}
	ISR4 = BIT6;	
}

/**
 * ****************************************************************************
 * WKO[66]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT54_WKO66(void)
{
//    Hook_IRQ_INT54_WKO66();
	WUC_Disable_WUx_Interrupt(WU66);
	if(IT6350_F6_IRQ != NULL){
		IT6350_F6_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[67]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT55_WKO67(void)
{
//    Hook_IRQ_INT55_WKO67();
	WUC_Disable_WUx_Interrupt(WU67);
	if(IT6350_F7_IRQ != NULL){
		IT6350_F7_IRQ();
	}
}


/**
 * ****************************************************************************
 * WKO[70]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT72_WKO70(void)
{
//    Hook_IRQ_INT72_WKO70();
	WUC_Disable_WUx_Interrupt(WU70);
	if(IT6350_E0_IRQ != NULL){
		IT6350_E0_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[74]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT76_WKO74(void)
{
	WUC_Disable_WUx_Interrupt(WU74);
	if(IT6350_I4_IRQ != NULL){
		IT6350_I4_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[75]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT77_WKO75(void)
{
	WUC_Disable_WUx_Interrupt(WU75);
	if(IT6350_I5_IRQ != NULL){
		IT6350_I5_IRQ();
	}

}

/**
 * ****************************************************************************
 * WKO[89]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT86_WKO89(void)
{
    hal_WUI_USBDminus_irq();


}

/**
 * ****************************************************************************
 * WKO[90]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT87_WKO90(void)
{
    hal_WUI_USBDplus_irq();


}

/**
 * ****************************************************************************
 * WKO[81]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT89_WKO81(void)
{
	WUC_Disable_WUx_Interrupt(WU81);
	if(IT6350_A4_IRQ != NULL){
		IT6350_A4_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[82]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT90_WKO82(void)
{
	WUC_Disable_WUx_Interrupt(WU82);
	if(IT6350_A5_IRQ != NULL){
		IT6350_A5_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[83]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT91_WKO83(void)
{
	WUC_Disable_WUx_Interrupt(WU83);
	if(IT6350_A6_IRQ != NULL){ 	
		IT6350_A6_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[91]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT96_WKO91(void)
{
//    Hook_IRQ_INT96_WKO91();
	WUC_Disable_WUx_Interrupt(WU91);
	if(IT6350_A0_IRQ != NULL){
		IT6350_A0_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[92]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT97_WKO92(void)
{
//    Hook_IRQ_INT97_WKO92();
	WUC_Disable_WUx_Interrupt(WU92);
	if(IT6350_A1_IRQ != NULL){
		IT6350_A1_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[94]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT99_WKO94(void)
{
//    Hook_IRQ_INT99_WKO94();
	WUC_Disable_WUx_Interrupt(WU94);
	if(IT6350_B4_IRQ != NULL){
		IT6350_B4_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[101]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT106_WKO101(void)
{
	WUC_Disable_WUx_Interrupt(WU101);
	if(IT6350_B0_IRQ != NULL){
		IT6350_B0_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[102]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT107_WKO102(void)
{
	WUC_Disable_WUx_Interrupt(WU102);
	if(IT6350_B1_IRQ != NULL){
		IT6350_B1_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[103]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT108_WKO103(void)
{
	WUC_Disable_WUx_Interrupt(WU103);
	if(IT6350_B3_IRQ != NULL){
		IT6350_B3_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[108]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT113_WKO108(void)
{
	WUC_Disable_WUx_Interrupt(WU108);
	if(IT6350_C3_IRQ != NULL){
		IT6350_C3_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[109]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT114_WKO109(void)
{
	WUC_Disable_WUx_Interrupt(WU109);
	if(IT6350_C5_IRQ != NULL){
		IT6350_C5_IRQ();
	}

}

/**
 * ****************************************************************************
 * WKO[114]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT119_WKO114(void)
{
	WUC_Disable_WUx_Interrupt(WU114);
	if(IT6350_E4_IRQ != NULL){
		IT6350_E4_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[117]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT122_WKO117(void)
{
	WUC_Disable_WUx_Interrupt(WU117);
	if(IT6350_G2_IRQ != NULL){
		IT6350_G2_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[118]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT123_WKO118(void)
{
	WUC_Disable_WUx_Interrupt(WU118);
	if(IT6350_G6_IRQ != NULL){
		IT6350_G6_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[119]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT124_WKO119(void)
{
	WUC_Disable_WUx_Interrupt(WU119);
	if(IT6350_I0_IRQ != NULL){
		IT6350_I0_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[120]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT125_WKO120(void)
{
	WUC_Disable_WUx_Interrupt(WU120);
	if(IT6350_I1_IRQ != NULL){
		IT6350_I1_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[121]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT126_WKO121(void)
{
	WUC_Disable_WUx_Interrupt(WU121);
	if(IT6350_I2_IRQ != NULL){
		IT6350_I2_IRQ();
	}
}

/**
 * ****************************************************************************
 * WKO[122]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT127_WKO122(void)
{
	WUC_Disable_WUx_Interrupt(WU122);
	if(IT6350_I3_IRQ != NULL){
		IT6350_I3_IRQ();
	}

}

/**
 * ****************************************************************************
 * WKO[134]
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT134_WKO134(void)
{
	WUC_Disable_WUx_Interrupt(WU134);
	if(IT6350_J6_IRQ != NULL){
		IT6350_J6_IRQ();
	}

}

/**
 * ****************************************************************************
 * SMBus E Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT152_SMBusE(void)
{
#if (EN_I2C_SLAVE == TRUE)
	I2C_Slave_Handler(I2C_E);
#endif
}

/**
 * ****************************************************************************
 * SMBus F Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT153_SMBusF(void)
{
#if (EN_I2C_SLAVE == TRUE)
//    Hook_IRQ_INT153_SMBusF();
#if 0	
//	printf("%02X\n", (*((volatile unsigned char *)(0x00F02D83))));
	volatile UINT8	u8Tmp0, u8Tmp1, u8Tmp2, u8Tmp3, u8Tmp4;
	u8Tmp4 = ECReg(I2C_NSWDST(2));
	u8Tmp0 = (*((volatile unsigned char *)(0x00F02D83)));
	WNCKR = 0x00;
	u8Tmp1 = (*((volatile unsigned char *)(0x00F02D83)));	
	WNCKR = 0x00;
	u8Tmp2 = (*((volatile unsigned char *)(0x00F02D83)));	
	WNCKR = 0x00;
	u8Tmp3 = (*((volatile unsigned char *)(0x00F02D83)));	

#endif
	I2C_Slave_Handler(I2C_F);
//		I2C_CTR_F |= BIT0;	
//	GPDRG ^= BIT6;
//	printf("%X, %X, %X, %X %X\r\n", u8Tmp0, u8Tmp1, u8Tmp2, u8Tmp3, u8Tmp4 );

//	I2C_CTR_F |= BIT0;	
//	printf("c0x%02X\n", (*((volatile unsigned char *)(0x00F02D83))));	
#endif
}

/**
 * ****************************************************************************
 * External Timer 3 Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
unsigned int g_u321msTick0 = 0;
unsigned int g_u321msTick1 = 0;
unsigned char g_u81msTickSel = 0;
void ISR_CODE IRQ_INT155_ET3(void)
{
    /* Write to clear external timer 3 interrupt */
    ISR19 = Int_ET3Intr;
	if(g_u81msTickSel){
		g_u321msTick1++;
	}else{
		g_u321msTick0++;
	}
    /* Request 1 mS timer service. */
    F_Service_MS_1 = 1;


    /* to increase 1ms counter */
    Timer1msCounter++;

	if(Timer1msCounter%10 == 8)  
	{
        /* set 10ms service event A */
        Service_Timer10msEventA++;
    }

    if(Timer1msCounter%60000 == 0)
    {
        Timer1msCounter=0x00;
    }

    
}

/**
 * ****************************************************************************
 * External Timer 5 Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
unsigned int g_u32100usTick0 = 0;
unsigned int g_u32100usTick1 = 0;
unsigned char g_u8100usTickSel = 0;
void ISR_CODE IRQ_INT157_ET5(void)	//91.55us
{
    /* Write to clear external timer 5 interrupt */
    ISR19 = Int_ET5Intr;
	if(g_u8100usTickSel){
		g_u32100usTick1++;
	}else{
		g_u32100usTick0++;
	}

//    Disable_External_Timer_x(ExternalTimer_5);
}

/**
 * ****************************************************************************
 * External Timer 7 Interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INT159_ET7(void)
{
    ISR19 = Int_ET7Intr;

    /* To set extend event service flag */
    EXTEND_EVENT_ISR_SERVICE_FLAG = 1;

    /* To set extend task service flag */
    F_Service_Extend_Task = 1;
}

/**
 * ****************************************************************************
 * ISR reserved
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void ISR_CODE IRQ_INTxxx_Reserved(void)
{

}

void ISR_CODE IRQ_Isr_Int1(void)
{
	_nop_();

}
/*
 * ****************************************************************************
 * IVT for Isr_Int1 function
 * ****************************************************************************
 */
const ISR_RODATA FUNCT_PTR_V_V IRQ_Service[] = 
{
	IRQ_INTxxx_Reserved,	//IRQ_INT0_Null,              //   INT0	Reserved
	IRQ_INTxxx_Reserved,	//IRQ_INT1_WKO20,             //   INT1	WUC interrupt WUI0	
	IRQ_INTxxx_Reserved,	//IRQ_INT2_KBCOBE,            //   INT2	KBC output buffer empty interrupt 
	IRQ_INTxxx_Reserved,	//IRQ_INT3_PMCPMC1OBE,        //   INT3	PMC output buffer empty interrupt 
	IRQ_INTxxx_Reserved,	//IRQ_INT4_SMBusD,            //   INT4	SMBus D Interrupt
	IRQ_INT5_WKINTAD,           //   INT5	WUC interrupt (WU10 ~ WU15)(WU40~47)	
	IRQ_INTxxx_Reserved,	//IRQ_INT6_WKO23,             //   INT6	WUC interrupt WUI3
	IRQ_INTxxx_Reserved,	//IRQ_INT7_PWM,               //   INT7	PWM interrupt 

	IRQ_INTxxx_Reserved,	//IRQ_INT8_ADC,    		    //   INT8  	ADC Interrupt 	
    IRQ_INT9_SMBusA,      	    //   INT9  	SMB0 Interrupt 
    IRQ_INT10_SMBusB,           //   INT10 	SMB1 Interrupt
	IRQ_INTxxx_Reserved,	//IRQ_INT11_KBMatrixScan,     //   INT11 	Key matrix scan Int
	IRQ_INTxxx_Reserved,	//IRQ_INT12_WKO26,            //   INT12	WUC interrupt SWUC wake up
	IRQ_INTxxx_Reserved,	//IRQ_INT13_WKINTC,           //   INT13	WUC interrupt KSI wake-up (WU30~37)
    IRQ_INT14_WKO25,            //   INT14	WUC interrupt Power switch
	IRQ_INTxxx_Reserved,	//IRQ_INT15_CIR,              //   INT15 	CIR interrupt 

	IRQ_INTxxx_Reserved,	//IRQ_INT16_SMBusC,           //   INT16	SMB2 Interrupt
	IRQ_INTxxx_Reserved,	//IRQ_INT17_WKO24,            //   INT17	WUC Interrupt WUI4
	IRQ_INTxxx_Reserved,	//IRQ_INT18_PS2Interrupt2,    //   INT18 	PS2 P2 Interrupt
	IRQ_INTxxx_Reserved,	//IRQ_INT19_PS2Interrupt1,    //   INT19 	PS2 P1 Interrupt
	IRQ_INTxxx_Reserved,	//IRQ_INT20_PS2Interrupt0,    //   INT20 	PS2 P0 Interrupt
	IRQ_INTxxx_Reserved,	//IRQ_INT21_WKO22,            //   INT21	WUC Interrupt WUI2
	IRQ_INTxxx_Reserved,	//IRQ_INT22_SMFISemaphore,    //   INT22  SMFI Semaphore Interrupt
    IRQ_INT23_Null,             //   INT23  --
    
	IRQ_INTxxx_Reserved,	//IRQ_INT24_KBCIBF,           //   INT24 	KBC input buffer empty interrupt 
	IRQ_INTxxx_Reserved,	//IRQ_INT25_PMCPMC1IBF,       //   INT25 	PMC input buffer empty interrupt 
	IRQ_INTxxx_Reserved,	//IRQ_INT26_PMC2OBE,          //   INT26  PMC2 Output Buffer Empty Intr.
	IRQ_INTxxx_Reserved,	//IRQ_INT27_PMC2IBF,          //   INT27  PMC2 Input Buffer Full Intr.
	IRQ_INTxxx_Reserved,	//IRQ_INT28_GINTofGPD5,       //   INT28  GINT from function 1 of GPD5
	IRQ_INTxxx_Reserved,	//IRQ_INT29_EGPC,             //   INT29  EGPC Interrupt
	IRQ_INTxxx_Reserved,	//IRQ_INT30_ET1,              //   INT30	External timer interrupt 
	IRQ_INTxxx_Reserved,	//IRQ_INT31_WKO21,            //   INT31	WUC	interrupt WUI1

	IRQ_INTxxx_Reserved,	//IRQ_INT32_GPINT0,           //   INT32  GPINT0	
	IRQ_INTxxx_Reserved,	//IRQ_INT33_GPINT1,           //   INT33  GPINT1	    
	IRQ_INTxxx_Reserved,	//IRQ_INT34_GPINT2,           //   INT34  GPINT2
	IRQ_INTxxx_Reserved,	//IRQ_INT35_GPINT3,           //   INT35  GPINT3
	IRQ_INTxxx_Reserved,	//IRQ_INT36_CIRGPINT,         //   INT36  CIR GPINT
    IRQ_INT37_SSPI,             //   INT37  SSPI Interrupt
    IRQ_INT38_UART1,            //   INT38  UART1 Interrupt
	IRQ_INTxxx_Reserved,	//IRQ_INT39_UART2,            //   INT39  UART2 Interrupt

	IRQ_INTxxx_Reserved,	//IRQ_INT40_Null,       		//   INT40 	--
	IRQ_INTxxx_Reserved,	//IRQ_INT41_Null,       		//   INT41 	--
	IRQ_INTxxx_Reserved,	//IRQ_INT42_Null,             //   INT42  --
	IRQ_INTxxx_Reserved,	//IRQ_INT43_Null,             //   INT43  --
	IRQ_INTxxx_Reserved,	//IRQ_INT44_Null,             //   INT44  --
	IRQ_INTxxx_Reserved,	//IRQ_INT45_Null,             //   INT45  --
	IRQ_INTxxx_Reserved,	//IRQ_INT46_Null,             //   INT46	--
	IRQ_INTxxx_Reserved,	//IRQ_INT47_Null,             //   INT47	--

	IRQ_INTxxx_Reserved,	//IRQ_INT48_WKO60,       	    //   INT48  WKO[60]	
	IRQ_INTxxx_Reserved,	//IRQ_INT49_WKO61,       	    //   INT49  WKO[61] 	
	IRQ_INTxxx_Reserved,	//IRQ_INT50_WKO62,            //   INT50  WKO[62]
	IRQ_INTxxx_Reserved,	//IRQ_INT51_WKO63,            //   INT51  WKO[63]
	IRQ_INTxxx_Reserved,	//IRQ_INT52_WKO64,            //   INT52  WKO[64]
	IRQ_INTxxx_Reserved,	//IRQ_INT53_WKO65,            //   INT53  WKO[65]
    IRQ_INT54_WKO66,            //   INT54  WKO[66]	
    IRQ_INT55_WKO67,            //   INT55  WKO[67]    

	IRQ_INTxxx_Reserved,	//IRQ_INT56_Null,             //   INT56  -- 	
	IRQ_INTxxx_Reserved,	//IRQ_INT57_Null,             //   INT57  --
	IRQ_INTxxx_Reserved,	//IRQ_INT58_ET2,              //   INT58  External Timer 2 Interrupt
	IRQ_INTxxx_Reserved,	//IRQ_INT59_DeferredSPIInstruction,   //   INT59  Deferred SPI Instruction     
	IRQ_INTxxx_Reserved,	//IRQ_INT60_TMRINTA0,         //   INT60  TMRINTA0
	IRQ_INTxxx_Reserved,	//IRQ_INT61_TMRINTA1,         //   INT61  TMRINTA1
	IRQ_INTxxx_Reserved,	//IRQ_INT62_TMRINTB0,         //   INT62  TMRINTB0  
	IRQ_INTxxx_Reserved,	//IRQ_INT63_TMRINTB1,         //   INT63  TMRINTB1

	IRQ_INTxxx_Reserved,	//IRQ_INT64_PMC2EXOBE,        //   INT64  PMC2EX Output Buffer Empty Intr.
	IRQ_INTxxx_Reserved,	//IRQ_INT65_PMC2EXIBF,        //   INT65  PMC2EX Input Buffer Full Intr.
	IRQ_INTxxx_Reserved,	//IRQ_INT66_PMC3OBE,          //   INT66  PMC3 Output Buffer Empty Intr.
	IRQ_INTxxx_Reserved,	//IRQ_INT67_PMC3IBF,          //   INT67  PMC3 Input Buffer Full Intr.
	IRQ_INTxxx_Reserved,	//IRQ_INT68_PMC4OBE,          //   INT68  PMC4 Output Buffer Empty Intr.
	IRQ_INTxxx_Reserved,	//IRQ_INT69_PMC4IBF,          //   INT69  PMC4 Input Buffer Full Intr.
	IRQ_INTxxx_Reserved,	//IRQ_INT70_Null,             //   INT70  --
	IRQ_INTxxx_Reserved,	//IRQ_INT71_I2BRAM,           //   INT71  I2BRAM Interrupt

    IRQ_INT72_WKO70,            //   INT72  WKO[70]	
	IRQ_INTxxx_Reserved,	//IRQ_INT73_WKO71,            //   INT73  WKO[71] 	
	IRQ_INTxxx_Reserved,	//IRQ_INT74_WKO72,            //   INT74  WKO[72]
	IRQ_INTxxx_Reserved,	//IRQ_INT75_WKO73,            //   INT75  WKO[73]
    IRQ_INT76_WKO74,            //   INT76  WKO[74]
    IRQ_INT77_WKO75,            //   INT77  WKO[75]
	IRQ_INTxxx_Reserved,	//IRQ_INT78_WKO76,            //   INT78  WKO[76]	
	IRQ_INTxxx_Reserved,	//IRQ_INT79_WKO77,            //   INT79  WKO[77]   

	IRQ_INTxxx_Reserved,	//IRQ_INT80_ET8,              //   INT80  External timer 8 interrupt 
	IRQ_INTxxx_Reserved,	//IRQ_INT81_SMBusClockHeld,   //   INT81  SMBus clock held interrupt.
	IRQ_INTxxx_Reserved,	//IRQ_INT82_CEC,              //   INT82  CEC interrupt.
	IRQ_INTxxx_Reserved,	//IRQ_INT83_H2RAMLPC,         //   INT83  H2RAM LPC Trigger.
	IRQ_INTxxx_Reserved,	//IRQ_INT84_Null,             //   INT84  --
	IRQ_INTxxx_Reserved,	//IRQ_INT85_WKO88,            //   INT85  WKO[88] 
    IRQ_INT86_WKO89,            //   INT86  WKO[89] 
    IRQ_INT87_WKO90,            //   INT87  WKO[90]

	IRQ_INTxxx_Reserved,	//IRQ_INT88_WKO80,            //   INT88  WKO[80]
    IRQ_INT89_WKO81,            //   INT89  WKO[81]
    IRQ_INT90_WKO82,            //   INT90  WKO[82]
    IRQ_INT91_WKO83,            //   INT91  WKO[83]
	IRQ_INTxxx_Reserved,	//IRQ_INT92_WKO84,            //   INT92  WKO[84]
	IRQ_INTxxx_Reserved,	//IRQ_INT93_WKO85,            //   INT93  WKO[85]
	IRQ_INTxxx_Reserved,	//IRQ_INT94_WKO86,            //   INT94  WKO[86]
	IRQ_INTxxx_Reserved,	//IRQ_INT95_WKO87,            //   INT95  WKO[87]

    IRQ_INT96_WKO91,            //   INT96  WKO[91]
    IRQ_INT97_WKO92,            //   INT97  WKO[92]
	IRQ_INTxxx_Reserved,	//IRQ_INT98_WKO93,            //   INT98  WKO[93]
    IRQ_INT99_WKO94,            //   INT99  WKO[94]
	IRQ_INTxxx_Reserved,	//IRQ_INT100_WKO95,           //   INT100 WKO[95]
	IRQ_INTxxx_Reserved,	//IRQ_INT101_WKO96,           //   INT101 WKO[96]
	IRQ_INTxxx_Reserved,	//IRQ_INT102_WKO97,           //   INT102 WKO[97]
	IRQ_INTxxx_Reserved,	//IRQ_INT103_WKO98,           //   INT103 WKO[98]

	IRQ_INTxxx_Reserved,	//IRQ_INT104_WKO99,           //   INT104 WKO[99]
	IRQ_INTxxx_Reserved,	//IRQ_INT105_WKO100,          //   INT105 WKO[100]
    IRQ_INT106_WKO101,          //   INT106 WKO[101]
    IRQ_INT107_WKO102,          //   INT107 WKO[102]
    IRQ_INT108_WKO103,          //   INT108 WKO[103]
	IRQ_INTxxx_Reserved,	//IRQ_INT109_WKO104,          //   INT109 WKO[104]
	IRQ_INTxxx_Reserved,	//IRQ_INT110_WKO105,          //   INT110 WKO[105]
	IRQ_INTxxx_Reserved,	//IRQ_INT111_WKO106,          //   INT111 WKO[106]

	IRQ_INTxxx_Reserved,	//IRQ_INT112_WKO107,          //   INT112 WKO[107]
    IRQ_INT113_WKO108,          //   INT113 WKO[108]
    IRQ_INT114_WKO109,          //   INT114 WKO[109]
	IRQ_INTxxx_Reserved,	//IRQ_INT115_WKO110,          //   INT115 WKO[110]
	IRQ_INTxxx_Reserved,	//IRQ_INT116_WKO111,          //   INT116 WKO[111]
	IRQ_INTxxx_Reserved,	//IRQ_INT117_WKO112,          //   INT117 WKO[112]
	IRQ_INTxxx_Reserved,	//IRQ_INT118_WKO113,          //   INT118 WKO[113]
    IRQ_INT119_WKO114,          //   INT119 WKO[114]

	IRQ_INTxxx_Reserved,	//IRQ_INT120_WKO115,          //   INT120 WKO[115]
	IRQ_INTxxx_Reserved,	//IRQ_INT121_WKO116,          //   INT121 WKO[116]
    IRQ_INT122_WKO117,          //   INT122 WKO[117]
    IRQ_INT123_WKO118,          //   INT123 WKO[118]
    IRQ_INT124_WKO119,          //   INT124 WKO[119]
    IRQ_INT125_WKO120,          //   INT125 WKO[120]
    IRQ_INT126_WKO121,          //   INT126 WKO[121]
    IRQ_INT127_WKO122,          //   INT127 WKO[122]

	IRQ_INTxxx_Reserved,	//IRQ_INT128_WKO128,          //   INT128 WKO[128]
	IRQ_INTxxx_Reserved,	//IRQ_INT129_WKO129,          //   INT129 WKO[129]
	IRQ_INTxxx_Reserved,	//IRQ_INT130_WKO130,          //   INT130 WKO[130]
	IRQ_INTxxx_Reserved,	//IRQ_INT131_WKO131,          //   INT131 WKO[131]
	IRQ_INTxxx_Reserved,	//IRQ_INT132_WKO132,          //   INT132 WKO[132]
	IRQ_INTxxx_Reserved,	//IRQ_INT133_WKO133,          //   INT133 WKO[133]
    IRQ_INT134_WKO134,          //   INT134 WKO[134]
	IRQ_INTxxx_Reserved,	//IRQ_INT135_Null,            //   INT135 --	
	IRQ_INTxxx_Reserved,	//IRQ_INT136_Null,            //   INT136 --
	IRQ_INTxxx_Reserved,	//IRQ_INT137_Null,            //   INT137 --
	IRQ_INTxxx_Reserved,	//IRQ_INT138_Null,            //   INT138 --
	IRQ_INTxxx_Reserved,	//IRQ_INT139_Null,            //   INT139 --
	IRQ_INTxxx_Reserved,	//IRQ_INT140_Null,            //   INT140 --
	IRQ_INTxxx_Reserved,	//IRQ_INT141_Null,            //   INT141 --
	IRQ_INTxxx_Reserved,	//IRQ_INT142_Null,            //   INT142 --
	IRQ_INTxxx_Reserved,	//IRQ_INT143_Null,            //   INT143 --

	IRQ_INTxxx_Reserved,	//IRQ_INT144_Null,            //   INT144 --
	IRQ_INTxxx_Reserved,	//IRQ_INT145_Null,            //   INT145 --
	IRQ_INTxxx_Reserved,	//IRQ_INT146_Null,            //   INT146 --
	IRQ_INTxxx_Reserved,	//IRQ_INT147_Null,            //   INT147 --
	IRQ_INTxxx_Reserved,	//IRQ_INT148_Null,            //   INT148 --
	IRQ_INTxxx_Reserved,	//IRQ_INT149_PMC5OBE,         //   INT149 PMC5 output buffer empty interrupt.
	IRQ_INTxxx_Reserved,	//IRQ_INT150_PMC5IBF,         //   INT150 PMC5 input buffer full interrupt.
	IRQ_INTxxx_Reserved,	//IRQ_INT151_VCI,             //   INT151 Voltage comparator interrupt.

    IRQ_INT152_SMBusE,          //   INT152 SMBus E Interrupt.
    IRQ_INT153_SMBusF,          //   INT153 SMBus F Interrupt.
	IRQ_INTxxx_Reserved,	//IRQ_INT154_OSCDMA,          //   INT154 OSC DMA Interrupt.
    IRQ_INT155_ET3,             //   INT155 External Timer 3 Interrupt.
	IRQ_INTxxx_Reserved,	//IRQ_INT156_ET4,             //   INT156 External Timer 4 Interrupt.
    IRQ_INT157_ET5,             //   INT157 External Timer 5 Interrupt.
	IRQ_INTxxx_Reserved,	//IRQ_INT158_ET6,             //   INT158 External Timer 6 Interrupt.
    IRQ_INT159_ET7,             //   INT159 External Timer 7 Interrupt.

    IRQ_INTxxx_Reserved,        //   INT160 Reserved
    IRQ_INTxxx_Reserved,        //   INT161 Reserved
    IRQ_INTxxx_Reserved,        //   INT162 Reserved
    IRQ_INTxxx_Reserved,        //   INT163 Reserved
    IRQ_INTxxx_Reserved,        //   INT164 Reserved
    IRQ_INTxxx_Reserved,        //   INT165 Reserved
    IRQ_INTxxx_Reserved,        //   INT166 Reserved
    IRQ_INTxxx_Reserved,        //   INT167 Reserved

    IRQ_INTxxx_Reserved,        //   INT168 Reserved
    IRQ_INTxxx_Reserved,        //   INT169 Reserved
    IRQ_INTxxx_Reserved,        //   INT170 Reserved
    IRQ_INTxxx_Reserved,        //   INT171 Reserved
    IRQ_INTxxx_Reserved,        //   INT172 Reserved
    IRQ_INTxxx_Reserved,        //   INT173 Reserved
    IRQ_INTxxx_Reserved,        //   INT174 Reserved
    IRQ_INTxxx_Reserved,        //   INT175 Reserved

    IRQ_INTxxx_Reserved,        //   INT176 Reserved
    IRQ_INTxxx_Reserved,        //   INT177 Reserved
    IRQ_INTxxx_Reserved,        //   INT178 Reserved
    IRQ_INTxxx_Reserved,        //   INT179 Reserved
    IRQ_INTxxx_Reserved,        //   INT180 Reserved
    IRQ_INTxxx_Reserved,        //   INT181 Reserved
    IRQ_INTxxx_Reserved,        //   INT182 Reserved
    IRQ_INTxxx_Reserved,        //   INT183 Reserved

    IRQ_INTxxx_Reserved,        //   INT184 Reserved
    IRQ_INTxxx_Reserved,        //   INT185 Reserved
    IRQ_INTxxx_Reserved,        //   INT186 Reserved
    IRQ_INTxxx_Reserved,        //   INT187 Reserved
    IRQ_INTxxx_Reserved,        //   INT188 Reserved
    IRQ_INTxxx_Reserved,        //   INT189 Reserved
    IRQ_INTxxx_Reserved,        //   INT190 Reserved
    IRQ_INTxxx_Reserved,        //   INT191 Reserved

    IRQ_INTxxx_Reserved,        //   INT192 Reserved
    IRQ_INTxxx_Reserved,        //   INT193 Reserved
    IRQ_INTxxx_Reserved,        //   INT194 Reserved
    IRQ_INTxxx_Reserved,        //   INT195 Reserved
    IRQ_INTxxx_Reserved,        //   INT196 Reserved
    IRQ_INTxxx_Reserved,        //   INT197 Reserved
    IRQ_INTxxx_Reserved,        //   INT198 Reserved
    IRQ_INTxxx_Reserved,        //   INT199 Reserved

    IRQ_INTxxx_Reserved,        //   INT200 Reserved
    IRQ_INTxxx_Reserved,        //   INT201 Reserved
    IRQ_INTxxx_Reserved,        //   INT202 Reserved
    IRQ_INTxxx_Reserved,        //   INT203 Reserved
    IRQ_INTxxx_Reserved,        //   INT204 Reserved
    IRQ_INTxxx_Reserved,        //   INT205 Reserved
    IRQ_INTxxx_Reserved,        //   INT206 Reserved
    IRQ_INTxxx_Reserved,        //   INT207 Reserved

    IRQ_INTxxx_Reserved,        //   INT208 Reserved
    IRQ_INTxxx_Reserved,        //   INT209 Reserved
    IRQ_INTxxx_Reserved,        //   INT210 Reserved
    IRQ_INTxxx_Reserved,        //   INT211 Reserved
    IRQ_INTxxx_Reserved,        //   INT212 Reserved
    IRQ_INTxxx_Reserved,        //   INT213 Reserved
    IRQ_INTxxx_Reserved,        //   INT214 Reserved
    IRQ_INTxxx_Reserved,        //   INT215 Reserved

    IRQ_INTxxx_Reserved,        //   INT216 Reserved
    IRQ_INTxxx_Reserved,        //   INT217 Reserved
    IRQ_INTxxx_Reserved,        //   INT218 Reserved
    IRQ_INTxxx_Reserved,        //   INT219 Reserved
    IRQ_INTxxx_Reserved,        //   INT220 Reserved
    IRQ_INTxxx_Reserved,        //   INT221 Reserved
    IRQ_INTxxx_Reserved,        //   INT222 Reserved
    IRQ_INTxxx_Reserved,        //   INT223 Reserved

    IRQ_INTxxx_Reserved,        //   INT224 Reserved
    IRQ_INTxxx_Reserved,        //   INT225 Reserved
    IRQ_INTxxx_Reserved,        //   INT226 Reserved
    IRQ_INTxxx_Reserved,        //   INT227 Reserved
    IRQ_INTxxx_Reserved,        //   INT228 Reserved
    IRQ_INTxxx_Reserved,        //   INT229 Reserved
    IRQ_INTxxx_Reserved,        //   INT230 Reserved
    IRQ_INTxxx_Reserved,        //   INT231 Reserved

    IRQ_INTxxx_Reserved,        //   INT232 Reserved
    IRQ_INTxxx_Reserved,        //   INT233 Reserved
    IRQ_INTxxx_Reserved,        //   INT234 Reserved
    IRQ_INTxxx_Reserved,        //   INT235 Reserved
    IRQ_INTxxx_Reserved,        //   INT236 Reserved
    IRQ_INTxxx_Reserved,        //   INT237 Reserved
    IRQ_INTxxx_Reserved,        //   INT238 Reserved
    IRQ_INTxxx_Reserved,        //   INT239 Reserved
};

/**
 * ****************************************************************************
 * ISR for interrupt HW1
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */			
void Section_Isr_Int1 Isr_Int1 (void)
{
    ulong_32 l_temp_evect;

    l_temp_evect = IVECT;
    /* Dispatch to service handler. */
    (IRQ_Service[(l_temp_evect-0x10)])();
#if 0	
    IRQ_Isr_Int1();
#else
	l_temp_evect = 0;
#endif


} 

/**
 * ****************************************************************************
 * dummy
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */			
void Section_Isr_dummy Isr_Int1_dummy (void)
{
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	_nop_();
	_nop_();
	_nop_();
	_nop_();

	_nop_();
	_nop_();
}

/**
 * ****************************************************************************
 * The function of disable all interrupts
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */	
void DisableAllInterrupt(void)
{
    DisableGlobalInt();
	_nop_();
	_nop_();
	_nop_();
	_nop_();
}

/**
 * ****************************************************************************
 * The function of enable all interrupts
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */	
void EnableAllInterrupt(void)
{
    EnableGlobalInt();
}

/**
 * ****************************************************************************
 * The function of enabling interrupts
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */	
void InitEnableInterrupt(void)
{
	ISR0 = 0xFF;
	ISR1 = 0xFF;
	ISR2 = 0xFF;
	ISR3 = 0xFF;
	ISR4 = 0xFF;
    ISR5 = 0xFF;
	ISR6 = 0xFF;
    ISR7 = 0xFF;
    ISR8 = 0xFF;
    ISR9 = 0xFF;
    ISR10 = 0xFF;
    ISR11 = 0xFF;
    ISR12 = 0xFF;
    ISR13 = 0xFF;
    ISR14 = 0xFF;
    ISR15 = 0xFF;
    ISR16 = 0xFF;
    ISR17 = 0xFF;
    ISR18 = 0xFF;
    ISR19 = 0xFF;

    EnableExternalInt();
	EnableAllInterrupt();
}

/**
 * ****************************************************************************
 * The function of enabling module interrupt
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */	
void EnableModuleInterrupt(void)
{

    /* enable all interrupts */
    EnableAllInterrupt();
}

/**
 * ****************************************************************************
 * Exception debug message dump function.
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_General_Exception General_Exception(void)
{
    DBG_IPC   = GET_IPC();
    DBG_IPSW  = GET_IPSW();
    DBG_SP    = __nds32__get_current_sp();
    DBG_ITYPE = GET_ITYPE();

    if(*((BYTE*)DBG_IPC) & 0x80)
    {
        SET_IPC(DBG_IPC + 2);
    }
    else
    {
        SET_IPC(DBG_IPC + 4);
    }
    
//    while(1);
}

/**
 * ****************************************************************************
 * HW0 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW0 Interrupt_HW0(void)
{
    while(1);
}

/**
 * ****************************************************************************
 * HW1 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW1 Interrupt_HW1(void)
{
    Isr_Int1();
}

/**
 * ****************************************************************************
 * HW2 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW2 Interrupt_HW2(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW3 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW3 Interrupt_HW3(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW4 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW4 Interrupt_HW4(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW5 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW5 Interrupt_HW5(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW6 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW6 Interrupt_HW6(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW7 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW7 Interrupt_HW7(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW8 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW8 Interrupt_HW8(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW9 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW9 Interrupt_HW9(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW10 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW10 Interrupt_HW10(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW11 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW11 Interrupt_HW11(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW12 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW12 Interrupt_HW12(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW13 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW13 Interrupt_HW13(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW14 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW14 Interrupt_HW14(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * HW15 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_HW15 Interrupt_HW15(void)
{
	while(1);
}

/**
 * ****************************************************************************
 * SW0 ISR
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Section_Interrupt_SW0 Interrupt_SW0(void)
{
	while(1);
}

/*
 * ****************************************************************************
 * Copyright Andes Technology Corporation 2009-2010
 * All Rights Reserved.
 *
 *  Revision History:
 *
 *    Dec.28.2009     Created.
 *    Sep.05.2012     Modify for IT8380, by Jyunlin [ite01242]
 * ****************************************************************************
 */

#define HAL_DISABLE_INTERRUPTS		        (0)
#define HAL_ENABLE_INTERRUPTS		        (1)

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 * int_op
 *
 * ****************************************************************************
 */
inline unsigned int hal_global_int_ctl(int int_op)
{
	int ret = GET_PSW() & PSW_mskGIE;

	if (int_op == HAL_DISABLE_INTERRUPTS) 
    {
        /* CPU int mask */
        SET_INT_MASK(0x00000000);
		/* GIE_DISABLE(); */
	}
	else
    {
        /* CPU int mask */
        /* SET_INT_MASK(0xC001FFFF); */

        /* CPU int mask */
        SET_INT_MASK(0x40010002);
		/* GIE_ENABLE(); */
	}

	return ret ? !HAL_DISABLE_INTERRUPTS : HAL_DISABLE_INTERRUPTS;
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void EnableGlobalInt(void)
{
    int ret = GET_PSW() & PSW_mskGIE;

    if(ret == HAL_DISABLE_INTERRUPTS)
    {
        GIE_ENABLE();
    }
    /* hal_global_int_ctl(HAL_ENABLE_INTERRUPTS); */
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void DisableGlobalInt(void)
{
    int ret = GET_PSW() & PSW_mskGIE;

    if(ret == HAL_ENABLE_INTERRUPTS)
    {
        GIE_DISABLE();
    }
    /* hal_global_int_ctl(HAL_DISABLE_INTERRUPTS); */
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void EnableExternalInt(void)
{
    hal_global_int_ctl(HAL_ENABLE_INTERRUPTS);
}

