/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_power.h
 * ****************************************************************************
 */

#ifndef CORE_POWER_H
#define CORE_POWER_H

/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void CheckResetSource(void);
extern void PowerM_Doze(void);
extern void PowerM_DeepDoze(void);
extern void PowerM_Sleep(void);
extern void ChangePLLFrequency(BYTE newseting);

#endif

