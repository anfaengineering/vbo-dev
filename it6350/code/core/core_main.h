/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_main.h
 * Dino Li
 * ****************************************************************************
 */
 
#ifndef CORE_MAIN_H
#define CORE_MAIN_H 

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define Only_Timer1msEvent  0x01
#define All_TimerEvent      0x00
#define Normal_MainService  0xFF
 
/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
int main(void);
extern void main_service(void);
void service_reserved(void);
void service_1mS(void); 
void ISR_Timer5msEvent(void);
void ISR_Timer10msEvent(void);
void Timer1msEvent(void);
void Timer10msEventA(void);
extern BYTE CheckEventPending(void);
extern void service_extend_task(void);

#endif  

