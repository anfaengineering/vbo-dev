/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_main.c
 * Dino Li
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

#if EN_FULL_USB
#else
void Init_USB(void)
{
    GPCRH5 = 0x80;
    GPCRH6 = 0x80;
    
    MCCR &= ~BIT7;
    PMER2  |= BIT7;
	
	/* init start */

    /* Reset slave */
    REG_WRITE_8BIT(HOST_SLAVE_CONTROL_REG, BIT1);
    DelayXms(1);
    REG_WRITE_8BIT(PORT0_MISC_CONTROL_REGISTER,
        REG_READ_8BIT(PORT0_MISC_CONTROL_REGISTER)&(~BIT4));
	REG_WRITE_8BIT(PORT1_MISC_CONTROL_REGISTER,
        REG_READ_8BIT(PORT1_MISC_CONTROL_REGISTER)&(~BIT4));

    /* Reset slave */
    REG_WRITE_8BIT(HOST_SLAVE_CONTROL_REG, 0x00);   

    REG_WRITE_8BIT(SC_CONTROL_REG, 0x31);    

    REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG,
        SC_TRANS_DONE_BIT|SC_RESET_EVENT_BIT|SC_SOF_RECEIVED_BIT);
    REG_WRITE_8BIT(SC_INTERRUPT_MASK_REG,
        SC_TRANS_DONE_BIT|SC_RESET_EVENT_BIT|SC_SOF_RECEIVED_BIT);
    IER2 |= BIT7;
    REG_WRITE_8BIT(SC_ADDRESS,0x00);
    
    //usb_slave_ep_reset();
     REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,
            ENDPOINT_ENABLE_BIT|ENDPOINT_READY_BIT);
   
    //gbUSBInitOK = TRUE;	

}
#endif
/**
 * ****************************************************************************
 * main function
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
int __attribute__ ((section (".func__main"))) main(void);
#if EN_FULL_USB
extern void USB_CODE usb_slave_init(void);
#endif
int main(void)
{
	DisableAllInterrupt();
    _init_sp_();
    Init_Timers();
#if EN_FULL_USB
#else
    Init_USB();	//20170310,Tony
#endif	
    //Core_Initialization();
	service_OEM_1();
    InitEnableInterrupt();
#if EN_FULL_USB
    usb_slave_init();
#endif
    
    BSRAM80 = 0;

	while(1)
   	{
        if(OEM_SkipMainServiceFunc()==Normal_MainService)
        {
        	#ifdef DEF_PCUSTOM_MAIN_SERVICE
        	oem_main_service();
        	#else /* #ifdef DEF_PCUSTOM_MAIN_SERVICE #else */
    		main_service();
    		EnableModuleInterrupt();
    		_nop_();
    		_nop_();
    		_nop_();
    		_nop_();

            /* No pending service event. */
            if(CheckEventPending()==0x00)
    		{                
                #if DEF_EC_FIRMWARE_DOZE_SUPPORT
                PowerM_Doze();
                #endif
                
                _nop_();
    		    _nop_();
    		    _nop_();
    		    _nop_();
            }
            #endif /* #ifdef DEF_PCUSTOM_MAIN_SERVICE #endif */
        }
  	} 

    return(0x0001);
}

/*
 * ****************************************************************************
 * table of main_service function
 * ****************************************************************************
 */
const FUNCT_PTR_V_V _CONST_AC service_table[16] =
{
#if EN_FULL_USB
	service_usb_isr,		/* usb interrupt service routine */
	service_extend_task,    /* service hid event */	
#else
	service_reserved,
	service_reserved,	
#endif
	service_reserved,
	service_reserved,
    service_reserved,
	service_reserved,
    service_1mS,            /* 1 millisecond elapsed */
 	service_reserved,

    service_reserved,
    service_reserved,
    service_reserved,
    service_reserved,
	service_OEM_1,		// Bu1
	service_reserved,	//service_OEM_2,
	service_reserved,	//service_OEM_3,
	service_reserved,	//service_OEM_4,
}; 

/**
 * ****************************************************************************
 * main service function
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void main_service(void) 
{
    ulong_32 service_index;

    /* service event not free. */

    while(CheckEventPending()==0x01)
    {

        for(service_index = 0;
            service_index<(sizeof(service_table)/sizeof(FUNCT_PTR_V_V));
            service_index++)
        {
            /* new event need service */
            if(F_Service_All[service_index]!=0x00)
            {
                F_Service_All[service_index]=0x00;

                /* Dispatch to service handler */
                (service_table[service_index])();

                //#ifdef DEF_SERVICE_ROUTINE_AFTER_ANY_EVENT
                //service_routine_after_any_event();
                //#endif
                
                #if DEF_MAIN_SERVICE_WITH_PRIORITY
                break;
                #endif
            }
        }
    }
}

/**
 * ****************************************************************************
 * reserved service function
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void service_reserved(void)
{

}

/**
 * ****************************************************************************
 * core 1ms service function
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void service_1mS(void)
{
    Timer1msEvent();

	if(ITempB01 > 50) ITempB01 = 0x00;
	else ITempB01++;
	Timer1msCnt++;
    if(Timer1msCnt>=10)
    {
        Timer1msCnt = 0x00;
    }
    
     
    if(Service_Timer10msEventA)
    {
        Service_Timer10msEventA=0;
        Timer10msEventA();
    }

}

/**
 * ****************************************************************************
 * 1ms service function
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Timer1msEvent(void)
{
    Hook_Timer1msEvent(Timer1msCnt);
}

/**
 * ****************************************************************************
 * 10ms service function A
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void Timer10msEventA(void)
{
    if(POWER_SAVING_MODE_DELAY>0)
    {
        POWER_SAVING_MODE_DELAY--;
    }
}

/**
 * ****************************************************************************
 * To check service event free or not.
 *
 * @return
 * 0x01, service event not free.
 * 0x00, No pending service event.
 *
 * @parameter
 *
 * ****************************************************************************
 */
BYTE CheckEventPending(void)
{
    if((F_Service_0 != 0x00)||
        (F_Service_1 != 0x00)||
        (F_Service_2 != 0x00)||
        (F_Service_3 != 0x00))
    {
        return(0x01);
    }
    else
    {
        return(0x00);
    }
}

/**
 * ****************************************************************************
 * handle hid_main()
 *
 * @return
 *
 * @parameter
 *
 * note, 1 ms time based
 *
 * ****************************************************************************
 */
void service_extend_task(void)
{
    if(Hook_Start_EC_Handle_Task()==0xFF)
    {
        //EC_Handle_Task();
    }
}

