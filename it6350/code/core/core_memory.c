/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_memory.c
 * Dino Li
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

/*
* *****************************************************************************
* [Kernel Memory Rang]
* 0x00080000 ~ 0x000803FF 
* 0x00080000 ~ 0x000801FF 512 bytes for kernel firmware
* 0x00080200 ~ 0x000802FF 256 bytes reserved
* 0x00080300 ~ 0x000803FF 256 bytes for ramdebug function
*
* [Other Memory Rang]
* 0x00080400 ~ 0x000817FF 5K bytes
* 1. linker
* 2. stack
* 3. OEM memory
*
* *****************************************************************************
*/

/* 
 * ****************************************************************************
 * global variables
 * ****************************************************************************
 */
const CBYTE *Scanner_Table_Pntr;
const CBYTE *Extendkey_Table_Pntr;

