/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_power.c
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

/**
 * ****************************************************************************
 * To read variable [RSTStatus] to get EC reset source
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void CheckResetSource(void)
{
    RSTStatus = RSTS;
}

/**
 * ****************************************************************************
 * doze mode
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void PowerM_Doze(void)
{
    BYTE   ucTemp;
    
    PLLCTRL = 0x00;             /* PLLCTRL = 00b, only cpu clock gating */
    ucTemp  = PLLCTRL;          /* dummy read */
    __nds32__standby_wake_grant();
}

/**
 * ****************************************************************************
 * deep doze mode
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void PowerM_DeepDoze(void)
{
    BYTE   ucTemp;

    /* No SOF, reset, resume, and transation event bit. */
    if(0 == (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & 0x0F))
    {
        PLLCTRL = 0x03;     /* PLLCTRL = 11b, no disable pll, gating pll out */
        ucTemp  = PLLCTRL;  /* dummy read */
        __nds32__standby_wake_grant();
    }
}

/**
 * ****************************************************************************
 * sleep mode
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void PowerM_Sleep(void)
{
    BYTE   ucTemp;

    /* No SOF, reset, resume, and transation event bit. */
    if(0 == (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & 0x0F))
    {
        PLLCTRL = 0x01;         /* PLLCTRL = 01b, disable pll */
        ucTemp  = PLLCTRL;      /* dummy read */
        __nds32__standby_wake_grant();
    }
}

/**
 * ****************************************************************************
 * Changing PLL frequency function
 *
 * @return
 *
 * @parameter
 * ("[0]  8 MHz)
 * ("[1] 16 MHz)
 * ("[2] 24 MHz)
 * ("[3] 32 MHz)
 * ("[4] 48 MHz(Default))
 * ("[5] 64 MHz)
 * ("[6] 72 MHz)
 * ("[7] 96 MHz)
 *
 * ****************************************************************************
 */
void ChangePLLFrequency(BYTE newseting)
{
    cpu_clock_select_ext(newseting);
}

