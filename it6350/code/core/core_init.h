/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * core_init.h
 * Dino Li
 * ****************************************************************************
 */
 
#ifndef CORE_INIT_H
#define CORE_INIT_H

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define all 	            byte
#define SCROLL   	        bit0
#define NUM      	        bit1
#define CAPS      	        bit2
#define OVL     	        bit3
#define LED4    	        bit4
#define LED5     	        bit5
#define LED6    	        bit6
#define LED7    	        bit7

#define LED_CTRL_INIT 		(MASK(SCROLL) | MASK(NUM) | MASK(CAPS) | MASK(OVL))
#define SAVE_KBD_STATE_INIT (maskAUXENAB + (2 << shiftCODESET))
#define EXT_CB2_INIT    	0x22		
#define SAVE_TYPEMATIC_INIT 0x2A

/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void Core_Initialization(void);

/* 
 * ****************************************************************************
 * structure
 * ****************************************************************************
 */
typedef struct REG_INIT
{
    uchar_8 	*address;
    uchar_8  	initdata;
} sREG_INIT;

#endif

