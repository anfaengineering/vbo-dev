/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * api_gpio.h
 * Dino Li
 * Version, 1.00
 * Note, To link [api_xxx.o] if related api function be used.
 * ****************************************************************************
 */

#ifndef API_GPIO_H
#define API_GPIO_H

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
/* GPIO ctrl define */

/* Alternate function */
#define ALT                     0x00

/* Input */
#define INPUT                   0x80

/* Output */
#define OUTPUT                  0x40

/* Port pin pull up */
#define PULL_UP                 0x04

/* Port pin pull down */
#define PULL_DW                 0x02

/* No output type control register */
#define OutputType_None         0x00

/* Push-pull output */
#define OutputType_Push_Pull    0x01

/* Open-drain output */
#define OutputType_Open_Drain   0x02

/* No output type register with this pin */
#define NoCtrlReg    (uchar_8 *) 0xFFFF

/* GPIO pin output toggle */
#define OutputMode_Inverse      0x02

/* GPIO pin output high */
#define OutputMode_High         0x01

/* GPIO pin output low */
#define OutputMode_Low          0x00

/* GPIO pin status high */
#define InputMode_High          0x01

/* GPIO pin status low */
#define InputMode_Low           0x00

/* GPIO port data mirror register high */
#define Mirror_High             0x01

/* GPIO port data mirror register low */
#define Mirror_Low              0x00

/* 
 * ****************************************************************************
 * GPIO Group & Pin Assignments
 * ****************************************************************************
 */
/*
 * ****************************************************************************
 *      Group A Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOA0 0x00 //  24  PWM0    WUI43   None    16mA        Up/Dn       GPI
#define GPIOA1 0x01 //  25  PWM1    WUI44   None    16mA        Up/Dn       GPI
#define GPIOA4 0x02 //  30  PWM4    WUI33   None    8mA         Up/Dn       GPI
#define GPIOA5 0x03 //  31  PWM5    WUI34   None    8mA         Up/Dn       GPI
#define GPIOA6 0x04 //  32  PWM6    WUI35   SSCK    8mA         Up/Dn       GPI

/*
 * ****************************************************************************
 *      Group B Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOB0 0x05 //  108 RXD     WUI53   SIN0    16mA        Up/Dn       GPI
#define GPIOB1 0x06 //  109 TXD     WUI54   SOUT0   16mA        Up/Dn       GPI
#define GPIOB3 0x07 //  110 SMCLK0  WUI55   None    4mA         Up/Dn       GPI
#define GPIOB4 0x08 //  111 SMDAT0  WUI46   None    4mA         Up/Dn       GPI

/*
 * ****************************************************************************
 *      Group C Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOC3 0x09 //  56  KSO16   WUI60   SMOSI   8mA         Up/Dn       GPI
#define GPIOC5 0x0A //  57  KSO17   WUI61   SMISO   8mA         Up/Dn       GPI

/*
 * ****************************************************************************
 *      Group E Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOE0 0x0B //  19  L80HLAT WUI24   BAO     16mA        Up/Dn       GPI
#define GPIOE4 0x0C //  125 PWRSW   WUI66   None    2mA         Up/Dn       GPI
#define GPIOE7 0x0D //  20  L80LLAT WUI7    None    16mA        Up/Dn       GPI

/*
 * ****************************************************************************
 *      Group F Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOF6 0x0E //  117 SMCLK2  WUI22   PECI    4mA         Up/Dn       GPI
#define GPIOF7 0x0F //  118 SMDAT2  WUI23   PECIRQT 4mA         Up/Dn       GPI

/*
 * ****************************************************************************
 *      Group G Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOG2 0x10 //  100 None    WUI69   SSCE0#  4mA         Up/Dn       GPI
#define GPIOG3 0x11 //  101 FSCE#   None    None    8mA (output only)       GPI
#define GPIOG4 0x12 //  102 FMOSI   None    None    8mA (output only)       GPI
#define GPIOG5 0x13 //  103 FMISO   None    None        (input only)        GPI
#define GPIOG6 0x14 //  104 FDIO3   WUI70   DSR0#   4mA         Up/Dn       GPI
#define GPIOG7 0x15 //  105 FSCK    None    None    8mA (output only)       GPI

/*
 * ****************************************************************************
 *      Group H Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOH5 0x16 //  98  HMISO   WUI41   None    16mA        Up/Dn   GPI/ID5
#define GPIOH6 0x17 //  99  HMOSI   WUI42   None    16mA        Up/Dn   GPI/ID6

/*
 * ****************************************************************************
 *      Group I Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOI0 0x18 //  66  ADC0    WUI71   None    2mA         None        GPI
#define GPIOI1 0x19 //  67  ADC1    WUI72   None    2mA         None        GPI
#define GPIOI2 0x1A //  68  ADC2    WUI73   None    2mA         None        GPI
#define GPIOI3 0x1B //  69  ADC3    WUI74   None    2mA         None        GPI
#define GPIOI4 0x1C //  70  ADC4    WUI28   None    2mA         None        GPI
#define GPIOI5 0x1D //  71  ADC5    WUI29   DCD1#   2mA         None        GPI

/*
 * ****************************************************************************
 *      Group J Index   Pin Func1   Func2   Func3   Output      Pull_Cap    Def
 *                                                              Driving
 * ****************************************************************************
 */
#define GPIOJ6 0x1E //  128 CK32K   WUI86   None    2mA         None      Func1
#define GPIOJ7 0x1F //  2   CK32KE  None    None    2mA         None      Func1

#define GPIO_MAX	(0x20)
/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void GPIO_Operation_Mode(BYTE pin_index, BYTE pin_mode,
    BYTE pin_output_type);
extern void GPIO_Output_Ctrl(BYTE pin_index, BYTE pin_output_mode);
extern BYTE GPIO_Input_Status_Get(BYTE pin_index);
extern BYTE GPIO_Mirror_Status_Get(BYTE pin_index);

/* 
 * ****************************************************************************
 * GPIO configuration registers struct
 * ****************************************************************************
 */
typedef struct GPIOConfReg
{
    CBYTE    GPIO_Index;
    CBYTE    GPIO_Ctrl_BIT;	
	uchar_8	*GPIO_DataReg;
    uchar_8	*GPIO_CtrlReg;	
    uchar_8	*GPIO_MirrorReg;
    uchar_8	*GPIO_OutputTypeReg;
} sGPIOConfReg;

#endif
