/* 
 * ****************************************************************************
 * hal.h
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

#ifndef __HAL_H__
#define __HAL_H__

#include "../../nds/include/os_cpu.h"
#include "../../nds/include/nds32_regs.h"
#include "../../nds/include/mem_layout.h"

/* 
 * ****************************************************************************
 * Function prototype
 * ****************************************************************************
 */
inline void hal_usb_irq(void);
inline void hal_WUI_USBDminus_irq(void);
inline void hal_WUI_USBDplus_irq(void);
extern void vApplicationTickHook(void);
extern void EC_Handle_Task(void);
extern void usb_isr_service_flag(void);
extern void service_usb_isr(void);
extern void usb_resume_k(void);
extern void usb_resume_k_se0(void);

#endif

