/* 
 * ****************************************************************************
 * hal.c
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

/**
 * ****************************************************************************
 * FUNCTION : hal_WUI_USBDminus_irq
 * Wake-up interrupt using USB D- interrupt service routine.
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
inline void RTOS_CODE_H hal_WUI_USBDminus_irq(void)
{
    /* INT86 GPDRH5 WUI interrupt mask disable */
    IER10 &= ~BIT6;

    /* clear interrupt */
    WUESR9 = BIT1;
    
#if EN_FULL_USB
    if(usb_slave_is_shutdown())
    {
        if(!usb_slave_resume(USB_RESUME_RESUME))
            IER10 |= BIT6;                
    }  
#endif
}

/**
 * ****************************************************************************
 * FUNCTION : hal_WUI_USBDplus_irq
 * WWake-up interrupt using USB D+ interrupt service routine.
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
inline void RTOS_CODE_H hal_WUI_USBDplus_irq(void)
{
    /* INT87 GPDRH5 WUI interrupt mask disable */
    IER10 &= ~BIT7;

    /* clear interrupt */
    WUESR9 = BIT2;

    /*
    if(usb_slave_is_shutdown())
    {    
        if(!usb_slave_resume(USB_RESUME_RESET))
            IER10 |= BIT7;
    }
    */
}

/**
 * ****************************************************************************
 * HID (USB or I2C) interrupt service routine.
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
inline void RTOS_CODE_H hal_usb_irq(void)
{
#if EN_FULL_USB
    hid_isr();
#endif	
}

/**
 * ****************************************************************************
 * HID over USB/I2C task
 *
 * @return
 *
 * @parameter
 *
 * @note, 1 ms time based
 *
 * ****************************************************************************
 */
void RTOS_CODE_H EC_Handle_Task(void)
{
#if EN_FULL_USB	
    BYTE l_skip_handle_usb_interface;

    l_skip_handle_usb_interface = 0;

    /* 1ms time based */
    if(EXTEND_EVENT_ISR_SERVICE_FLAG)
    {
        EXTEND_EVENT_ISR_SERVICE_FLAG = 0;
        vApplicationTickHook();
        EC_HANDLE_HID_TASK_STATUS = 0x01;
        
        if(l_skip_handle_usb_interface==0)
        {
            hid_main();
        }
        EC_HANDLE_HID_TASK_STATUS = 0x00;

        EC_HANDLE_TASK_INDEX++;
    }
#endif

}

/**
 * ****************************************************************************
 * 1ms application hook function
 *
 * @return
 *
 * @parameter
 *
 * @note, 1 ms time based
 *
 * ****************************************************************************
 */
void RTOS_CODE_H vApplicationTickHook(void)
{   
    IER2 &= ~BIT7;
    _nop_();
    _nop_();
    _nop_();
    _nop_();
#if EN_FULL_USB
    
    if(POWER_SAVING_MODE_DELAY==0)
    {
        usb_slave_check_suspend();
    }
    else
    {
        usb_slave_check_sof();
    }

    if(hid_time_interval())
    {

    }
 
    if( !usb_slave_is_shutdown() )
    {

    }
    else
    {
        /* for wakeup host */
        usb_slave_resume(USB_RESUME_REMOTEWAKEUP);
    }
#endif
    
    IER2 |= BIT7;
}
#if EN_FULL_USB

/**
 * ****************************************************************************
 *  usb isr service flag
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void usb_isr_service_flag(void)
{
    /* service flag for "usb_slave_main();" function */
    USB_ISR_SERVICE_FLAG = 1;
}

/**
 * ****************************************************************************
 * usb interrupt service routine
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void service_usb_isr(void)
{
    IER2 &= ~BIT7;
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    usb_slave_main();
    IER2 |= BIT7;
    

}

/**
 * ****************************************************************************
 * 2 ms resume k
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
__attribute__((optimize("O0")))
void usb_resume_k(void)
{
    DisableAllInterrupt();
    REG_WRITE_8BIT(SC_CONTROL_REG, 0x0A);
    InstructionDelayXms(2);
    REG_WRITE_8BIT(SC_CONTROL_REG, 0x31);
    EnableAllInterrupt();
     USB_REMOTE_WAKEUP_MONITOR_T++;
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
__attribute__((optimize("O0")))
void usb_resume_k_se0(void)
{
    DisableAllInterrupt();
    REG_WRITE_8BIT(SC_CONTROL_REG, 0x08);
    InstructionDelayXms(2);
    REG_WRITE_8BIT(SC_CONTROL_REG, 0x31);
    EnableAllInterrupt();
    USB_REMOTE_WAKEUP_MONITOR++;
}
#endif
