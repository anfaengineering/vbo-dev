/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * api_wuc.c
 * Dino Li
 * Version, 1.10
 * Note, To link [api_xxx.o] if related api function be used.
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

/* 
 * ****************************************************************************
 * WUI Input Controlling Table
 * ****************************************************************************
 */
const sWUCControlReg _CONST_3B8 asWUCControlReg[WU_MAX] = 
{
/*
 * Index, BIT[x],	WUEMR[x], WUESR[[x], WUENR[x], 
 * IER[x], ISR[x], BIT[x] of IER[x] and ISR[x]
 */
/* Group 1 */
/* Group 2 */
{ WU25, BIT5, Int_WKO25, &WUEMR2, &WUESR2, WUC_Reserved, &IER1, &ISR1},

/* Group 3 */
/* Group 4 */
{ WU46, BIT6, Int_WKINTAD, &WUEMR4, &WUESR4, &WUENR4, &IER0, &ISR0},

/* Group 5 */
/* Group 6 */
{ WU66, BIT6, Int_WKO66 ,&WUEMR6, &WUESR6, WUC_Reserved, &IER6, &ISR6},
{ WU67, BIT7, Int_WKO67 ,&WUEMR6, &WUESR6, WUC_Reserved, &IER6, &ISR6},

/* Group 7 */
{ WU70, BIT0, Int_WKO70, &WUEMR7, &WUESR7, WUC_Reserved, &IER9, &ISR9},
{ WU74, BIT4, Int_WKO74, &WUEMR7, &WUESR7, WUC_Reserved, &IER9, &ISR9},
{ WU75, BIT5, Int_WKO75, &WUEMR7, &WUESR7, WUC_Reserved, &IER9, &ISR9},

/* Group 8 */
{ WU81, BIT1, Int_WKO81, &WUEMR8, &WUESR8, WUC_Reserved, &IER11, &ISR11},
{ WU82, BIT2, Int_WKO82, &WUEMR8, &WUESR8, WUC_Reserved, &IER11, &ISR11},
{ WU83, BIT3, Int_WKO83, &WUEMR8, &WUESR8, WUC_Reserved, &IER11, &ISR11},

/* Group 9 */
{ WU89, BIT1, Int_WKO89, &WUEMR9, &WUESR9, WUC_Reserved, &IER10, &ISR10},
{ WU90, BIT2, Int_WKO90, &WUEMR9, &WUESR9, WUC_Reserved, &IER10, &ISR10},
{ WU91, BIT3, Int_WKO91, &WUEMR9, &WUESR9, WUC_Reserved, &IER12, &ISR12},
{ WU92, BIT4, Int_WKO92, &WUEMR9, &WUESR9, WUC_Reserved, &IER12, &ISR12},
{ WU94, BIT6, Int_WKO94, &WUEMR9, &WUESR9, WUC_Reserved, &IER12, &ISR12},

/* Group 10 */
{ WU101, BIT5, Int_WKO101, &WUEMR10, &WUESR10, WUC_Reserved, &IER13, &ISR13},
{ WU102, BIT6, Int_WKO102, &WUEMR10, &WUESR10, WUC_Reserved, &IER13, &ISR13},
{ WU103, BIT7, Int_WKO103, &WUEMR10, &WUESR10, WUC_Reserved, &IER13, &ISR13},

/* Group 11 */
{ WU108, BIT4, Int_WKO108, &WUEMR11, &WUESR11, WUC_Reserved, &IER14, &ISR14},
{ WU109, BIT5, Int_WKO109, &WUEMR11, &WUESR11, WUC_Reserved, &IER14, &ISR14},

/* Group 12 */
{ WU114, BIT2, Int_WKO114, &WUEMR12, &WUESR12, WUC_Reserved, &IER14, &ISR14},
{ WU117, BIT5, Int_WKO117, &WUEMR12, &WUESR12, WUC_Reserved, &IER15, &ISR15},
{ WU118, BIT6, Int_WKO118, &WUEMR12, &WUESR12, WUC_Reserved, &IER15, &ISR15},
{ WU119, BIT7, Int_WKO119, &WUEMR12, &WUESR12, WUC_Reserved, &IER15, &ISR15},

/* Group 13 */
{ WU120, BIT0, Int_WKO120, &WUEMR13, &WUESR13, WUC_Reserved, &IER15, &ISR15},
{ WU121, BIT1, Int_WKO121, &WUEMR13, &WUESR13, WUC_Reserved, &IER15, &ISR15},
{ WU122, BIT2, Int_WKO122, &WUEMR13, &WUESR13, WUC_Reserved, &IER15, &ISR15},

/* Group 14 */
{ WU134, BIT6, Int_WKO134, &WUEMR14, &WUESR14, WUC_Reserved, &IER16, &ISR16},
};

/**
 * ****************************************************************************
 * The function for enabling interrupt of WUC input.
 *
 * @return
 *
 * @parameter
 * wuc_input,
 * WU10  || WU11  || WU12  || WU13  || WU14  || WU15  || WU16  || WU17 ||
 * WU20  || WU21  || WU22  || WU23  || WU24  || WU25  || WU26  || WU27 ||
 * WU30  || WU31  || WU32  || WU33  || WU34  || WU35  || WU36  || WU37 ||
 * WU40  || WU41  || WU42  || WU43  || WU44  || WU45  || WU46  || WU47 ||
 * WU50  || WU51  || WU52  || WU53  || WU54  || WU55  || WU56  || WU57 ||
 * WU60  || WU61  || WU62  || WU63  || WU64  || WU65  || WU66  || WU67 ||
 * WU70  || WU71  || WU72  || WU73  || WU74  || WU75  || WU76  || WU77 ||
 * WU80  || WU81  || WU82  || WU83  || WU84  || WU85  || WU86  || WU87 ||
 * WU88  || WU89  || WU90  || WU91  || WU92  || WU93  || WU94  || WU95 ||
 * WU96  || WU97  || WU98  || WU99  || WU100 || WU101 || WU102 || WU103 ||
 * WU104 || WU105 || WU106 || WU107 || WU108 || WU109 || WU110 || WU111 ||
 * WU112 || WU113 || WU114 || WU115 || WU116 || WU117 || WU118 || WU119 ||
 * WU120 || WU121 || WU122 || WU123 || WU124 || WU125 || WU126 || WU127 ||
 * WU128 || WU129 || WU130 || WU131 || WU132 || WU133 || WU134 || WU135 ||
 * interrupt_mode,
 * WUC_Rising || WUC_Falling
 *
 * ****************************************************************************
 */
void WUC_Enable_WUx_Interrupt(BYTE wuc_input, BYTE interrupt_mode)
{
    BYTE l_wuc_ctrl;
    BYTE l_intc_ctrl;

    l_wuc_ctrl = asWUCControlReg[wuc_input].WUC_Ctrl_BIT;
    l_intc_ctrl = asWUCControlReg[wuc_input].INTC_Ctrl_BIT;

    /* WUC input trigger mode setting */
    if(interrupt_mode == WUC_Falling)
    {
        /*
         * WUC falling edge or either edge trigger
         * either edge only for group 7, 10, 12
         */
        (*asWUCControlReg[wuc_input].WUC_WUEMRx) |= l_wuc_ctrl;
    }
    else
    {
        /* WUC rising trigger */
        (*asWUCControlReg[wuc_input].WUC_WUEMRx) &= ~l_wuc_ctrl;
    }

    /* W/C WUESRx corresponding bit */
    (*asWUCControlReg[wuc_input].WUC_WUESRx) = l_wuc_ctrl;

    /* Need enabling WUENRx */
    if(asWUCControlReg[wuc_input].WUC_WUENRx != WUC_Reserved)
    {
        // Enable wake-up interrupt
        (*asWUCControlReg[wuc_input].WUC_WUENRx) |= l_wuc_ctrl;
    }
    
    /* IERx : keep high level trigger */

    /* W/C ISRx corresponding bit */
    (*asWUCControlReg[wuc_input].INTC_ISRx) = l_intc_ctrl;

    /* Enable INTC */
    (*asWUCControlReg[wuc_input].INTC_IERx) |= l_intc_ctrl;
}

/**
 * ****************************************************************************
 * The function for disabling interrupt of WUC input.
 *
 * @return
 *
 * @parameter
 * wuc_input,
 * WU10  || WU11  || WU12  || WU13  || WU14  || WU15  || WU16  || WU17 ||
 * WU20  || WU21  || WU22  || WU23  || WU24  || WU25  || WU26  || WU27 ||
 * WU30  || WU31  || WU32  || WU33  || WU34  || WU35  || WU36  || WU37 ||
 * WU40  || WU41  || WU42  || WU43  || WU44  || WU45  || WU46  || WU47 ||
 * WU50  || WU51  || WU52  || WU53  || WU54  || WU55  || WU56  || WU57 ||
 * WU60  || WU61  || WU62  || WU63  || WU64  || WU65  || WU66  || WU67 ||
 * WU70  || WU71  || WU72  || WU73  || WU74  || WU75  || WU76  || WU77 ||
 * WU80  || WU81  || WU82  || WU83  || WU84  || WU85  || WU86  || WU87 ||
 * WU88  || WU89  || WU90  || WU91  || WU92  || WU93  || WU94  || WU95 ||
 * WU96  || WU97  || WU98  || WU99  || WU100 || WU101 || WU102 || WU103 ||
 * WU104 || WU105 || WU106 || WU107 || WU108 || WU109 || WU110 || WU111 ||
 * WU112 || WU113 || WU114 || WU115 || WU116 || WU117 || WU118 || WU119 ||
 * WU120 || WU121 || WU122 || WU123 || WU124 || WU125 || WU126 || WU127 ||
 * WU128 || WU129 || WU130 || WU131 || WU132 || WU133 || WU134 || WU135 ||
 *
 * ****************************************************************************
 */
void WUC_Disable_WUx_Interrupt(BYTE wuc_input)
{
    BYTE l_wuc_ctrl;
    BYTE l_intc_ctrl;

    l_wuc_ctrl = asWUCControlReg[wuc_input].WUC_Ctrl_BIT;
    l_intc_ctrl = asWUCControlReg[wuc_input].INTC_Ctrl_BIT;

    /* Disable INTC */
    (*asWUCControlReg[wuc_input].INTC_IERx) &= ~l_intc_ctrl;

    /* W/C WUESRx corresponding bit */
    (*asWUCControlReg[wuc_input].WUC_WUESRx) = l_wuc_ctrl;

    /* W/C ISRx corresponding bit */
    (*asWUCControlReg[wuc_input].INTC_ISRx) = l_intc_ctrl;
    
}
#if 0
/**
 * ****************************************************************************
 * ISR of WUC input.
 *
 * @return
 *
 * @parameter
 * wuc_input,
 * WU10  || WU11  || WU12  || WU13  || WU14  || WU15  || WU16  || WU17 ||
 * WU20  || WU21  || WU22  || WU23  || WU24  || WU25  || WU26  || WU27 ||
 * WU30  || WU31  || WU32  || WU33  || WU34  || WU35  || WU36  || WU37 ||
 * WU40  || WU41  || WU42  || WU43  || WU44  || WU45  || WU46  || WU47 ||
 * WU50  || WU51  || WU52  || WU53  || WU54  || WU55  || WU56  || WU57 ||
 * WU60  || WU61  || WU62  || WU63  || WU64  || WU65  || WU66  || WU67 ||
 * WU70  || WU71  || WU72  || WU73  || WU74  || WU75  || WU76  || WU77 ||
 * WU80  || WU81  || WU82  || WU83  || WU84  || WU85  || WU86  || WU87 ||
 * WU88  || WU89  || WU90  || WU91  || WU92  || WU93  || WU94  || WU95 ||
 * WU96  || WU97  || WU98  || WU99  || WU100 || WU101 || WU102 || WU103 ||
 * WU104 || WU105 || WU106 || WU107 || WU108 || WU109 || WU110 || WU111 ||
 * WU112 || WU113 || WU114 || WU115 || WU116 || WU117 || WU118 || WU119 ||
 * WU120 || WU121 || WU122 || WU123 || WU124 || WU125 || WU126 || WU127 ||
 * WU128 || WU129 || WU130 || WU131 || WU132 || WU133 || WU134 || WU135 ||
 *
 * ****************************************************************************
 */
void WUC_WUx_ISR(BYTE wuc_input)
{
    BYTE l_wuc_ctrl;
    BYTE l_intc_ctrl;

    l_wuc_ctrl = asWUCControlReg[wuc_input].WUC_Ctrl_BIT;
    l_intc_ctrl = asWUCControlReg[wuc_input].INTC_Ctrl_BIT;

    /* Disable INTC */
    (*asWUCControlReg[wuc_input].INTC_IERx) &= ~l_intc_ctrl;

    /* W/C WUESRx corresponding bit */
    (*asWUCControlReg[wuc_input].WUC_WUESRx) = l_wuc_ctrl;

    /* W/C ISRx corresponding bit */
    (*asWUCControlReg[wuc_input].INTC_ISRx) = l_intc_ctrl;

    /* Enable INTC again */
    (*asWUCControlReg[wuc_input].INTC_IERx) |= l_intc_ctrl;

}

/**
 * ****************************************************************************
 * The function for enabling IERx of WUC only
 *
 * @return
 *
 * @parameter
 * wuc_input,
 * WU10  || WU11  || WU12  || WU13  || WU14  || WU15  || WU16  || WU17 ||
 * WU20  || WU21  || WU22  || WU23  || WU24  || WU25  || WU26  || WU27 ||
 * WU30  || WU31  || WU32  || WU33  || WU34  || WU35  || WU36  || WU37 ||
 * WU40  || WU41  || WU42  || WU43  || WU44  || WU45  || WU46  || WU47 ||
 * WU50  || WU51  || WU52  || WU53  || WU54  || WU55  || WU56  || WU57 ||
 * WU60  || WU61  || WU62  || WU63  || WU64  || WU65  || WU66  || WU67 ||
 * WU70  || WU71  || WU72  || WU73  || WU74  || WU75  || WU76  || WU77 ||
 * WU80  || WU81  || WU82  || WU83  || WU84  || WU85  || WU86  || WU87 ||
 * WU88  || WU89  || WU90  || WU91  || WU92  || WU93  || WU94  || WU95 ||
 * WU96  || WU97  || WU98  || WU99  || WU100 || WU101 || WU102 || WU103 ||
 * WU104 || WU105 || WU106 || WU107 || WU108 || WU109 || WU110 || WU111 ||
 * WU112 || WU113 || WU114 || WU115 || WU116 || WU117 || WU118 || WU119 ||
 * WU120 || WU121 || WU122 || WU123 || WU124 || WU125 || WU126 || WU127 ||
 * WU128 || WU129 || WU130 || WU131 || WU132 || WU133 || WU134 || WU135 ||
 *
 * @note, To enable IERx only, no w/c ISRx
 *
 * ****************************************************************************
 */
void WUC_Enable_WUx_Interrupt_IERx_Only(BYTE wuc_input)
{
    BYTE l_intc_ctrl;

    l_intc_ctrl = asWUCControlReg[wuc_input].INTC_Ctrl_BIT;

    /* Enable INTC */
    (*asWUCControlReg[wuc_input].INTC_IERx) |= l_intc_ctrl;
}
#endif
