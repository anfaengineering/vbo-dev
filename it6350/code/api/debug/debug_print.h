/* 
 * ****************************************************************************
 * debug_print.h
 * Copyright (c) ITE, All Rights Reserved
 *
 * Implementation Uart interrupt
 *
 * ****************************************************************************
 */

#ifndef __DEBUG_PRINT__ /* { */
#define __DEBUG_PRINT__

/* 
 * ****************************************************************************
 * Function prototype
 * ****************************************************************************
 */
extern void InitDebugPrint(void);

#ifdef __ENABLE_DBG_MSG__
extern int  rtl_printf(const char *format, ...);
#else /* #ifdef __ENABLE_DBG_MSG__ #else */
extern int  rtl_printf(const char *format, ...);                   
#endif /* #ifdef __ENABLE_DBG_MSG__ #endif */

/* 
 * ****************************************************************************
 * Debug API
 * ****************************************************************************
 */
 #ifdef __ENABLE_DBG_MSG__
#define RTL_PRINTF(fmt, arg...)     rtl_printf(fmt, ##arg)
#define printk                      RTL_PRINTF
#define printf                      RTL_PRINTF
#define sprintf(buf, fmt, arg...)   rtl_sprintf(buf, fmt, ##arg)
#else
#define RTL_PRINTF(fmt, arg...)     
#define printk                      RTL_PRINTF
#define printf                      RTL_PRINTF
#endif
#endif /* } __DEBUG_PRINT__ */

