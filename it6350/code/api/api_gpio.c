/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * api_gpio.c
 * Dino Li
 * Version, 1.00
 * Note, To link [api_xxx.o] if related api function be used.
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

/* 
 * ****************************************************************************
 * GPIO Configuration Table
 * ****************************************************************************
 */
const sGPIOConfReg  asGPIOConfReg[GPIO_MAX] = 
{
    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type */
    /* Group A */
 	{ GPIOA0,   BIT0,	&GPDRA,     &GPCRA0,	    &GPDMRA,    &GPOTA		},
    { GPIOA1,   BIT1,	&GPDRA,     &GPCRA1,	    &GPDMRA,    &GPOTA		},
    { GPIOA4,   BIT4,	&GPDRA,     &GPCRA4,        &GPDMRA,    NoCtrlReg	},
    { GPIOA5,   BIT5,	&GPDRA,     &GPCRA5,	    &GPDMRA,    NoCtrlReg	},
    { GPIOA6,   BIT6,	&GPDRA,     &GPCRA6,	    &GPDMRA,    NoCtrlReg	},

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group B */
 	{ GPIOB0,   BIT0,	&GPDRB,     &GPCRB0,	    &GPDMRB,    &GPOTB		},
    { GPIOB1,   BIT1,	&GPDRB,     &GPCRB1,	    &GPDMRB,    &GPOTB		},
    { GPIOB3,   BIT3,	&GPDRB,     &GPCRB3,	    &GPDMRB,    &GPOTB		},
    { GPIOB4,   BIT4,	&GPDRB,     &GPCRB4,        &GPDMRB,    &GPOTB		},

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group C */
    { GPIOC3,   BIT3,	&GPDRC,     &GPCRC3,	    &GPDMRC,    NoCtrlReg	},
    { GPIOC5,   BIT5,	&GPDRC,     &GPCRC5,	    &GPDMRC,    NoCtrlReg	},

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group D */

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group E */
 	{ GPIOE0,   BIT0,	&GPDRE,     &GPCRE0,	    &GPDMRE,    &GPOTE		},
    { GPIOE4,   BIT4,	&GPDRE,     &GPCRE4,        &GPDMRE,    &GPOTE		},
    { GPIOE7,   BIT7,	&GPDRE,     &GPCRE7,	    &GPDMRE,    &GPOTE		},

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group F */
    { GPIOF6,   BIT6,	&GPDRF,     &GPCRF6,	    &GPDMRF,    &GPOTF		},
    { GPIOF7,   BIT7,	&GPDRF,     &GPCRF7,	    &GPDMRF,    &GPOTF		},

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group G */
    { GPIOG2,	BIT2,   &GPDRG,     &GPCRG2,	    &GPDMRG,    NoCtrlReg    },
    { GPIOG3,	BIT3,   &GPDRG,     &GPCRG3,	    &GPDMRG,    NoCtrlReg    },
    { GPIOG4,	BIT4,   &GPDRG,     &GPCRG4,        &GPDMRG,    NoCtrlReg    },
    { GPIOG5,	BIT5,   &GPDRG,     &GPCRG5,	    &GPDMRG,    NoCtrlReg    },
    { GPIOG6,	BIT6,   &GPDRG,     &GPCRG6,	    &GPDMRG,    NoCtrlReg    },
    { GPIOG7,	BIT7,   &GPDRG,     &GPCRG7,	    &GPDMRG,    NoCtrlReg    },

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group H */
    { GPIOH5,	BIT5,   &GPDRH,     &GPCRH5,	    &GPDMRH,    &GPOTH		},
    { GPIOH6,	BIT6,   &GPDRH,     &GPCRH6,	    &GPDMRH,    &GPOTH		},

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group I */
 	{ GPIOI0,	BIT0,   &GPDRI,     &GPCRI0,	    &GPDMRI,    NoCtrlReg	},
    { GPIOI1,	BIT1,   &GPDRI,     &GPCRI1,	    &GPDMRI,    NoCtrlReg	},
    { GPIOI2,	BIT2,   &GPDRI,     &GPCRI2,	    &GPDMRI,    NoCtrlReg	},
    { GPIOI3,	BIT3,   &GPDRI,     &GPCRI3,	    &GPDMRI,    NoCtrlReg	},
    { GPIOI4,	BIT4,   &GPDRI,     &GPCRI4,        &GPDMRI,    NoCtrlReg	},
    { GPIOI5,	BIT5,   &GPDRI,     &GPCRI5,	    &GPDMRI,    NoCtrlReg	},

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group J */
    { GPIOJ6,	BIT6,   &GPDRJ,     &GPCRJ6,	    &GPDMRJ,    NoCtrlReg	},
    { GPIOJ7,	BIT7,   &GPDRJ,     &GPCRJ7,	    &GPDMRJ,    NoCtrlReg	},

    /* Index,   BIT[x],	Data Reg,   Control Reg,    Mirror Reg, Output Type  */
    /* Group M */
};

/**
 * ****************************************************************************
 * The function for selecting GPIO operation mode.
 *
 * @return
 *
 * @parameter
 * pin_index,
 * GPIOA0 || GPIOA1 || GPIOA2 || GPIOA3 || GPIOA4 || GPIOA5 || GPIOA6 || GPIOA7
 * GPIOB0 || GPIOB1 || GPIOB2 || GPIOB3 || GPIOB4 || GPIOB5 || GPIOB6 || GPIOB7
 * GPIOC0 || GPIOC1 || GPIOC2 || GPIOC3 || GPIOC4 || GPIOC5 || GPIOC6 || GPIOC7
 * GPIOD0 || GPIOD1 || GPIOD2 || GPIOD3 || GPIOD4 || GPIOD5 || GPIOD6 || GPIOD7
 * GPIOE0 || GPIOE1 || GPIOE2 || GPIOE3 || GPIOE4 || GPIOE5 || GPIOE6 || GPIOE7
 * GPIOF0 || GPIOF1 || GPIOF2 || GPIOF3 || GPIOF4 || GPIOF5 || GPIOF6 || GPIOF7
 * GPIOG0 || GPIOG1 || GPIOG2 || GPIOG3 || GPIOG4 || GPIOG5 || GPIOG6 || GPIOG7
 * GPIOH0 || GPIOH1 || GPIOH2 || GPIOH3 || GPIOH4 || GPIOH5 || GPIOH6 || GPIOH7
 * GPIOI0 || GPIOI1 || GPIOI2 || GPIOI3 || GPIOI4 || GPIOI5 || GPIOI6 || GPIOI7
 * GPIOJ0 || GPIOJ1 || GPIOJ2 || GPIOJ3 || GPIOJ4 || GPIOJ5 || GPIOJ6 || GPIOJ7
 * GPIOM0 || GPIOM1 || GPIOM2 || GPIOM3 || GPIOM4 || GPIOM5 || GPIOM6 || GPIOM7
 * pin_mode, [ ALT || INPUT || OUTPUT ] + [ PULL_UP || PULL_DW ]
 * pin_output_type,
 * OutputType_None || OutputType_Push_Pull || OutputType_Open_Drain
 *
 * ****************************************************************************
 */
void GPIO_Operation_Mode(BYTE pin_index, BYTE pin_mode, BYTE pin_output_type)
{
    /* To set GPIO operation mode. */
	if(pin_index < GPIO_MAX){
	    *asGPIOConfReg[pin_index].GPIO_CtrlReg = pin_mode;

	    /* If GPIO operation mode is output. */
	    if((pin_mode & OUTPUT) == OUTPUT)
	    {
	        /* Open-drain output */
	        if(pin_output_type == OutputType_Open_Drain)
	        {
	            *asGPIOConfReg[pin_index].GPIO_OutputTypeReg |=
	                asGPIOConfReg[pin_index].GPIO_Ctrl_BIT;
	        }
	        /* Push-pull output */
	        else if(pin_output_type == OutputType_Push_Pull)
	        {
	            *asGPIOConfReg[pin_index].GPIO_OutputTypeReg &=
	                ~asGPIOConfReg[pin_index].GPIO_Ctrl_BIT;
	        }
	        /* No output type control register */
	        else
	        {
	            /* Do nothing */
	        }
	    }
	}
}
#if 0
/**
 * ****************************************************************************
 * The function for GPIO output control.
 *
 * @return
 *
 * @parameter
 * pin_index,
 * GPIOA0 || GPIOA1 || GPIOA2 || GPIOA3 || GPIOA4 || GPIOA5 || GPIOA6 || GPIOA7
 * GPIOB0 || GPIOB1 || GPIOB2 || GPIOB3 || GPIOB4 || GPIOB5 || GPIOB6 || GPIOB7
 * GPIOC0 || GPIOC1 || GPIOC2 || GPIOC3 || GPIOC4 || GPIOC5 || GPIOC6 || GPIOC7
 * GPIOD0 || GPIOD1 || GPIOD2 || GPIOD3 || GPIOD4 || GPIOD5 || GPIOD6 || GPIOD7
 * GPIOE0 || GPIOE1 || GPIOE2 || GPIOE3 || GPIOE4 || GPIOE5 || GPIOE6 || GPIOE7
 * GPIOF0 || GPIOF1 || GPIOF2 || GPIOF3 || GPIOF4 || GPIOF5 || GPIOF6 || GPIOF7
 * GPIOG0 || GPIOG1 || GPIOG2 || GPIOG3 || GPIOG4 || GPIOG5 || GPIOG6 || GPIOG7
 * GPIOH0 || GPIOH1 || GPIOH2 || GPIOH3 || GPIOH4 || GPIOH5 || GPIOH6 || GPIOH7
 * GPIOI0 || GPIOI1 || GPIOI2 || GPIOI3 || GPIOI4 || GPIOI5 || GPIOI6 || GPIOI7
 * GPIOJ0 || GPIOJ1 || GPIOJ2 || GPIOJ3 || GPIOJ4 || GPIOJ5 || GPIOJ6 || GPIOJ7
 * GPIOM0 || GPIOM1 || GPIOM2 || GPIOM3 || GPIOM4 || GPIOM5 || GPIOM6 || GPIOM7
 * pin_output_mode,
 * OutputMode_Inverse || OutputMode_High || OutputMode_Low
 *
 * ****************************************************************************
 */
void GPIO_Output_Ctrl(BYTE pin_index, BYTE pin_output_mode)
{
	if(pin_index < GPIO_MAX){
	    /* Output high */
	    if(pin_output_mode == OutputMode_High)
	    {
	        *asGPIOConfReg[pin_index].GPIO_DataReg |=
	            asGPIOConfReg[pin_index].GPIO_Ctrl_BIT;
	    }
	    /* Output toggle */
	    else if(pin_output_mode == OutputMode_Inverse)
	    {
	         *asGPIOConfReg[pin_index].GPIO_DataReg ^=
	            asGPIOConfReg[pin_index].GPIO_Ctrl_BIT;
	    }
	    /* Output low */
	    else
	    {
	        *asGPIOConfReg[pin_index].GPIO_DataReg &=
	            ~asGPIOConfReg[pin_index].GPIO_Ctrl_BIT;
	    }
	}
}

/**
 * ****************************************************************************
 * The function for reading GPIO pin status.
 *
 * @return,
 * InputMode_High : high
 * InputMode_Low : low
 *
 * @parameter
 * pin_index,
 * GPIOA0 || GPIOA1 || GPIOA2 || GPIOA3 || GPIOA4 || GPIOA5 || GPIOA6 || GPIOA7
 * GPIOB0 || GPIOB1 || GPIOB2 || GPIOB3 || GPIOB4 || GPIOB5 || GPIOB6 || GPIOB7
 * GPIOC0 || GPIOC1 || GPIOC2 || GPIOC3 || GPIOC4 || GPIOC5 || GPIOC6 || GPIOC7
 * GPIOD0 || GPIOD1 || GPIOD2 || GPIOD3 || GPIOD4 || GPIOD5 || GPIOD6 || GPIOD7
 * GPIOE0 || GPIOE1 || GPIOE2 || GPIOE3 || GPIOE4 || GPIOE5 || GPIOE6 || GPIOE7
 * GPIOF0 || GPIOF1 || GPIOF2 || GPIOF3 || GPIOF4 || GPIOF5 || GPIOF6 || GPIOF7
 * GPIOG0 || GPIOG1 || GPIOG2 || GPIOG3 || GPIOG4 || GPIOG5 || GPIOG6 || GPIOG7
 * GPIOH0 || GPIOH1 || GPIOH2 || GPIOH3 || GPIOH4 || GPIOH5 || GPIOH6 || GPIOH7
 * GPIOI0 || GPIOI1 || GPIOI2 || GPIOI3 || GPIOI4 || GPIOI5 || GPIOI6 || GPIOI7
 * GPIOJ0 || GPIOJ1 || GPIOJ2 || GPIOJ3 || GPIOJ4 || GPIOJ5 || GPIOJ6 || GPIOJ7
 * GPIOM0 || GPIOM1 || GPIOM2 || GPIOM3 || GPIOM4 || GPIOM5 || GPIOM6 || GPIOM7
 *
 * ****************************************************************************
 */
BYTE GPIO_Input_Status_Get(BYTE pin_index)
{
	if(pin_index < GPIO_MAX){
	    if(IS_MASK_SET(*asGPIOConfReg[pin_index].GPIO_DataReg,
	        asGPIOConfReg[pin_index].GPIO_Ctrl_BIT))
	    {
	        return(InputMode_High);
	    }
	    else
	    {
	        return(InputMode_Low);
	    }
	}
}

/**
 * ****************************************************************************
 * The function for reading GPIO port data mirror register.
 *
 * @return,
 * Mirror_High : high
 * Mirror_Low : low
 *
 * @parameter
 * pin_index,
 * GPIOA0 || GPIOA1 || GPIOA2 || GPIOA3 || GPIOA4 || GPIOA5 || GPIOA6 || GPIOA7
 * GPIOB0 || GPIOB1 || GPIOB2 || GPIOB3 || GPIOB4 || GPIOB5 || GPIOB6 || GPIOB7
 * GPIOC0 || GPIOC1 || GPIOC2 || GPIOC3 || GPIOC4 || GPIOC5 || GPIOC6 || GPIOC7
 * GPIOD0 || GPIOD1 || GPIOD2 || GPIOD3 || GPIOD4 || GPIOD5 || GPIOD6 || GPIOD7
 * GPIOE0 || GPIOE1 || GPIOE2 || GPIOE3 || GPIOE4 || GPIOE5 || GPIOE6 || GPIOE7
 * GPIOF0 || GPIOF1 || GPIOF2 || GPIOF3 || GPIOF4 || GPIOF5 || GPIOF6 || GPIOF7
 * GPIOG0 || GPIOG1 || GPIOG2 || GPIOG3 || GPIOG4 || GPIOG5 || GPIOG6 || GPIOG7
 * GPIOH0 || GPIOH1 || GPIOH2 || GPIOH3 || GPIOH4 || GPIOH5 || GPIOH6 || GPIOH7
 * GPIOI0 || GPIOI1 || GPIOI2 || GPIOI3 || GPIOI4 || GPIOI5 || GPIOI6 || GPIOI7
 * GPIOJ0 || GPIOJ1 || GPIOJ2 || GPIOJ3 || GPIOJ4 || GPIOJ5 || GPIOJ6 || GPIOJ7
 * GPIOM0 || GPIOM1 || GPIOM2 || GPIOM3 || GPIOM4 || GPIOM5 || GPIOM6 || GPIOM7
 *
 * ****************************************************************************
 */
BYTE GPIO_Mirror_Status_Get(BYTE pin_index)
{
	if(pin_index < GPIO_MAX){
	    if(IS_MASK_SET(*asGPIOConfReg[pin_index].GPIO_MirrorReg,
	        asGPIOConfReg[pin_index].GPIO_Ctrl_BIT))
	    {
	        return(Mirror_High);
	    }
	    else
	    {
	        return(Mirror_Low);
	    }
	}
}
#endif
