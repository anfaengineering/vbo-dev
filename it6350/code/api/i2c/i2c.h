/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * i2c.h
 * Created on: 2012/8/15
 * Author: ite00595
 * ****************************************************************************
 */

#ifndef I2C_H_
#define I2C_H_

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define I2C_PIO_READ_LENGTH_BY_FIRST_WORD   0xFFFF

/* Define Register I2C Transfer Status */
#define I2C_STAT_TRANS_IN_PROGRESS      BIT7
#define I2C_STAT_ADDR_MATCH             BIT6
#define I2C_STAT_BUS_BUSY               BIT5
#define I2C_STAT_ARB                    BIT4
#define I2C_STAT_STOP_DETECT            BIT4
#define I2C_STAT_TIMEOUT                BIT3
#define I2C_STAT_DIR_M_READ             BIT2
#define I2C_STAT_INT_PENDING            BIT1
#define I2C_STAT_ACK_RECV               BIT0

/* Define Register I2C Control */
#define I2C_CRTL_RX_MODE                BIT7
#define I2C_CRTL_INT_EN                 BIT6
#define I2C_CRTL_MASTER_MODE            BIT5
#define I2C_CRTL_SOFT_RESET             BIT4
#define I2C_CRTL_ACK                    BIT3
#define I2C_CRTL_START                  BIT2
#define I2C_CRTL_STOP                   BIT1
#define I2C_CRTL_HALT                   BIT0

/* Define Register I2C Control1 */
#define I2C_CRTL1_SMB_MASTER            BIT7
#define I2C_CRTL1_I2C_MASTER            BIT6
#define I2C_CRTL1_CMD_NUM1              BIT5
#define I2C_CRTL1_CMD_NUM0              BIT4
#define I2C_CRTL1_REG_SWITCH            BIT3
#define I2C_CRTL1_RSVD                  BIT2
#define I2C_CRTL1_MDL_EN                BIT1
#define I2C_CRTL1_BLK_MODE              BIT0

/* Define Register I2C Control2 */
#define I2C_CRTL2_M4_INT                BIT7
#define I2C_CRTL2_M3_INT                BIT6
#define I2C_CRTL2_M2_INT                BIT5
#define I2C_CRTL2_M1_INT                BIT4
#define I2C_CRTL2_I2C_SLAVE             BIT3
#define I2C_CRTL2_SMB_SLAVE             BIT2
#define I2C_CRTL2_RSVD                  BIT1
#define I2C_CRTL2_ID_SCL_HLD            BIT0

/* Define Register I2C Control3 */
#define I2C_CRTL3_RSVD2                 BIT7
#define I2C_CRTL3_W_POINTER4            BIT6
#define I2C_CRTL3_W_POINTER3            BIT5
#define I2C_CRTL3_W_POINTER2            BIT4
#define I2C_CRTL3_LAST_DATA_NO_REQED    BIT3
#define I2C_CRTL3_DMA_LEN_EN            BIT2
#define I2C_CRTL3_RSVD1                 BIT1
#define I2C_CRTL3_DMA_EN                BIT0

/* Define Register I2C Control4 */
#define I2C_CRTL4_ONE_SHOT_M4           BIT7
#define I2C_CRTL4_ONE_SHOT_M3           BIT6
#define I2C_CRTL4_ONE_SHOT_M2           BIT5
#define I2C_CRTL4_ONE_SHOT_M1           BIT4
#define I2C_CRTL4_R_M4                  BIT3
#define I2C_CRTL4_R_M3                  BIT2
#define I2C_CRTL4_R_M2                  BIT1
#define I2C_CRTL4_R_M1                  BIT0

/* Define Register I2C Control5 */
#define I2C_CRTL5_MODE_SELECT_M4        (BIT7 | BIT6)
#define I2C_CRTL5_ONE_SHOT_M4           0
#define I2C_CRTL5_EN_MODE_M4            BIT6
#define I2C_CRTL5_PWR_SAVE_M4           BIT7
#define I2C_CRTL5_AUTO_MODE_M4          (BIT7 | BIT6)
#define I2C_CRTL5_MODE_SELECT_M3        (BIT5 | BIT4)
#define I2C_CRTL5_ONE_SHOT_M3           0
#define I2C_CRTL5_EN_MODE_M3            BIT4
#define I2C_CRTL5_PWR_SAVE_M3           BIT5
#define I2C_CRTL5_AUTO_MODE_M3          (BIT5 | BIT4)
#define I2C_CRTL5_MODE_SELECT_M2        (BIT3 | BIT2)
#define I2C_CRTL5_ONE_SHOT_M2           0
#define I2C_CRTL5_EN_MODE_M2            BIT2
#define I2C_CRTL5_PWR_SAVE_M2           BIT3
#define I2C_CRTL5_AUTO_MODE_M2          (BIT3 | BIT2)
#define I2C_CRTL5_MODE_SELECT_M1        (BIT1 | BIT0)
#define I2C_CRTL5_ONE_SHOT_M1           0
#define I2C_CRTL5_EN_MODE_M1            BIT0
#define I2C_CRTL5_PWR_SAVE_M1           BIT1
#define I2C_CRTL5_AUTO_MODE_M1          (BIT1 | BIT0)

enum _i2c_touch_pad_read_mode
{
   I2C_TP_Read_Interrupt = 1,
   I2C_TP_Read_Polling,
};

/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void do_i2c_interrupt_flow(UINT8 ucCH);
extern void do_i2c_int_init_flow(UINT8 ucCH);
extern UINT8 i2c_fifo_read_def(
    UINT8 ucCH, UINT8 ucSlave, UINT8 ucAdd, UINT8 *Var, UINT8 ucCount);
extern UINT8 i2c_fifo_write_def(
    UINT8 ucCH, UINT8 ucSlave,  UINT8 ucAdd, UINT8 *Var, UINT8 ucCount);
extern UINT8 i2c_pio_block_read(
    UINT8 ucCH, UINT8 ucSlaveID, UINT8 *pWBuf,
    UINT16 usWCount, UINT8 *pRBuf,UINT16 usRCount);
extern UINT8 i2c_pio_block_read_error_handle(
    UINT8 ucCH, UINT8 ucSlaveID, UINT8 *pWBuf,
    UINT16 usWCount, UINT8 *pRBuf, UINT16 usRCount);
extern UINT8 i2c_pio_block_write(
    UINT8 ucCH, UINT8 ucSlaveID, UINT8 ucAddr, UINT8 *pWBuf, UINT16 usWCount);
extern void i2c_channel_reset(UINT8 p_channel);
extern UINT8 i2c_touch_pad_write(
    UINT8 ucCH, UINT16 usWDataSram, UINT8 ucWTestCount);
extern BOOL i2c_touch_pad_read(
    UINT8 ucCH, UINT8 ucWTestCount, UINT8 ucRTestCount ,UINT8 ucServiceMode);
extern BOOL i2c_touch_pad_read_buffer_mode(UINT8 ucCH, UINT8 ucRTestCount);


extern UINT8 i2c_block_dma_read(UINT8 ucCH, UINT8 ucSlaveID, UINT8* pInputBuf, UINT8 ucWCount, UINT8* pOutputBuf, UINT8 ucRCount);
extern UINT8 i2c_block_dma_write(UINT8 ucCH,UINT8 ucSlaveID, UINT8 ucAddr, UINT8* pInputBuf, UINT8 ucWCount);

#endif /* I2C_H_ */

