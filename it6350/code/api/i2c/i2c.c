/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * i2c.c
 * Created on: 2012/8/15
 * Author: ite00595
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

UINT8 I2C_DMA_ADD gucDMABuffer[256];
UINT8 gucI2cSpeed[3] = {0x76, 0x76, 0x76};		// 1M:0x0A, 400K:0x1C, 100K:0x76

volatile UINT8 * DebugRam = (UINT8*) gucDMABuffer;


/**
 * ****************************************************************************
 * Function name: i2c_polling_interrupt()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */ 
BOOL i2c_polling_interrupt(UINT8 ucCH)
{
    UINT8 uclCH = ucCH;
    volatile unsigned char  l_time_out;
    volatile int i = 0;

    l_time_out = 0;

    while(((ECReg(I2C_STAT(uclCH)) & 0x02)!= 0x02) )
	{
        /* Delay 15.26 us */
        WNCKR = 0x00;
		i++;
		if((i % 1500) == 0)
		{
	        /* Check Bus Busy */
		    if(((ECReg(I2C_STAT(uclCH)) & 0x20) != 0x20))
		    {
		        return 0;
		    }
            else
            {
                if(++l_time_out > 4)
                {
                    return 0;
                }
            }
		}

	}
	for(i=0; i<10; i++){;}
    /* Read Clear */
	gucI2CSensorErrStatus = (ECReg(I2C_ERROR_STAT(ucCH)) & 0x0F);

    /* Clear Interrupt */
	ECReg(I2C_CONTROL(uclCH)) |= 0x01;
	if(gucI2CSensorErrStatus)
	{
	    return 0;
	}

    /* Prevent the floating state of I2C pins */
	if( ECReg(I2C_STAT(uclCH)) & 0x08 ) return 0; 
	return 1;
}
//****************************************************************************************
// Function name: i2c_bypass_dma_read()
// Description:
//   i2c dma read for bypass
// Arguments:
//   UINT8 ucCH: Channel 0:D, 1:E, 2:F.
//   UINT8 ucSlaveID: I2C slave address
//   UINT16 usWAdd: Write SRAM address
//   UINT8 ucWCount: Write data count
//   UINT16 usRAdd: Read SRAM address
//   UINT8 ucRCount: Read data count
// Return Values:
//   -1: bus busy
//   0: read action done
// Note:
//   none
//****************************************************************************************
	 
UINT8 i2c_bypass_dma_read(UINT8 ucCH,UINT8 ucSlaveID, UINT16 usWAdd, UINT8 ucWCount, UINT16 usRAdd, UINT8 ucRCount)
{
     //Set master    
     //RTL_PRINTF("i2c_touch_pad_read\r\n");      

    //
    // bus busy
    //
	while(ECReg(I2C_STAT(ucCH)) & I2C_STAT_BUS_BUSY);
    if(ucWCount)
    {
        ECReg(I2C_CONTROL(ucCH)) = (I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        ECReg(I2C_CONTROL(ucCH)) &= ~(I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
    }
	ECReg(I2C_ADD(ucCH)) = 0x11;
	ECReg(I2C_PRESCALE(ucCH)) =gucI2cSpeed[ucCH];//0x1C;//0x76;	//1M:0x0A, 400K:0x1C, 100K:0x76
	ECReg(I2C_TIME_OUT(ucCH)) = 0xFF;		
	if(ucWCount)
	{		  
	    ECReg(I2C_CONTROL1(ucCH)) = (I2C_CRTL1_MDL_EN | I2C_CRTL1_BLK_MODE);   // 
	    ECReg(I2C_DATA_NUM1(ucCH)) = ucWCount;	  
        ECReg(I2C_ADD_H(ucCH)) = (UINT8)((usWAdd & 0x0000FF00) >> 8);  // Address high	0x64
        ECReg(I2C_ADD_L(ucCH)) = ((UINT8)(usWAdd & 0x000000FF));  //  Low	0x65	    
        ECReg(I2C_CONTROL1(ucCH)) = (I2C_CRTL1_MDL_EN | I2C_CRTL1_BLK_MODE | I2C_CRTL1_REG_SWITCH);   // 
        ECReg(I2C_SLAVE_ID1(ucCH)) = ucSlaveID ;       
    }
    else
    {        
	    ECReg(I2C_CONTROL1(ucCH)) =  (I2C_CRTL1_I2C_MASTER |I2C_CRTL1_MDL_EN );   // 
	    ECReg(I2C_SLAVE_ID1(ucCH)) = (ucSlaveID | 0x01);
    }
    // Fill read info
	ECReg(I2C_DATA_NUM1(ucCH)) = ucRCount;	
	ECReg(I2C_ADD_H(ucCH)) = (UINT8)((usRAdd & 0x0000FF00) >> 8); // High 0x96
    ECReg(I2C_ADD_L(ucCH)) = (UINT8)(usRAdd & 0x000000FF ); // Low  0x97				
    
	ECReg(I2C_CONTROL3(ucCH)) = (I2C_CRTL3_DMA_EN);
    ECReg(I2C_CONTROL4(ucCH)) = ((ECReg(I2C_CONTROL4(ucCH)) & ~I2C_CRTL4_R_M1) | I2C_CRTL4_R_M1);  
    ECReg(I2C_CONTROL5(ucCH)) &= ~I2C_CRTL5_MODE_SELECT_M1;   // mode select
	ECReg(I2C_CONTROL(ucCH)) = (I2C_CRTL_MASTER_MODE| I2C_CRTL_INT_EN|I2C_CRTL_ACK|I2C_CRTL_START);
	ECReg(I2C_CONTROL4(ucCH)) |= I2C_CRTL4_ONE_SHOT_M1;   
	
	if(i2c_polling_interrupt(ucCH) == 1)
    {
        ECReg(I2C_CONTROL(ucCH)) = (I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        ECReg(I2C_CONTROL(ucCH)) &= ~(I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        return 0;
    }
	else
    {
        ECReg(I2C_CONTROL(ucCH)) = (I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        ECReg(I2C_CONTROL(ucCH)) &= ~(I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        return -1;
    }
}

//****************************************************************************************
// Function name: i2c_bypass_dma_write()
// Description:
//   i2c dma write for bypass
// Arguments:
//   UINT8 ucCH: Channel 0:D, 1:E, 2:F.
//   UINT8 ucSlaveID: I2C slave address
//   UINT16 usWAdd: Write SRAM address
//   UINT8 ucWCount: Write data count
//   UINT16 usRAdd: Read SRAM address
//   UINT8 ucRCount: Read data count
// Return Values:
//   -1: bus busy
//   0: read action done
// Note:
//   none
//****************************************************************************************

UINT8 i2c_bypass_dma_write(UINT8 ucCH,UINT8 ucSlaveID, UINT16 usWAdd, UINT8 ucWCount)
{
  
	while(ECReg(I2C_STAT(ucCH)) & I2C_STAT_BUS_BUSY);
    ECReg(I2C_CONTROL(ucCH)) = (I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
    ECReg(I2C_CONTROL(ucCH)) &= ~(I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
	ECReg(I2C_ADD(ucCH)) = 0x11;
	ECReg(I2C_PRESCALE(ucCH)) = gucI2cSpeed[ucCH];//0x1C;//0x76;		//1M:0x0A, 400K:0x1C, 100K:0x76
	ECReg(I2C_TIME_OUT(ucCH)) = 0xFF;
	ECReg(I2C_CONTROL1(ucCH)) = (I2C_CRTL1_MDL_EN | I2C_CRTL1_BLK_MODE);   // 	
	ECReg(I2C_DATA_NUM1(ucCH)) = ucWCount;

    ECReg(I2C_ADD_H(ucCH)) = (UINT8)((usWAdd & 0x0000FF00) >> 8);  // Address high	0x64
    ECReg(I2C_ADD_L(ucCH)) = ((UINT8)(usWAdd & 0x000000FF));  //  Low	0x65	
    
    ECReg(I2C_SLAVE_ID1(ucCH)) = ucSlaveID;	
		
	ECReg(I2C_CONTROL3(ucCH)) = (I2C_CRTL3_DMA_EN);
    ECReg(I2C_CONTROL4(ucCH)) &= ~I2C_CRTL4_R_M1;   
    ECReg(I2C_CONTROL5(ucCH)) &= ~I2C_CRTL5_MODE_SELECT_M1;   // control4	       
	ECReg(I2C_CONTROL(ucCH)) = (I2C_CRTL_MASTER_MODE| I2C_CRTL_INT_EN|I2C_CRTL_ACK|I2C_CRTL_START);	

	ECReg(I2C_CONTROL4(ucCH)) |= I2C_CRTL4_ONE_SHOT_M1;    
    if(i2c_polling_interrupt(ucCH) == 1)
    {
        ECReg(I2C_CONTROL(ucCH)) = (I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        ECReg(I2C_CONTROL(ucCH)) &= ~(I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        return 0;
    }
    else
    {
        ECReg(I2C_CONTROL(ucCH)) = (I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        ECReg(I2C_CONTROL(ucCH)) &= ~(I2C_CRTL_SOFT_RESET | I2C_CRTL_HALT);
        return -1;
    }
} 


//****************************************************************************************
// Function name: i2c_block_dma_read()
// Description:
//   i2c block dma read 
// Arguments:
//   UINT8 ucCH: Channel 0:D, 1:E, 2:F.
//   UINT8 ucSlaveID: I2C slave address
//   UINT8* pInputBuf: Write SRAM address
//   UINT8 ucWCount: Write data count
//   UINT8* pOutputBuf: Read SRAM address
//   UINT8 ucRCount: Read data count
// Return Values:
//   -1: bus busy
//   0: read action done
// Note:
//   none
//****************************************************************************************
	 
UINT8 i2c_block_dma_read(UINT8 ucCH, UINT8 ucSlaveID, UINT8* pInputBuf, UINT8 ucWCount, UINT8* pOutputBuf, UINT8 ucRCount)
{
    UINT16 usSRAM;
    UINT8* pi2c_Buf;
   	int i;
    
    pi2c_Buf = (UINT8*)gucDMABuffer;      
    usSRAM = (UINT16)(((UINT32)pi2c_Buf - 0x80000) + 0xD000);
   
    // Assign write data
    for(i = 0; i< ucWCount; i++)
    {
        pi2c_Buf[i] = pInputBuf[i];
    }
    // DMA write
    if(i2c_bypass_dma_read(ucCH, ucSlaveID, usSRAM, ucWCount, usSRAM, ucRCount) == 0)
    {
        for(i = 0; i < ucRCount; i++)
       	{
       	   pOutputBuf[i] = pi2c_Buf[i];
       	}

        return 0;
    }

    return -1;
}	 


//****************************************************************************************
// Function name: i2c_block_dma_write()
// Description:
//   i2c block dma write
// Arguments:
//   UINT8 ucCH: Channel 0:D, 1:E, 2:F.
//   UINT8 ucSlaveID: I2C slave address
//   UINT8* pInputBuf: Write SRAM address
//   UINT8 ucWCount: Write data count
// Return Values:
//   -1: bus busy
//   0: read action done
// Note:
//   none
//****************************************************************************************
	 
UINT8 i2c_block_dma_write(UINT8 ucCH,UINT8 ucSlaveID, UINT8 ucAddr, UINT8* pInputBuf, UINT8 ucWCount)
{
    UINT16 usSRAM;
    UINT8* pi2c_Buf;
   	int i;
    
    pi2c_Buf = (UINT8*)gucDMABuffer;      
    usSRAM = (UINT16)(((UINT32)pi2c_Buf - 0x80000) + 0xD000);
    pi2c_Buf[0]=ucAddr;
    // Assign write data
    for(i = 0; i< ucWCount; i++)
    {
        pi2c_Buf[(i+1)] = pInputBuf[i];
    }

    // DMA write
    return i2c_bypass_dma_write(ucCH, ucSlaveID, usSRAM, (ucWCount+1));
}	 


