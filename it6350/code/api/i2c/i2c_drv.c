/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * i2c_drv.c
 * Created on: 2012/8/15
 * Author: ite00595
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"
extern UINT8 gucI2cSpeed[3];
/**
 * ****************************************************************************
 * i2c channel init
 *
 * @return
 *
 * @parameter
 * u8CH, 0 ~ 2
 *
 * ****************************************************************************
 */
void I2C_CODE i2c_drv_init(UINT8 u8CH, UINT8 u8Speed)
{
	if(u8CH < I2C_MAX_DRV_COUNT)
	{
		gucI2cSpeed[u8CH] = u8Speed;
		switch(u8CH)
		{
			case I2C_D:
		        GPCRH1 = ALT;   /* Ch3 */
                GPCRH2 = ALT;   /* Ch3 */
                GCR2 |= BIT5;   /* Enable Ch3 */
                break;
			case I2C_E:
                GPCRE0 = ALT;   /* Ch4 */
                GPCRE7 = ALT;   /* Ch4 */
                PMER1 |= BIT0;  /* Enable Ch4 */
				break;
			case I2C_F:
                GPCRA4 = ALT;   /* Ch5 */
                GPCRA5 = ALT;   /* Ch5 */
                PMER1 |= BIT1;  /* Enable Ch5 */
				break;
		}
		/* gucI2CINTFlag[u8CH] = 1; */
	}
}

