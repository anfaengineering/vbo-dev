/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * api_i2c_slave.h
 * Dino Li
 * Version, 1.00
 * Note, To link [api_xxx.o] if related api function be used.
 * ****************************************************************************
 */

#ifndef _API_I2C_SLAVE_H_
#define _API_I2C_SLAVE_H_

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define _I2C_Slave_A                0
#define _I2C_Slave_B                1

#define _I2C_Match_Slave_Address1   0
#define _I2C_Match_Slave_Address2   1

#define _I2C_Slave_at_Channel_A     0x00
#define _I2C_Slave_at_Channel_B     0x01
#define _I2C_Slave_at_Channel_C     0x02
#define _I2C_Slave_at_Channel_D   	0x03
#define _I2C_Slave_at_Channel_E     0x04
#define _I2C_Slave_at_Channel_F   	0x05

#define _I2C_Slave_at_Channel_NULL  0xFF


/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void I2C_Slave_A_OEM_Init(
	BYTE *p_byte_read_buffer,
    FUNCT_PTR_V_V p_stop_condition_func,
    INT32 p_buf_size);

extern void I2C_Slave_B_OEM_Init(
	BYTE *p_byte_read_buffer,
    FUNCT_PTR_V_V p_stop_condition_func,
    INT32 p_buf_size);

#if 1
extern void I2C_Slave_Initialization(
    BYTE p_slave_x,
    BYTE p_slave_x_at_channel,
    BYTE p_slave_x_addr,
    BYTE p_slave_x_addr2);
#else
extern void I2C_Slave_Initialization(
    BYTE p_slave_a_at_channel,
    BYTE p_slave_b_at_channel,
    BYTE p_slave_a_addr,
    BYTE p_slave_a_addr2,
    BYTE p_slave_b_addr,
    BYTE p_slave_b_addr2);
#endif
extern void I2C_Slave_Interface_Select(
    BYTE p_slave_module, BYTE p_local_channel);
extern void I2C_Slave_A_Enable(BYTE p_slave_address_1, BYTE p_slave_address_2);
extern void I2C_Slave_B_Enable(BYTE p_slave_address_1, BYTE p_slave_address_2);
extern void I2C_Slave_A_ISR_Address_x(void);
#if 1
void I2C_Slave_B_ISR_Address_1(void);
#else
extern void I2C_Slave_B_ISR_Address_x(BYTE p_match_slave_address);
extern void I2C_Slave_B_ISR(void);
#endif
/* 
 * ****************************************************************************
 * structure
 * ****************************************************************************
 */
typedef struct I2CSlaveConfig
{
	uchar_8	*SMBusPin0	;   /* clock pin */
	uchar_8	*SMBusPin1	;   /* data pin */
} sI2CSlaveConfig;

#endif

