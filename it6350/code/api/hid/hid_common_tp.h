/* 
 * ****************************************************************************
 * hid_common_tp.h
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

#ifndef __HID_COMMON_TP_H__
#define __HID_COMMON_TP_H__

/* 
 * ****************************************************************************
 * Function prototype
 * ****************************************************************************
 */
extern UINT16 hid_get_feature_report_touch(UINT8 u8ID, UINT16 u16Len,
                                            UINT8 *pBuf);
extern void hid_set_feature_report_touch(UINT8 u8ID, UINT16 u16Len,
                                            UINT8 *pBuf);
extern UINT16 hid_get_input_report_touch(UINT8 u8ID, UINT8 *pBuf);
extern void hid_set_output_report_touch(UINT8 u8ID, UINT16 u16Len,
                                            UINT8 *pBuf);
extern void hid_set_mouse_protocol(UINT8 u8Protocol);
#endif

