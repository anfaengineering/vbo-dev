/* 
 * ****************************************************************************
 * hid_common_kb.h
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

#ifndef __HID_COMMON_KB_H__
#define __HID_COMMON_KB_H__

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define HID_CUSTOM_CMD_LENGTH           0x10

#define HID_REPORT_DES_TOTAL_LENGTH     (sizeof(hid_report_descriptor))
#define HID_REPORT_2_DES_TOTAL_LENGTH   (sizeof(hid_report_descriptor_2))

#define HID_KB_KEYCODE_NUM              6
#define HID_KB_MM_USAGE_CUSTOM_FN_OFFSET 0x4000
#define HID_KB_MM_USAGE_KEY_RELEASE_MASK 0x8000

#define MOD_LEFT_CTRL	0x01
#define MOD_LEFT_SHIFT	0x02
#define MOD_LEFT_ALT	0x04
#define MOD_LEFT_GUI	0x08
#define MOD_RIGHT_CTRL	0x10
#define MOD_RIGHT_SHIFT	0x20
#define MOD_RIGHT_ALT	0x40
#define MOD_RIGHT_GUI	0x80

/* Status LED control bits */
#define LED_NUMLOCK	    0x01
#define LED_CAPSLOCK	0x02
#define LED_SCROLLLOCK	0x04
#define LED_COMPOSE	    0x08
#define LED_KANA	    0x10

/* 4ms */
#define HID_IDLE_DURATION_UNIT	0x4

/* Multimedia keyboard control bits */
enum _HID_KB_MM_CONSUMER_FN_TYPE_
{
    MMKB_MUTE =	0,
    MMKB_VOLUP,	
    MMKB_VOLDOWN,
    MMKB_PLAY,
    MMKB_STOP,	
    MMKB_PREV,	
    MMKB_NEXT,	
    MMKB_MAIL,

    MMKB_CAL,
    MMKB_SEARCH,
    MMKB_HOME,
    MMKB_FAVORITE,
    MMKB_REFRESH,
    MMKB_WWWSTOP,
    MMKB_WWWFORWARD,
    MMKB_WWWBACK,

    MMKB_BASSBOOST,
    MMKB_LOUDNESS,
    MMKB_BASSUP,
    MMKB_BASSDOWN,
    MMKB_TREBLEUP,
    MMKB_TREBLEDOWN,
    MMKB_MEDIASELECT,
    MMKB_MYCOMPUTER,

    MMKB_MAX,
};

enum _HID_KB_MM__USAGE_CUSTOM_FN_
{
    HID_KB_MM_USAGE_CUSTOM_WIRELESS_RADIO_CONTROL
        = HID_KB_MM_USAGE_CUSTOM_FN_OFFSET,
    HID_KB_MM_USAGE_CUSTOM_SYSTEM_POWER_DOWN,
    HID_KB_MM_USAGE_CUSTOM_SYSTEM_SLEEP,
    HID_KB_MM_USAGE_CUSTOM_SYSTEM_WAKEUP,
    HID_KB_MM_USAGE_CUSTOM_BACK_LIGHT_OFF,
    HID_KB_MM_USAGE_CUSTOM_TOUCH_PAD_ON,
    HID_KB_MM_USAGE_CUSTOM_TOUCH_PAD_OFF,
};

enum _HID_REPORT_ID_
{
    HID_KB_GENERIC = 1,
    HID_KB_MM_CONSUMER_FN_REPORT_ID,
    HID_KB_MM_WIRELESS_FN_REPORT_ID,
    HID_KB_MM_OEM_FN_REPORT_ID,
    HID_KB_MM_SYSTEM_FN_REPORT_ID,
    HID_UPS_POWER_SUMMARY,
    HID_UPS_DESIGN_CAPACITY,
    HID_UPS_BATTERY_PRESENT,
    HID_UPS_CONFIG_VOLTAGE,
    HID_UPS_PRODUCT,
};

/* 
 * ****************************************************************************
 * Function prototype
 * ****************************************************************************
 */
extern void hid_set_idle_rate(UINT8 u8ReportID, UINT8 u8IdleRate);          
extern void hid_set_kb_protocol(UINT8 u8Protocol);        
extern UINT8 hid_get_kb_protocol();
extern void hid_buff_fn_key(UINT16 u16UsageValue, BOOL bRelease);

/* 
 * ****************************************************************************
 * structure
 * ****************************************************************************
 */

/* 
 * data structure for boot protocol keyboard report
 * see HID1_11.pdf appendix B section 1
 */
#pragma pack(1)
typedef struct {

	UINT8 id;

	UINT8 modifier;
	UINT8 reserved;
	UINT8 keycode[HID_KB_KEYCODE_NUM];
} keyboard_report_t;
#pragma pack()

/* 
 * ****************************************************************************
 * global variables
 * ****************************************************************************
 */
extern keyboard_report_t gtkeyboard_report;
extern BYTE gu8Protocol;

#endif

