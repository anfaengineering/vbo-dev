/* 
 * ****************************************************************************
 * hid_custom_cmd.c
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

#if EN_FULL_USB
/* internal data */
const BYTE gpHidCustomCmdInitKey[HID_CUSTOM_CMD_LENGTH - 1] =
{'I', 'T', 'E', ' ', 'T', 'e', 'c', 'h', '.', ' ', 'I', 'n', 'c', '.', '\0'};

/* global variables for hid custom command */
static BOOL gbHidCustomCmdInit = FALSE;
static UINT8 gpHidCustomCmdBuffer[HID_CUSTOM_CMD_LENGTH];

/* common */
#define HCC_REPORT_ID           gpHidCustomCmdBuffer[0]
#define HCC_KEY_PTR             (gpHidCustomCmdBuffer + 1)             
#define HCC_CMD                 gpHidCustomCmdBuffer[1]

/* memory/register read/write */
#define HCC_ADDRESS             (((UINT32)gpHidCustomCmdBuffer[4] << 16)+ \
                                ((UINT32)gpHidCustomCmdBuffer[3] << 8) + \
                                gpHidCustomCmdBuffer[2])
#define HCC_COUNT               gpHidCustomCmdBuffer[5]
#define HCC_VALUE_PTR           (gpHidCustomCmdBuffer + 6)

/**
 * ****************************************************************************
 * custom command jump to boot code for firmware flash
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
static void HID_CODE hcc_switch_to_bbk(void)
{
    /* disable interrupts */
    hal_global_int_ctl(HAL_DISABLE_INTERRUPTS);

    /* in-transcation status stage */
    usb_slave_status_stage_in();
    DelayXms(5);
    /* set D+/D- low */
    ECReg(SC_CONTROL_REG) = 0x08;
    DelayXms(1000);

    /* set flag(main code to bbk), only for Bx IC */
    MCCR1 |= (0x2A << 2);
    /* jump to BBK section */
    __asm__ volatile 
    (
        "j __bbk_begin"
    );
}

/**
 * ****************************************************************************
 * custom command handler
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
int HID_CODE hcc_do_custom_cmd(UINT8 *pBuf)
{
	std_memcpy(gpHidCustomCmdBuffer, pBuf, HID_CUSTOM_CMD_LENGTH);

    if(gbHidCustomCmdInit)
    {
    	switch(HCC_CMD)
    	{
            /* parse cmd code */
            case HCC_SWITCH_TO_BBK:
                hcc_switch_to_bbk();
                break;
            /* cmd error, disable custom cmd */
            default:
                gbHidCustomCmdInit = FALSE;
                break;

    	}
    }
    else
    {
        /* check key */
        if(std_memcmp(HCC_KEY_PTR,
            (UINT8*)gpHidCustomCmdInitKey, sizeof(gpHidCustomCmdInitKey)) == 0)
        {
            gbHidCustomCmdInit = TRUE;
        }
    }
    
	return 0;
}

/**
 * ****************************************************************************
 * custom command read memory/register
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
int HID_CODE hcc_get_custom_data(UINT8 *pBuf)
{
    if(gbHidCustomCmdInit)
    {
		
    }
    else
    {
        /* custom cmd was diasbled, send 0xFF */
        std_memset(HCC_KEY_PTR, 0xFF, sizeof(gpHidCustomCmdInitKey));
    }

	std_memcpy(pBuf, gpHidCustomCmdBuffer, HID_CUSTOM_CMD_LENGTH);

	return HID_CUSTOM_CMD_LENGTH;
}

#endif
