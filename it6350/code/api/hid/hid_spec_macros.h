/* 
 * ****************************************************************************
 *
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
 * ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * Copyright (c) Microsoft Corporation. All rights reserved
 *
 * Module Name:
 *
 *  hid_spec_macros.h
 *
 * Abstract:
 *
 *  This module contains the macro definitions for the HID Usages
 *
 * ****************************************************************************
 */

#ifndef _HID_SPEC_MACROS_H_
#define _HID_SPEC_MACROS_H_

/* 
 * ****************************************************************************
 *
 * Other HID definitions
 * NOTE: These definitions are designed to permit compiling the
 * HID report descriptors
 * with somewhat self-explanatory information to
 * help readability and reduce errors
 *
 * ****************************************************************************
 */
/* input,output,feature flags */
#define Data_Arr_Abs                            0x00
#define Const_Arr_Abs                           0x01
#define Data_Var_Abs                            0x02
#define Const_Var_Abs                           0x03
#define Data_Var_Rel                            0x06

/* collection flags */
#define Physical                                0x00
#define Application                             0x01
#define Logical                                 0x02
#define NamedArray                              0x04
#define UsageSwitch                             0x05

/* other */
#define Undefined                               0x00

#define HID_USAGE_PAGE(a)                       0x05,a
#define HID_USAGE(a)                            0x09,a
#define HID_USAGE16(a,b)                        0x0A,a,b

/* This or-s the mod into usage */
#define HID_USAGE_SENSOR_DATA(a,b)              a|b
#define HID_COLLECTION(a)                       0xA1,a
#define HID_REPORT_ID(a)                        0x85,a
#define HID_REPORT_SIZE(a)                      0x75,a
#define HID_REPORT_COUNT(a)                     0x95,a
#define HID_USAGE_MIN_8(a)                      0x19,a
#define HID_USAGE_MIN_16(a,b)                   0x1A,a,b
#define HID_USAGE_MAX_8(a)                      0x29,a
#define HID_USAGE_MAX_16(a,b)                   0x2A,a,b
#define HID_LOGICAL_MIN_8(a)                    0x15,a
#define HID_LOGICAL_MIN_16(a,b)                 0x16,a,b
#define HID_LOGICAL_MIN_32(a,b,c,d)             0x17,a,b,c,d
#define HID_LOGICAL_MAX_8(a)                    0x25,a
#define HID_LOGICAL_MAX_16(a,b)                 0x26,a,b
#define HID_LOGICAL_MAX_32(a,b,c,d)             0x27,a,b,c,d
#define HID_UNIT_EXPONENT(a)                    0x55,a
#define HID_INPUT(a)                            0x81,a
#define HID_OUTPUT(a)                           0x91,a
#define HID_FEATURE(a)                          0xB1,a
#define HID_END_COLLECTION                      0xC0

/* Consumer Page (0x0C) */
#define HID_USAGE_CONSUMER_UNDEFINED                        0x000
#define HID_USAGE_CONSUMER_CONSUMER_CONTROL                 0x001
#define HID_USAGE_CONSUMER_NUMERIC_KEY_PAD                  0x002
#define HID_USAGE_CONSUMER_PROGRAMMABLE_BUTTONS             0x003
#define HID_USAGE_CONSUMER_MICROPHONE                       0x004
#define HID_USAGE_CONSUMER_HEADPHONE                        0x005
#define HID_USAGE_CONSUMER_GRAPHIC_EQUALIZER                0x006
 
#define HID_USAGE_CONSUMER_PLUS_10                          0x020
#define HID_USAGE_CONSUMER_PLUS_100                         0x021
#define HID_USAGE_CONSUMER_AM_PM                            0x022

#define HID_USAGE_CONSUMER_POWER                            0x030
#define HID_USAGE_CONSUMER_RESET                            0x031
#define HID_USAGE_CONSUMER_SLEEP                            0x032
#define HID_USAGE_CONSUMER_SLEEP_AFTER                      0x033
#define HID_USAGE_CONSUMER_SLEEP_MODE                       0x034
#define HID_USAGE_CONSUMER_ILLUMINATION                     0x035
#define HID_USAGE_CONSUMER_FUNCTION_BUTTONS                 0x036

#define HID_USAGE_CONSUMER_MENU                             0x040
#define HID_USAGE_CONSUMER_MENU_PICK                        0x041
#define HID_USAGE_CONSUMER_MENU_UP                          0x042
#define HID_USAGE_CONSUMER_MENU_DOWN                        0x043
#define HID_USAGE_CONSUMER_MENU_LEFT                        0x044
#define HID_USAGE_CONSUMER_MENU_RIGHT                       0x045
#define HID_USAGE_CONSUMER_MENU_ESCAPE                      0x046
#define HID_USAGE_CONSUMER_MENU_VALUE_INCREASE              0x047
#define HID_USAGE_CONSUMER_MENU_VALUE_DECREASE              0x048
 
#define HID_USAGE_CONSUMER_DATA_ON_SCREEN                   0x060
#define HID_USAGE_CONSUMER_CLOSED_CAPTION                   0x061
#define HID_USAGE_CONSUMER_CLOSED_CAPTION_SELECT            0x062
#define HID_USAGE_CONSUMER_VCR_TV                           0x063
#define HID_USAGE_CONSUMER_BROADCAST_MODE                   0x064
#define HID_USAGE_CONSUMER_SNAPSHOT                         0x065
#define HID_USAGE_CONSUMER_STILL                            0x066

#define HID_USAGE_CONSUMER_BRIGHTNESS_INCREMENT             0x06F
#define HID_USAGE_CONSUMER_BRIGHTNESS_DECREMENT             0x070

#define HID_USAGE_CONSUMER_SELECTION                        0x080
#define HID_USAGE_CONSUMER_ASSIGN_SELECTION                 0x081
#define HID_USAGE_CONSUMER_MODE_STEP                        0x082
#define HID_USAGE_CONSUMER_RECALL_LAST                      0x083
#define HID_USAGE_CONSUMER_ENTER_CHANNEL                    0x084
#define HID_USAGE_CONSUMER_ORDER_MOVIE                      0x085
#define HID_USAGE_CONSUMER_CHANNEL                          0x086
#define HID_USAGE_CONSUMER_MEDIA_SELECTION                  0x087
#define HID_USAGE_CONSUMER_MEDIA_SELECT_COMPUTER            0x088
#define HID_USAGE_CONSUMER_MEDIA_SELECT_TV                  0x089
#define HID_USAGE_CONSUMER_MEDIA_SELECT_WWW                 0x08A
#define HID_USAGE_CONSUMER_MEDIA_SELECT_DVD                 0x08B
#define HID_USAGE_CONSUMER_MEDIA_SELECT_TELEPHONE           0x08C
#define HID_USAGE_CONSUMER_MEDIA_SELECT_PROGRAM_GUIDE       0x08D
#define HID_USAGE_CONSUMER_MEDIA_SELECT_VIDEO_PHONE         0x08E
#define HID_USAGE_CONSUMER_MEDIA_SELECT_GAMES               0x08F
#define HID_USAGE_CONSUMER_MEDIA_SELECT_MESSAGES            0x090
#define HID_USAGE_CONSUMER_MEDIA_SELECT_CD                  0x091
#define HID_USAGE_CONSUMER_MEDIA_SELECT_VCR                 0x092
#define HID_USAGE_CONSUMER_MEDIA_SELECT_TUNER               0x093
#define HID_USAGE_CONSUMER_QUIT                             0x094
#define HID_USAGE_CONSUMER_HELP                             0x095
#define HID_USAGE_CONSUMER_MEDIA_SELECT_TAPE                0x096
#define HID_USAGE_CONSUMER_MEDIA_SELECT_CABLE               0x097
#define HID_USAGE_CONSUMER_MEDIA_SELECT_SATELLITE           0x098
#define HID_USAGE_CONSUMER_MEDIA_SELECT_SECURITY            0x099
#define HID_USAGE_CONSUMER_MEDIA_SELECT_HOME                0x09A
#define HID_USAGE_CONSUMER_MEDIA_SELECT_CALL                0x09B
#define HID_USAGE_CONSUMER_CHANNEL_INCREMENT                0x09C
#define HID_USAGE_CONSUMER_CHANNEL_DECREMENT                0x09D
#define HID_USAGE_CONSUMER_MEDIA_SELECT_SAP                 0x09E
#define HID_USAGE_CONSUMER_RESERVED                         0x09F
#define HID_USAGE_CONSUMER_VCR_PLUS                         0x0A0
#define HID_USAGE_CONSUMER_ONCE                             0x0A1
#define HID_USAGE_CONSUMER_DAILY                            0x0A2
#define HID_USAGE_CONSUMER_WEEKLY                           0x0A3
#define HID_USAGE_CONSUMER_MONTHLY                          0x0A4

#define HID_USAGE_CONSUMER_PLAY                             0x0B0
#define HID_USAGE_CONSUMER_PAUSE                            0x0B1
#define HID_USAGE_CONSUMER_RECORD                           0x0B2
#define HID_USAGE_CONSUMER_FAST_FORWARD                     0x0B3
#define HID_USAGE_CONSUMER_REWIND                           0x0B4
#define HID_USAGE_CONSUMER_SCAN_NEXT_TRACK                  0x0B5
#define HID_USAGE_CONSUMER_SCAN_PREV_TRACK                  0x0B6
#define HID_USAGE_CONSUMER_STOP                             0x0B7
#define HID_USAGE_CONSUMER_EJECT                            0x0B8
#define HID_USAGE_CONSUMER_RANDOM_PLAY                      0x0B9
#define HID_USAGE_CONSUMER_SELECT_DISC                      0x0BA
#define HID_USAGE_CONSUMER_ENTER_DISC                       0x0BB
#define HID_USAGE_CONSUMER_REPEAT                           0x0BC
#define HID_USAGE_CONSUMER_TRACKING                         0x0BD
#define HID_USAGE_CONSUMER_TRACK_NORMAL                     0x0BE
#define HID_USAGE_CONSUMER_SLOW_TRACKING                    0x0BF
#define HID_USAGE_CONSUMER_FRAME_FORWARD                    0x0C0
#define HID_USAGE_CONSUMER_FRAME_BACK                       0x0C1
#define HID_USAGE_CONSUMER_MARK                             0x0C2
#define HID_USAGE_CONSUMER_CLEAR_MARK                       0x0C3
#define HID_USAGE_CONSUMER_REPEAT_FROM_MARK                 0x0C4
#define HID_USAGE_CONSUMER_RETURN_TO_MARK                   0x0C5
#define HID_USAGE_CONSUMER_SEARCH_MARK_FORWARD              0x0C6
#define HID_USAGE_CONSUMER_SEARCK_MARK_BACKWARDS            0x0C7
#define HID_USAGE_CONSUMER_COUNTER_RESET                    0x0C8
#define HID_USAGE_CONSUMER_SHOW_COUNTER                     0x0C9
#define HID_USAGE_CONSUMER_TRACKING_INCREMENT               0x0CA
#define HID_USAGE_CONSUMER_TRACKING_DECREMENT               0x0CB
#define HID_USAGE_CONSUMER_STOP_EJECT                       0x0CC
#define HID_USAGE_CONSUMER_PLAY_PAUSE                       0x0CD
#define HID_USAGE_CONSUMER_PLAY_SKIP                        0x0CE

#define HID_USAGE_CONSUMER_VOLUME                           0x0E0
#define HID_USAGE_CONSUMER_BALANCE                          0x0E1
#define HID_USAGE_CONSUMER_MUTE                             0x0E2
#define HID_USAGE_CONSUMER_BASS                             0x0E3
#define HID_USAGE_CONSUMER_TREBLE                           0x0E4
#define HID_USAGE_CONSUMER_BASS_BOOST                       0x0E5
#define HID_USAGE_CONSUMER_SURROUND_MODE                    0x0E6
#define HID_USAGE_CONSUMER_LOUDNESS                         0x0E7
#define HID_USAGE_CONSUMER_MPX                              0x0E8
#define HID_USAGE_CONSUMER_VOLUME_INCREMENT                 0x0E9
#define HID_USAGE_CONSUMER_VOLUME_DECREMENT                 0x0EA

#define HID_USAGE_CONSUMER_SPEED_SELECT                     0x0F0
#define HID_USAGE_CONSUMER_PLAYBACK_SPEED                   0x0F1
#define HID_USAGE_CONSUMER_STANDARD_PLAY                    0x0F2
#define HID_USAGE_CONSUMER_LONG_PLAY                        0x0F3
#define HID_USAGE_CONSUMER_EXTENDED_PLAY                    0x0F4
#define HID_USAGE_CONSUMER_SLOW                             0x0F5

#define HID_USAGE_CONSUMER_FAN_ENABLE                       0x100
#define HID_USAGE_CONSUMER_FAN_SPEED                        0x101
#define HID_USAGE_CONSUMER_LIGHT_ENABLE                     0x102
#define HID_USAGE_CONSUMER_LIGHT_ILLUMINATION_LEVEL         0x103
#define HID_USAGE_CONSUMER_CLIMATE_CONTROL_ENABLE           0x104
#define HID_USAGE_CONSUMER_ROOM_TEMPERATURE                 0x105
#define HID_USAGE_CONSUMER_SECURITY_ENABLE                  0x106
#define HID_USAGE_CONSUMER_FIRE_ALARM                       0x107
#define HID_USAGE_CONSUMER_POLICE_ALARM                     0x108
#define HID_USAGE_CONSUMER_PROXIMITY                        0x109
#define HID_USAGE_CONSUMER_MOTION                           0x10A
#define HID_USAGE_CONSUMER_DURESS_ALARM                     0x10B
#define HID_USAGE_CONSUMER_HOLDUP_ALARM                     0x10C
#define HID_USAGE_CONSUMER_MEDICAL_ALARM                    0x10D
 
#define HID_USAGE_CONSUMER_BALANCE_RIGHT                    0x150
#define HID_USAGE_CONSUMER_BALANCE_LEFT                     0x151
#define HID_USAGE_CONSUMER_BASS_INCREMENT                   0x152
#define HID_USAGE_CONSUMER_BASS_DECREMENT                   0x153
#define HID_USAGE_CONSUMER_TREBLE_INCREMENT                 0x154
#define HID_USAGE_CONSUMER_TREBLE_DECREMENT                 0x155

#define HID_USAGE_CONSUMER_SPEAKER_SYSTEM                   0x160
#define HID_USAGE_CONSUMER_CHANNEL_LEFT                     0x161
#define HID_USAGE_CONSUMER_CHANNEL_RIGHT                    0x162
#define HID_USAGE_CONSUMER_CHANNEL_CENTER                   0x163
#define HID_USAGE_CONSUMER_CHANNEL_FRONT                    0x164
#define HID_USAGE_CONSUMER_CHANNEL_CENTER_FRONT             0x165
#define HID_USAGE_CONSUMER_CHANNEL_SIDE                     0x166
#define HID_USAGE_CONSUMER_CHANNEL_SURROUND                 0x167
#define HID_USAGE_CONSUMER_CHANNEL_LOW_FREQ_ENH             0x168
#define HID_USAGE_CONSUMER_CHANNEL_TOP                      0x169
#define HID_USAGE_CONSUMER_CHANNEL_UNKNOWN                  0x16A

#define HID_USAGE_CONSUMER_SUB_CHANNEL                      0x170
#define HID_USAGE_CONSUMER_SUB_CHANNEL_INCREMENT            0x171
#define HID_USAGE_CONSUMER_SUB_CHANNEL_DECREMENT            0x172
#define HID_USAGE_CONSUMER_ALTERNATE_AUDIO_INCREMENT        0x173
#define HID_USAGE_CONSUMER_ALTERNATE_AUDIO_DECREMENT        0x174

#define HID_USAGE_CONSUMER_APP_LAUNCH_BUTTONS               0x180
#define HID_USAGE_CONSUMER_AL_LAUNCH_BUTTON_CONFIG_TOOL     0x181
#define HID_USAGE_CONSUMER_AL_PROG_BUTTON_CONFIG            0x182
#define HID_USAGE_CONSUMER_AL_CONSUMER_CONTROL_CONFIG       0x183
#define HID_USAGE_CONSUMER_AL_WORD_PROCESSOR                0x184
#define HID_USAGE_CONSUMER_AL_TEXT_EDITOR                   0x185
#define HID_USAGE_CONSUMER_AL_SPREADSHEET                   0x186
#define HID_USAGE_CONSUMER_AL_GRAPHICS_EDITOR               0x187
#define HID_USAGE_CONSUMER_AL_PRESENTATION_APP              0x188
#define HID_USAGE_CONSUMER_AL_DATABASE_APP                  0x189
#define HID_USAGE_CONSUMER_AL_EMAIL_READER                  0x18A
#define HID_USAGE_CONSUMER_AL_NEWSREADER                    0x18B
#define HID_USAGE_CONSUMER_AL_VOICEMAIL                     0x18C
#define HID_USAGE_CONSUMER_AL_CONTACTS_ADDESSBOOK           0x18D
#define HID_USAGE_CONSUMER_AL_CALENDAR_SCHEDULE             0x18E
#define HID_USAGE_CONSUMER_AL_TASK_PROJECT_MANAGER          0x18F
#define HID_USAGE_CONSUMER_AL_LOG_JOURNAL_TIMECARD          0x190
#define HID_USAGE_CONSUMER_AL_CHECKBOOK_FINANCE             0x191
#define HID_USAGE_CONSUMER_AL_CALCULATOR                    0x192
#define HID_USAGE_CONSUMER_AL_AV_CAPTURE_PLAYBACK           0x193
#define HID_USAGE_CONSUMER_AL_LOCAL_MACHINE_BROWSER         0x194
#define HID_USAGE_CONSUMER_AL_LAN_WAN_BROWSER               0x195
#define HID_USAGE_CONSUMER_AL_INTERNET_BROWSER              0x196
#define HID_USAGE_CONSUMER_AL_REMOTE_NETWORKING_ISP_CONNECT 0x197
#define HID_USAGE_CONSUMER_AL_NETWORK_CONFERENCE            0x198
#define HID_USAGE_CONSUMER_AL_NETWORK_CHAT                  0x199
#define HID_USAGE_CONSUMER_AL_TELEPHONY_DIALER              0x19A
#define HID_USAGE_CONSUMER_AL_LOGON                         0x19B
#define HID_USAGE_CONSUMER_AL_LOGOFF                        0x19C
#define HID_USAGE_CONSUMER_AL_LOGON_LOGOFF                  0x19D
#define HID_USAGE_CONSUMER_AL_TERMINAL_LOCK_SCREENSAVER     0x19E
#define HID_USAGE_CONSUMER_AL_CONTROL_PANEL                 0x19F
#define HID_USAGE_CONSUMER_AL_COMMAND_LINE_PROCESSOR_RUN    0x1A0
#define HID_USAGE_CONSUMER_AL_PROCESS_TASK_MANAGER          0x1A1
#define HID_USAGE_CONSUMER_AL_SELECT_TASK_APP               0x1A2
#define HID_USAGE_CONSUMER_AL_NEXT_TASK_APP                 0x1A3
#define HID_USAGE_CONSUMER_AL_PREV_TASK_APP                 0x1A4
#define HID_USAGE_CONSUMER_AL_PREEMPTIVE_HALT_TASK_APP      0x1A5
#define HID_USAGE_CONSUMER_AL_INTEGRATED_HELP_CENTER        0x1A6
#define HID_USAGE_CONSUMER_AL_DOCUMENTS                     0x1A7
#define HID_USAGE_CONSUMER_AL_THESAURUS                     0x1A8
#define HID_USAGE_CONSUMER_AL_DICTIONARY                    0x1A9
#define HID_USAGE_CONSUMER_AL_DESKTOP                       0x1AA
#define HID_USAGE_CONSUMER_AL_SPELL_CHECK                   0x1AB
#define HID_USAGE_CONSUMER_AL_GRAMMAR_CHECK                 0x1AC
#define HID_USAGE_CONSUMER_AL_WIRELESS_STATUS               0x1AD
#define HID_USAGE_CONSUMER_AL_KEYBOARD_LAYOUT               0x1AE
#define HID_USAGE_CONSUMER_AL_VIRUS_PROTECTION              0x1AF
#define HID_USAGE_CONSUMER_AL_ENCRYPTION                    0x1B0
#define HID_USAGE_CONSUMER_AL_SCREENSAVER                   0x1B1
#define HID_USAGE_CONSUMER_AL_ALARMS                        0x1B2
#define HID_USAGE_CONSUMER_AL_CLOCK                         0x1B3
#define HID_USAGE_CONSUMER_AL_FILE_BROWSER                  0x1B4
#define HID_USAGE_CONSUMER_AL_POWER_STATUS                  0x1B5

#define HID_USAGE_CONSUMER_GENERIC_GUI_APP_CONTROLS         0x200
#define HID_USAGE_CONSUMER_AC_NEW                           0x201
#define HID_USAGE_CONSUMER_AC_OPEN                          0x202
#define HID_USAGE_CONSUMER_AC_CLOSE                         0x203
#define HID_USAGE_CONSUMER_AC_EXIT                          0x204
#define HID_USAGE_CONSUMER_AC_MAXIMIZE                      0x205
#define HID_USAGE_CONSUMER_AC_MINIMIZE                      0x206
#define HID_USAGE_CONSUMER_AC_SAVE                          0x207
#define HID_USAGE_CONSUMER_AC_PRINT                         0x208
#define HID_USAGE_CONSUMER_AC_PROPERTIES                    0x209

#define HID_USAGE_CONSUMER_AC_UNDO                          0x21A
#define HID_USAGE_CONSUMER_AC_COPY                          0x21B
#define HID_USAGE_CONSUMER_AC_CUT                           0x21C
#define HID_USAGE_CONSUMER_AC_PASTE                         0x21D
#define HID_USAGE_CONSUMER_AC_SELECT_ALL                    0x21E
#define HID_USAGE_CONSUMER_AC_FIND                          0x21F
#define HID_USAGE_CONSUMER_AC_FIND_AND_REPLACE              0x220
#define HID_USAGE_CONSUMER_AC_SEARCH                        0x221
#define HID_USAGE_CONSUMER_AC_GO_TO                         0x222
#define HID_USAGE_CONSUMER_AC_HOME                          0x223
#define HID_USAGE_CONSUMER_AC_BACK                          0x224
#define HID_USAGE_CONSUMER_AC_FORWARD                       0x225
#define HID_USAGE_CONSUMER_AC_STOP                          0x226
#define HID_USAGE_CONSUMER_AC_REFRESH                       0x227
#define HID_USAGE_CONSUMER_AC_PREV_LINK                     0x228
#define HID_USAGE_CONSUMER_AC_NEXT_LINK                     0x229
#define HID_USAGE_CONSUMER_AC_BOOKMARKS                     0x22A
#define HID_USAGE_CONSUMER_AC_HISTORY                       0x22B
#define HID_USAGE_CONSUMER_AC_SUBSCRIPTIONS                 0x22C
#define HID_USAGE_CONSUMER_AC_ZOOM_IN                       0x22D
#define HID_USAGE_CONSUMER_AC_ZOOM_OUT                      0x22E
#define HID_USAGE_CONSUMER_AC_ZOOM                          0x22F
#define HID_USAGE_CONSUMER_AC_FULL_SCREEN_VIEW              0x230
#define HID_USAGE_CONSUMER_AC_NORMAL_VIEW                   0x231
#define HID_USAGE_CONSUMER_AC_VIEW_TOGGLE                   0x232
#define HID_USAGE_CONSUMER_AC_SCROLL_UP                     0x233
#define HID_USAGE_CONSUMER_AC_SCROLL_DOWN                   0x234
#define HID_USAGE_CONSUMER_AC_SCROLL                        0x235
#define HID_USAGE_CONSUMER_AC_PAN_LEFT                      0x236
#define HID_USAGE_CONSUMER_AC_PAN_RIGHT                     0x237
#define HID_USAGE_CONSUMER_AC_PAN                           0x238
#define HID_USAGE_CONSUMER_AC_NEW_WINDOW                    0x239
#define HID_USAGE_CONSUMER_AC_TILE_HORIZONTALLY             0x23A
#define HID_USAGE_CONSUMER_AC_TILE_VERTICALLY               0x23B
#define HID_USAGE_CONSUMER_AC_FORMAT                        0x23C
#define HID_USAGE_CONSUMER_AC_EDIT                          0x23D
#define HID_USAGE_CONSUMER_AC_BOLD                          0x23E
#define HID_USAGE_CONSUMER_AC_ITALICS                       0x23F
#define HID_USAGE_CONSUMER_AC_UNDERLINE                     0x240
#define HID_USAGE_CONSUMER_AC_STRIKETHROUGH                 0x241
#define HID_USAGE_CONSUMER_AC_SUBSCRIPT                     0x242
#define HID_USAGE_CONSUMER_AC_SUPERSCRIPT                   0x243
#define HID_USAGE_CONSUMER_AC_ALL_CAPS                      0x244
#define HID_USAGE_CONSUMER_AC_ROTATE                        0x245
#define HID_USAGE_CONSUMER_AC_RESIZE                        0x246
#define HID_USAGE_CONSUMER_AC_FLIP_HORIZONTAL               0x247
#define HID_USAGE_CONSUMER_AC_FLIP_VERTICAL                 0x248
#define HID_USAGE_CONSUMER_AC_MIRROR_HORIZONTAL             0x249
#define HID_USAGE_CONSUMER_AC_MIRROR_VERTICAL               0x24A
#define HID_USAGE_CONSUMER_AC_FONT_SELECT                   0x24B
#define HID_USAGE_CONSUMER_AC_FONT_COLOR                    0x24C
#define HID_USAGE_CONSUMER_AC_FONT_SIZE                     0x24D
#define HID_USAGE_CONSUMER_AC_JUSTIFY_LEFT                  0x24E
#define HID_USAGE_CONSUMER_AC_JUSTIFY_CENTER_H              0x24F
#define HID_USAGE_CONSUMER_AC_JUSTIFY_RIGHT                 0x250
#define HID_USAGE_CONSUMER_AC_JUSTIFY_BLOCK_H               0x251
#define HID_USAGE_CONSUMER_AC_JUSTIFY_TOP                   0x252
#define HID_USAGE_CONSUMER_AC_JUSTIFY_CENTER_V              0x253
#define HID_USAGE_CONSUMER_AC_JUSTIFY_BOTTOM                0x254
#define HID_USAGE_CONSUMER_AC_JUSTIFY_BLOCK_V               0x255
#define HID_USAGE_CONSUMER_AC_INDENT_DECREASE               0x256
#define HID_USAGE_CONSUMER_AC_INDENT_INCREASE               0x257
#define HID_USAGE_CONSUMER_AC_NUMBERED_LIST                 0x258
#define HID_USAGE_CONSUMER_AC_RESTART_NUMBERING             0x259
#define HID_USAGE_CONSUMER_AC_BULLETED_LIST                 0x25A
#define HID_USAGE_CONSUMER_AC_PROMOTE                       0x25B
#define HID_USAGE_CONSUMER_AC_DEMOTE                        0x25C
#define HID_USAGE_CONSUMER_AC_YES                           0x25D
#define HID_USAGE_CONSUMER_AC_NO                            0x25E
#define HID_USAGE_CONSUMER_AC_CANCEL                        0x25F
#define HID_USAGE_CONSUMER_AC_CATALOG                       0x260
#define HID_USAGE_CONSUMER_AC_BUY_CHECKOUT                  0x261
#define HID_USAGE_CONSUMER_AC_ADD_TO_CART                   0x262
#define HID_USAGE_CONSUMER_AC_EXPAND                        0x263
#define HID_USAGE_CONSUMER_AC_EXPAND_ALL                    0x264
#define HID_USAGE_CONSUMER_AC_COLLAPSE                      0x265
#define HID_USAGE_CONSUMER_AC_COLLAPSE_ALL                  0x266
#define HID_USAGE_CONSUMER_AC_PRINT_PREVIEW                 0x267
#define HID_USAGE_CONSUMER_AC_PASTE_SPECIAL                 0x268
#define HID_USAGE_CONSUMER_AC_INSERT_MODE                   0x269
#define HID_USAGE_CONSUMER_AC_DELETE                        0x26A
#define HID_USAGE_CONSUMER_AC_LOCK                          0x26B
#define HID_USAGE_CONSUMER_AC_UNLOCK                        0x26C
#define HID_USAGE_CONSUMER_AC_PROTECT                       0x26D
#define HID_USAGE_CONSUMER_AC_UNPROTECT                     0x26E
#define HID_USAGE_CONSUMER_AC_ATTACH_COMMENT                0x26F
#define HID_USAGE_CONSUMER_AC_DELETE_COMMENT                0x270
#define HID_USAGE_CONSUMER_AC_VIEW_COMMENT                  0x271
#define HID_USAGE_CONSUMER_AC_SELECT_WORD                   0x272
#define HID_USAGE_CONSUMER_AC_SELECT_SENTENCE               0x273
#define HID_USAGE_CONSUMER_AC_SELECT_PARAGRAPH              0x274
#define HID_USAGE_CONSUMER_AC_SELECT_COLUMN                 0x275
#define HID_USAGE_CONSUMER_AC_SELECT_ROW                    0x276
#define HID_USAGE_CONSUMER_AC_SELECT_TABLE                  0x277
#define HID_USAGE_CONSUMER_AC_SELECT_OBJECT                 0x278
#define HID_USAGE_CONSUMER_AC_REDO_REPEAT                   0x279
#define HID_USAGE_CONSUMER_AC_SORT                          0x27A
#define HID_USAGE_CONSUMER_AC_SORT_ASCENDING                0x27B
#define HID_USAGE_CONSUMER_AC_SORT_DESCENDING               0x27C
#define HID_USAGE_CONSUMER_AC_FILTER                        0x27D
#define HID_USAGE_CONSUMER_AC_SET_CLOCK                     0x27E
#define HID_USAGE_CONSUMER_AC_VIEW_CLOCK                    0x27F
#define HID_USAGE_CONSUMER_AC_SELECT_TIME_ZONE              0x280
#define HID_USAGE_CONSUMER_AC_EDIT_TIME_ZONES               0x281
#define HID_USAGE_CONSUMER_AC_SET_ALARM                     0x282
#define HID_USAGE_CONSUMER_AC_CLEAR_ALARM                   0x283
#define HID_USAGE_CONSUMER_AC_SNOOZE_ALARM                  0x284
#define HID_USAGE_CONSUMER_AC_RESET_ALARM                   0x285
#define HID_USAGE_CONSUMER_AC_SYNCHRONIZE                   0x286
#define HID_USAGE_CONSUMER_AC_SEND_RECEIVE                  0x287
#define HID_USAGE_CONSUMER_AC_SEND_TO                       0x288
#define HID_USAGE_CONSUMER_AC_REPLY                         0x289
#define HID_USAGE_CONSUMER_AC_REPLY_ALL                     0x28A
#define HID_USAGE_CONSUMER_AC_FORWARD_MSG                   0x28B
#define HID_USAGE_CONSUMER_AC_SEND                          0x28C
#define HID_USAGE_CONSUMER_AC_ATTACH_FILE                   0x28D
#define HID_USAGE_CONSUMER_AC_UPLOAD                        0x28E
#define HID_USAGE_CONSUMER_AC_DOWNLOAD                      0x28F
#define HID_USAGE_CONSUMER_AC_SET_BORDERS                   0x290
#define HID_USAGE_CONSUMER_AC_INSERT_ROW                    0x291
#define HID_USAGE_CONSUMER_AC_INSERT_COLUMN                 0x292
#define HID_USAGE_CONSUMER_AC_INSERT_FILE                   0x293
#define HID_USAGE_CONSUMER_AC_INSERT_PICTURE                0x294
#define HID_USAGE_CONSUMER_AC_INSERT_OBJECT                 0x295
#define HID_USAGE_CONSUMER_AC_INSERT_SYMBOL                 0x296
#define HID_USAGE_CONSUMER_AC_SAVE_AND_CLOSE                0x297
#define HID_USAGE_CONSUMER_AC_RENAME                        0x298
#define HID_USAGE_CONSUMER_AC_MERGE                         0x299
#define HID_USAGE_CONSUMER_AC_SPLIT                         0x29A
#define HID_USAGE_CONSUMER_AC_DISTRIBUTE_HORIZONTALLY       0x29B
#define HID_USAGE_CONSUMER_AC_DISTRIBUTE_VERTICALLY         0x29C
#endif

