/* 
 * ****************************************************************************
 * hid_custom_cmd.h
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

#ifndef _HID_CUSTOM_CMD_H_
#define _HID_CUSTOM_CMD_H_

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define HID_CUSTOM_REPORT_ID    0x5A

#define HCC_CHECK_COUNT()   \
	do{                     \
		if(HCC_COUNT > 10)  \
			HCC_COUNT = 10; \
	}while(0)


enum _ENUM_HID_CUSTOM_CMD_
{
    HCC_GET_SENSOR_DATA = 0,
    HCC_SET_SENSOR_DATA,
    HCC_DO_SAVE_DATA_TO_FLASH,
    HCC_DO_SELF_TEST,
    HCC_DO_CALIBRATE,
    HCC_READ_MEM,
    HCC_WRITE_MEM,
    HCC_READ_REG,
    HCC_WRITE_REG,
    HCC_SWITCH_TO_BBK,
    HCC_GET_VERSION
};

/* 
 * ****************************************************************************
 * Function prototype
 * ****************************************************************************
 */
extern int hcc_do_custom_cmd(UINT8 *pBuf);
extern int hcc_get_custom_data(UINT8 *pBuf);

#endif

