/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * hid_hal.c
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

#if EN_FULL_USB
/**
 * ****************************************************************************
 * HID (USB or I2C) interrupt service routine.
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void HID_CODE hid_isr(void)
{
    usb_hid_isr();
}

/**
 * ****************************************************************************
 * HID service main routine.
 *
 * @return
 *
 * @parameter
 *
 * @ note
 * 1ms time based
 *
 * ****************************************************************************
 */
void HID_CODE hid_main(void)
{
    switch(HID_INTERFACE_STEP)
    {
        case HID_INTERFACE_STEP_INIT:

            /* Init HID */
            hid_init();


            /* next step */
            HID_INTERFACE_STEP = HID_INTERFACE_STEP_DEVICE;
            break;

        case HID_INTERFACE_STEP_DEVICE:
#if 1			
            /* next step */
            HID_INTERFACE_STEP = HID_INTERFACE_STEP_SERVICE;
#endif			
            break;

        case HID_INTERFACE_STEP_SERVICE:
            hid_service();
            break;

        default:
            HID_INTERFACE_STEP = HID_INTERFACE_STEP_INIT;
            break;
    }
}

#endif
