/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * usb_slave_hid.c
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

#if EN_FULL_USB
/**
 * ****************************************************************************
 * get feature report of HID over USB
 *
 * @return
 * bytes to return
 *
 * @parameter
 * u8ReportID
 * u16Len
 * *pBuf
 * u16Interface
 *
 * ****************************************************************************
 */
UINT32 HID_CODE usb_slave_hid_get_feature_report(
    UINT8 u8ReportID, UINT16 u16Len, UINT8 *pBuf, UINT16 u16Interface)
{
    if(0 == u16Interface){
    	return hid_get_feature_report(u8ReportID, u16Len, pBuf);
    }

    return 0;
}

/**
 * ****************************************************************************
 * set feature report of HID over USB
 *
 * @return
 *
 * @parameter
 * u8ReportID
 * u16Len
 * *pBuf
 * u16Interface
 *
 * ****************************************************************************
 */
UINT32  HID_CODE usb_slave_hid_set_feature_report(
    UINT8 u8ReportID, UINT16 u16Len, UINT8 *pBuf, UINT16 u16Interface)
{
    if(0 == u16Interface){
        hid_set_feature_report(u8ReportID, u16Len, pBuf);
    }

    return 0;
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
int  HID_CODE usb_slave_hid_get_report_id(BOOL bClearINTC)
{
    static UINT8 u8CurID = 0;
    UINT8 u8IdCount = 0;
    
    for(;u8IdCount < sizeof(gucHIDINTC) ;u8CurID++,u8IdCount++)
    {
	    if(u8CurID == TOTAL_HID_DEVICE)
		    u8CurID = 0;
        if(0 == gucHIDInterval[u8CurID]||1 == gucHIDINTC[u8CurID])
        {
            /* Jyunlin 20140313 */
            if(TRUE == bClearINTC)
            {
                gucHIDINTC[u8CurID] = 0;
            }
            u8IdCount = u8CurID;
            u8CurID++;
	        return u8IdCount;        
        }        
    }
    return -1;
}





/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL HID_CODE usb_slave_hid_check_data_ready(void)
{
    return (0 <= usb_slave_hid_get_report_id(FALSE)) ? TRUE : FALSE;
}

/**
 * ****************************************************************************
 * To check any data need to send to host or not.
 *
 * @return
 * 1, no any data need send to host.
 *
 * @parameter
 * check_kb_signal, >0 to check key scan function is working
 *
 * ****************************************************************************
 */
BOOL HID_CODE usb_slave_hid_data_is_empty(BOOL check_kb_signal)
{
    BOOL bRet = TRUE;

    bRet = (!(scan.kbf_head != scan.kbf_tail));

    if(check_kb_signal>0)
    {
        bRet = (bRet && (F_Service_KEY == 0) && (KSI == 0xFF) &&
            (Timer_A.fbit.TMR_SCAN==0));
    }




    
    return  bRet;
}

/**
 * ****************************************************************************
 * handle class command
 *
 * @return
 *
 * @parameter
 * wReq
 * wVal
 * u16Len
 * *pBufIn
 * *pBufOut
 * u16Interface
 *
 * ****************************************************************************
 */
UINT32  HID_CODE usb_slave_hid_cmd_dispatch(UINT16 wReq, UINT16 wVal,
    UINT16 u16Len, UINT8 *pBufIn, UINT8 *pBufOut, UINT16 u16Interface)
{
	UINT8 type;
    int id;

	/* Report Type */
	type = (UINT8)(wVal >> 8);

	/* Report ID */
	id = (UINT8)(wVal);

	/* GET_REPORT */
	if(wReq == 0x01)
	{
		/* Feature Report */
		if(type == 0x03)
		{
			return usb_slave_hid_get_feature_report(id, u16Len, pBufIn,
                u16Interface);
		}
		/* Input Report */
		else
		{
			return 0;
		}
	}
	/* SET_REPORT */
	else if(wReq == 0x09)
	{
		return usb_slave_hid_set_feature_report(id, u16Len, pBufOut,
            u16Interface);
	}    
    /* Output reports */
	else if(wReq == 0x02)
	{
	}            
    /* for remote wake up */
	else if(wReq == 0xFF)
	{
        return 0;
	}            
	else
	{
        return 0;
	}

	return 0;
}

#endif

