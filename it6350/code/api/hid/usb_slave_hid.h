/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * usb_slave_hid.h
 * ****************************************************************************
 */

#ifndef __USB_SLAVE_HID_H__
#define __USB_SLAVE_HID_H__

/* 
 * ****************************************************************************
 * Function prototype
 * ****************************************************************************
 */
extern UINT32 usb_slave_hid_cmd_dispatch(UINT16 wReq, UINT16 wVal,
    UINT16 u16Len, UINT8 *pBufIn, UINT8 *pBufOut, UINT16 u16Interface);
extern BOOL usb_slave_hid_data_is_empty(BOOL check_kb_signal);
#endif /* __HID_MEMS_H__ */

