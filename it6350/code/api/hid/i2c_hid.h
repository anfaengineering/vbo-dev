/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * i2c_hid.h
 * Created on: 2012/8/15
 * Author: ite00595
 * ****************************************************************************
 */

#ifndef I2C_HID_H_
#define I2C_HID_H_

/* 
 * ****************************************************************************
 * debug
 * ****************************************************************************
 */
#undef __I2C_DBG_ENABLE__

#ifdef __I2C_DBG_ENABLE__
#define I2C_DBG	printf
#else
#define I2C_DBG(a,...)
#endif

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define I2C_HID_DESCRIPTOR_REPORT_LEN_OFFSET    0x04
#define I2C_HID_DESCRIPTOR_CMD_REG_OFFSET       0x10
#define I2C_HID_DESCRIPTOR_DATA_REG_OFFSET      0x12
#define I2C_HID_DESCRIPTOR_OUTPUT_REG_OFFSET    0x0C
#define I2C_HID_DESCRIPTOR_VENDORID_OFFSET      20
#define I2C_HID_DESCRIPTOR_PRODUCTID_OFFSET     22
#define I2C_HID_DESCRIPTOR_VERSION_OFFSET       24

#define I2C_HID_MAX_INPUT_SIZE                  0x1E

/* 
 * ****************************************************************************
 * global variables
 * ****************************************************************************
 */
//extern UINT8 gucTPInitDone;
//extern UINT16 gu16VendorID;
//extern UINT16 gu16ProductID;
//extern UINT16 gu16Version;
extern UINT16 guc_tp_buffer_head;
//extern UINT16 gu16ReportLen;
//extern UINT8 gucHIDTPRDataAddr[];
//extern UINT8 gucHIDTPWDataAddr[];
//extern UINT8 gucHIDTPDataAddr[];
//extern UINT16 gusHIDTPRSRAMAddr;
//extern UINT16 gusHIDTPWSRAMAddr;
//extern UINT16 gusHIDTPSRAMAddr;
extern UINT8 guc_tp_buffer[];
extern UINT16 guc_tp_buffer_address;
extern UINT8 guc_tp_buffer_set[];
extern UINT16 guc_tp_buffer_head;

//* ****************************************************************************
typedef struct _I2C_SLAVE{
	UINT8 ucCH;
	UINT8 ucSlave;
	UINT8 ucDataNum;
	UINT8 ucI2CMode;
	UINT8 ucDMAMode;
	UINT8 ucDMADataLen;
	//UINT8 *ucDTR 		= &pstI2CSlave->ucDTR[0];
	UINT16 usSramAddDst;
	UINT16 usSramAddSrc;
	UINT16 ucSlaveAdd2;	
}I2cSlaveStr;
/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void i2c_hid_tp_init(void);
void i2c_hid_tp_get_report_descriptor(BYTE **ppBuf, UINT16 *pu16Size);
UINT16 i2c_hid_tp_get_report_length();
UINT8 i2c_hid_tp_check_interrupt(void);
BOOL i2c_hid_tp_check_input(BYTE **ppBuf);
UINT16 i2c_hid_tp_get_report(UINT8 ucReportID,UINT8 ucWLen, UINT8* pRBuf);
void i2c_hid_tp_set_report(UINT8 ucReportID,UINT8 ucWLen, UINT8* pWBuf);
void i2c_hid_tp_output_report(UINT8 ucReportID,UINT8 ucWLen, UINT8* pWBuf);
UINT8 *i2c_hid_tp_vendor(
    UINT16 u16RegAddr, UINT8 *pu8Values, UINT16 u16ValueLen, UINT16 u16ReadLen);
UINT8 i2c_hid_tp_reset(void);
BOOL i2c_hid_read_tp_buffer(BYTE **ppBuf);
void i2c_hid_set_tp_buffer(void);
extern void I2C_Channel_WDT(void);


void I2C_Slave_Init(I2cSlaveStr *pstI2CSlave);
void I2C_Slave_Config(UINT8 ucCH, UINT8 ucSlave_Addr);
void I2C_Slave_Handler(UINT8 ucCH);
#endif /* I2C_HID_H_ */

