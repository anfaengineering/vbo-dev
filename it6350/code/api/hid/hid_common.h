/* 
 * ****************************************************************************
 * hid_common.h
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

#ifndef __HID_COMMON_H__
#define __HID_COMMON_H__

/* 
 * ****************************************************************************
 * definition
 * ****************************************************************************
 */
#define TOTAL_HID_DEVICE                1 

/* 
 * ****************************************************************************
 * global variables
 * ****************************************************************************
 */
extern volatile UINT8  gucHIDInitDone;
extern volatile UINT8  gucHIDINTC[TOTAL_HID_DEVICE];
extern volatile int    gucHIDLastTimer[TOTAL_HID_DEVICE];
extern UINT16          gucHIDInterval[TOTAL_HID_DEVICE];

/* 
 * ****************************************************************************
 * Function prototype
 * ****************************************************************************
 */
extern UINT8 hid_init(void);
extern void hid_service(void);
extern int hid_time_interval(void);

#endif
