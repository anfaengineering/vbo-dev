/* 
 * ****************************************************************************
 * hid_common.c
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

#if EN_FULL_USB
/* global variables for hid*/
volatile UINT8  gucHIDInitDone = 0;
volatile UINT8  gucHIDINTC[TOTAL_HID_DEVICE];
volatile int    gucHIDLastTimer[TOTAL_HID_DEVICE];
UINT16          gucHIDInterval[TOTAL_HID_DEVICE];

/**
 * ****************************************************************************
 * hid init
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT8 hid_init(void)
{
    UINT8  i; 

    for(i=0; i<(TOTAL_HID_DEVICE); i++)
    {
        gucHIDINTC[i]       = 0;         
        gucHIDLastTimer[i]  = 0;
        gucHIDInterval[i]   = 0;
    }    

	gtkeyboard_report.modifier = 0;
	gtkeyboard_report.reserved = 0;
	gtkeyboard_report.keycode[0] = 0;
	gtkeyboard_report.keycode[1] = 0;
	gtkeyboard_report.keycode[2] = 0;
	gtkeyboard_report.keycode[3] = 0;
	gtkeyboard_report.keycode[4] = 0;
	gtkeyboard_report.keycode[5] = 0;	

//    usb_slave_init();
    gucHIDInitDone = 1;

    return gucHIDInitDone;
}

/**
 * ****************************************************************************
 * hid service main
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void HID_CODE hid_service(void)
{
    /* disable usb interface interrupt */
    IER2 &= ~BIT7;
    _nop_();
    _nop_();
    _nop_();
    _nop_();
    usb_slave_main();

    /* enable usb interface interrupt */
    IER2 |= BIT7;
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
int HID_CODE hid_time_interval(void)
{
    int i = 0;   
    
    if (gucHIDInitDone==1)
    {        
        for( i = 0; i< (TOTAL_HID_DEVICE); i++ )
        {
            gucHIDLastTimer[i]++;
            if(gucHIDInterval[i] != 0 )
            {
                if(gucHIDLastTimer[i]>= gucHIDInterval[i] )
                {
                    gucHIDLastTimer[i] =  0;
                    gucHIDINTC[i] = 1;
                    return TRUE;
                }
            }
        }
    }
    return  FALSE;
}
#endif