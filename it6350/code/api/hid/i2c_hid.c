/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * i2c_hid.c
 * Created on: 2012/8/15
 * Author: ite00595
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

/* 
 * ****************************************************************************
 * global variables
 * ****************************************************************************
 */
//UINT8 gucHIDTPRDataAddr[512];
//UINT8 gucHIDTPWDataAddr[96];
//UINT8 gucHIDTPDataAddr[96];
//UINT16 gusHIDTPRSRAMAddr;
//UINT16 gusHIDTPWSRAMAddr;
//UINT16 gusHIDTPSRAMAddr;



//UINT16 gu16ReportLen = 0;
//UINT16 gu16VendorID = 0;
//UINT16 gu16ProductID = 0;
//UINT16 gu16Version = 0;
//UINT8 gucTPInitDone = 0;
#if 0
static UINT8 gucCmdReg = 0;
static UINT8 gucDataReg = 0;
static UINT8 gucOutputReg = 0;
static UINT16 gu16_wInputRegister = 0;
static UINT16 gu16_wMaxInputLength = 0;
#endif
//UINT16 gu16_InputReportReadLength = 0;




#if (EN_I2C_SLAVE == TRUE)
// * ****************************************************************************
UINT8 gusI2cSlaveRRam[256*6];
UINT8 gusI2cSlaveWRam[128*2+2];
UINT16 usWSRAM, usRSRAM;
void I2C_Slave_Init(I2cSlaveStr *pstI2CSlave)
{
	UINT8 ucCH 			= pstI2CSlave->ucCH;	
		
	ECReg(I2C_CONTROL(ucCH)) 	= 0x11;
	ECReg(I2C_CONTROL(ucCH)) 	= 0x00;
	ECReg(I2C_ADD(ucCH)) 		= (pstI2CSlave->ucSlave);
	ECReg(I2C_PRESCALE(ucCH)) 	= 0x7F;
	ECReg(I2C_TIME_OUT(ucCH)) 	= 0xFF;   	

	ECReg(I2C_ADD_H(ucCH)) 		= (UINT8)((pstI2CSlave->usSramAddDst & 0xFF00) >> 8); // High 0x96
	ECReg(I2C_ADD_L(ucCH)) 			= (UINT8)(pstI2CSlave->usSramAddDst & 0x00FF); // Low  0x97		    
	ECReg(I2C_DATA_NUM1(ucCH)) 	= pstI2CSlave->ucDataNum; // num usSramAddSrc
	ECReg(I2C_SLAVE_ADD1(ucCH))	= (UINT8) ((pstI2CSlave->usSramAddSrc & 0xFF00) >> 8);  
	ECReg(I2C_SLAVE_ADD2(ucCH))	= (UINT8)(pstI2CSlave->usSramAddSrc & 0x00FF); 			// 0x61			// 0x61    
	ECReg(I2C_CONTROL(ucCH))   		= 0x48;
	ECReg(I2C_CONTROL1(ucCH))  		= 0x02;  		
	ECReg(I2C_CONTROL2(ucCH))  		= pstI2CSlave->ucI2CMode;	
	ECReg(I2C_CONTROL3(ucCH)) 		= pstI2CSlave->ucDMAMode;
	ECReg(I2C_ADD2(ucCH)) 			= pstI2CSlave->ucSlaveAdd2;  	
	
	if(ucCH == 0){
		IER0 |= BIT4;
	}else if(ucCH == 1){
		IER19 |= BIT0;
	}else if(ucCH == 2){
		IER19 |= BIT1;	
	}


}
// * ****************************************************************************
void I2C_Slave_Config(UINT8 ucCH, UINT8 ucSlave_Addr)
{
	I2cSlaveStr	stI2CSlave;
	UINT16 i;

    	usWSRAM = (UINT16)(((UINT32)gusI2cSlaveWRam - 0x80000) + 0xD000);
    	usRSRAM = (UINT16)(((UINT32)gusI2cSlaveRRam - 0x80000) + 0xD000);

	for(i = 0; i < 128*4; i++){
		gusI2cSlaveRRam[i] = i+0x20;
	}
	
	if(ucCH < 3){
	    	stI2CSlave.ucCH  = ucCH;
		stI2CSlave.ucSlave = ucSlave_Addr;
		stI2CSlave.ucDataNum = 0xFF;
		stI2CSlave.ucI2CMode = 0x09;//0x09;
		stI2CSlave.ucDMAMode = 0x01;		
		stI2CSlave.usSramAddDst = usWSRAM;
		stI2CSlave.usSramAddSrc = usRSRAM;
		stI2CSlave.ucSlaveAdd2 = 0x00;
		stI2CSlave.ucDMADataLen = 0x00;		

		I2C_Slave_Init(&stI2CSlave);
	}
}
//#endif
// * ****************************************************************************
UINT8	ucI2COffset;
	volatile UINT8	ucI2CSta;// = ECReg(I2C_STAT(ucCH));
	UINT16 usOffset;// = gusI2cSlaveWRam[0];	
	UINT16	usRPtr;
	volatile UINT8 ucWLen;// = ECReg(I2C_NSWDST(ucCH));
void I2C_Slave_Handler(UINT8 ucCH)
{
		I2C_CTR_F |= BIT0;
GPDRG = 0;
	ucWLen = ECReg(I2C_NSWDST(ucCH));
	ucI2CSta = ECReg(I2C_STAT(ucCH));
	if(ucI2CSta & 0x04){
//		GPCRA4 = 0x80;
		
	}
	return;
	if((ucWLen == 0x00) && (ucI2CSta & 0x60) == 0x02){
GPDRG |= 0x08;		
		I2C_CTR_F |= BIT0;
		ucI2CSta = ECReg(I2C_STAT(ucCH));
		if((ucI2CSta & 0x60) == 0x02){
			return;
		}
	}		

	if(ucWLen  ||(ucI2CSta & 0xEB) == 0xE3){	
//		volatile UINT8 ucWLen = ECReg(I2C_NSWDST(ucCH));
GPDRG |= 0x10;
		if(ucWLen){
			usOffset = gusI2cSlaveWRam[0];	
			for(usRPtr = 1; usRPtr < ucWLen; usRPtr++){
				gusI2cSlaveRRam[usRPtr + usOffset - 1] = gusI2cSlaveWRam[usRPtr];
			}		
			usRPtr = usRSRAM + usOffset;
			ECReg(I2C_SLAVE_ADD1(ucCH))	= (UINT8)((usRPtr & 0xFF00) >> 8);  
			ECReg(I2C_SLAVE_ADD2(ucCH))	= (UINT8)(usRPtr & 0x00FF); 			// 0x61			// 0x61    				
		}
		I2C_CTR_F |= BIT0;			
	}else if(ucI2CSta == 0xD3){	// I2C-w Stop ack
GPDRG |= 0x20;	
//		volatile UINT8 ucWLen = ECReg(I2C_NSWDST(ucCH));
		usOffset = gusI2cSlaveWRam[0];	
		for(usRPtr = 1; usRPtr < ucWLen; usRPtr++){
			gusI2cSlaveRRam[usRPtr + usOffset - 1] = gusI2cSlaveWRam[usRPtr];
		}	
#if 0		
		for(usRPtr = 0; usRPtr < 0xFF; usRPtr++){
			if((usRPtr & 0xF) == 0x00){
				printf("\r\n");
			}
			printf("%02X  ", gusI2cSlaveRRam[usRPtr]);
		}
//		printf("len = 0x%X\n", ucWLen);
#endif		
	}
	if(ucI2CSta & I2C_STAT_TIMEOUT){
		ucI2CSta = ECReg(I2C_ERROR_STAT(ucCH));		
	}	
	I2C_CTR_F |= BIT0;	
GPDRG |= 0x80;	
#if 0
	if((ucI2CSta & 0xD7) == 0xC7){	// I2C-r ID ack
		ucOffset = gusI2cSlaveWRam[0];	
		usRPtr = usRSRAM + ucOffset;
		ECReg(I2C_SLAVE_ADD1(ucCH))	= (UINT8)((usRPtr & 0xFF00) >> 8);  
		ECReg(I2C_SLAVE_ADD2(ucCH))	= (UINT8)(usRPtr & 0x00FF); 			// 0x61			// 0x61    		
	}else if((ucI2CSta & 0xD7) == 0xD3){	// I2C-w Data stop
		UINT8 ucWLen = ECReg(I2C_NSWDST(ucCH));
		ucOffset = gusI2cSlaveWRam[0];		
		for(usRPtr = 1; usRPtr < ucWLen; usRPtr++){
			gusI2cSlaveRRam[usRPtr + ucOffset - 1] = gusI2cSlaveWRam[usRPtr];
		}
#if 0		
		for(usRPtr = 0; usRPtr < 0xFF; usRPtr++){
			if((usRPtr & 0xF) == 0x00){
				printf("\r\n");
			}
			printf("%02X  ", gusI2cSlaveRRam[usRPtr]);
		}
//		printf("len = 0x%X\n", ucWLen);
#endif
//		printf("b%02X\n", ucOffset);
	}else if((ucI2CSta & 0xD7) == 0xC3){
	}else if((ucI2CSta & 0xD7) == 0xC6){
	}
	printf("%02X\n", ucI2CSta);		
//	ucI2CSta = ECReg(I2C_ERROR_STAT(ucCH));
	if(ucI2CSta & I2C_STAT_TIMEOUT){
		I2C_CTR_F |= BIT0;
		ucI2CSta = ECReg(I2C_ERROR_STAT(ucCH));		
	}
#endif
}
// * ****************************************************************************

#endif

