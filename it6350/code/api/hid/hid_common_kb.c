/* 
 * ****************************************************************************
 * hid_common_kb.c
 * Copyright (c) ITE, All Rights Reserved
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h" 

#if EN_FULL_USB
keyboard_report_t gtkeyboard_report;


/* interface 0 report descriptor */
const UINT8 HID_DATA hid_report_descriptor[] =
{
    /* Custom Device */
    0x06, 0x85, 0xFF,   // USAGE_PAGE (Vender Defined Usage Page)
    0x09, 0x95,         // USAGE (Vendor Usage 0x01)
    0xA1, 0x01,         // COLLECTION (Application)
    0x85, HID_CUSTOM_REPORT_ID,     // REPORT_ID (90)
    0x09, 0x01,         // USAGE (Vendor Usage 0x01)
    0x15, 0x00,         // LOGICAL_MINIMUM(0)
    0x26, 0xff, 0x00,   // LOGICAL_MAXIMUM(255)
    0x75, 0x08,         // REPORT_SIZE (0x08)
    0x95, HID_CUSTOM_CMD_LENGTH,    // REPORT_COUNT (0x10)
    0xB1, 0x00,         // FEATURE (Data,Ary,Abs)
    0xC0,               // END_COLLECTION
    

};

/* interface 1 report descriptor */
const UINT8 HID_DATA hid_report_descriptor_2[] =
{
    0x05, 0x0c,         // USAGE_PAGE (Consumer Devices)
    0x09, 0x01,         // USAGE (Consumer Control)
    0xa1, 0x01,         // COLLECTION (Application)
    0x85, 0x01,         // REPORT_ID (1)
    0x15, 0x00,         // LOGICAL_MINIMUM (0)
    0x25, 0x01,         // LOGICAL_MAXIMUM (1)
    0x75, 0x01,         // REPORT_SIZE (1)
    0x95, 0x18,         // REPORT_COUNT (24)
    0x09, 0xe2,         // USAGE (Mute) 0x01
    0x09, 0xe9,         // USAGE (Volume Up) 0x02
    0x09, 0xea,         // USAGE (Volume Down) 0x03
    0x09, 0xcd,         // USAGE (Play/Pause) 0x04
    0x09, 0xb7,         // USAGE (Stop) 0x05
    0x09, 0xb6,         // USAGE (Scan Previous Track) 0x06
    0x09, 0xb5,         // USAGE (Scan Next Track) 0x07
    0x0a, 0x8a, 0x01,   // USAGE (Mail) 0x08
    0x0a, 0x92, 0x01,   // USAGE (Calculator) 0x09
    0x0a, 0x21, 0x02,   // USAGE (www search) 0x0a
    0x0a, 0x23, 0x02,   // USAGE (www home) 0x0b
    0x0a, 0x2a, 0x02,   // USAGE (www favorites) 0x0c
    0x0a, 0x27, 0x02,   // USAGE (www refresh) 0x0d
    0x0a, 0x26, 0x02,   // USAGE (www stop) 0x0e
    0x0a, 0x25, 0x02,   // USAGE (www forward) 0x0f
    0x0a, 0x24, 0x02,   // USAGE (www back) 0x10
    
    0x09, 0xE5,         // USAGE (Bass Boost) 0x11
    0x09, 0xE7,         // USAGE (Loudness) 0x12
    0x0a, 0x52, 0x01,   // USAGE (Bass Up) 0x13
    0x0a, 0x53, 0x01,   // USAGE (Bass Down) 0x14
    0x0a, 0x54, 0x01,   // USAGE (Treble up) 0x15
    0x0a, 0x55, 0x01,   // USAGE (Trebale down) 0x16
    0x0a, 0x83, 0x01,   // USAGE (Media Select) 0x17
    0x0a, 0x94, 0x01,   // USAGE (My Computer) 0x18
    0x81, 0x62,         // INPUT (Data,Var,Abs,NPrf,Null)
    0xc0,
};

/**
 * ****************************************************************************
 * HID get feature report
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT16 HID_CODE hid_get_feature_report(UINT8 u8ID, UINT16 u16Len, UINT8 *pBuf)
{
    UINT16 nBytesReturned;

    nBytesReturned = hcc_get_custom_data(pBuf);

    return nBytesReturned;
}

/**
 * ****************************************************************************
 * HID set feature report
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void HID_CODE hid_set_feature_report(UINT8 u8ID, UINT16 u16Len, UINT8 *pBuf)
{
  //  BYTE bTmpStat = 0;
    
    hcc_do_custom_cmd(pBuf);

}

#endif
