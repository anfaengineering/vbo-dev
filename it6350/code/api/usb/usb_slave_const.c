/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * usb_slave_const.c
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

#if EN_FULL_USB
/* Full speed Configuration */

const UINT8 HID_DATA u8FSDeviceDescriptor[DEVICE_LENGTH] =
{
	/* DEVICE descriptor : from 0 */

    /* bLength , 0x12 */
	DEVICE_LENGTH,

    /* bDescriptorType , 0x01 */
	DT_DEVICE,

    /* bcdUSB , 0x00 0x02 */
	mLowByte(FS_USB_SPEC_VER),
	mHighByte(FS_USB_SPEC_VER),

    /* bDeviceClass , 0x00 */
	FS_bDeviceClass,

    /* bDeviceSubClass */
	FS_bDeviceSubClass,

    /* bDeviceProtocol */
	FS_bDeviceProtocol,

    /* bMaxPacketSize0 */
	EP0MAXPACKETSIZE,

    /* idVendor */
	mLowByte(FS_VENDOR_ID),
	mHighByte(FS_VENDOR_ID),

    /* idProduct */
	mLowByte(FS_PRODUCT_ID),
	mHighByte(FS_PRODUCT_ID),

	/* bcdDeviceReleaseNumber */
	mLowByte(FS_DEVICE_RELEASE_NO),
	mHighByte(FS_DEVICE_RELEASE_NO),

    /* iManufacturer */
	FS_iManufacturer,

    /* iProduct */
	FS_iProduct,

	0x00,


    /* bNumConfigurations */
	FS_CONFIGURATION_NUMBER
};



const UINT8 HID_DATA u8FSConfigDescriptor01[FS_C1_CONFIG_TOTAL_LENGTH] =
{
	/* CONFIGURATION descriptor */

    /* bLength */
	CONFIG_LENGTH,

    /* bDescriptorType CONFIGURATION */
	DT_CONFIGURATION,

	/* wTotalLength, include all descriptors */
	mLowByte(FS_C1_CONFIG_TOTAL_LENGTH),
	mHighByte(FS_C1_CONFIG_TOTAL_LENGTH),

    /* bNumInterface */
	FS_C1_INTERFACE_NUMBER,

    /* bConfigurationValue */
	FS_C1,

    /* iConfiguration */
	FS_C1_iConfiguration,

    /* 
     * bmAttribute
     * D7: Reserved(set to one)
     * D6: Self-powered
     * D5: Remote Wakeup
     * D4..0: Reserved(reset to zero)
     */
	FS_C1_bmAttribute,


	0x32,


	/* Interface 0 */

        /* Alternate Setting 0 */

	    /* bLength */
		INTERFACE_LENGTH,

	    /* bDescriptorType INTERFACE */
		DT_INTERFACE,

        /* bInterfaceNumber */
		FS_C1_I0_A0_bInterfaceNumber,

        /* bAlternateSetting */
		FS_C1_I0_A0_bAlternateSetting,

	    /* bNumEndpoints(excluding endpoint zero) */
		FS_C1_I0_A0_EP_NUMBER,

        /* bInterfaceClass */
		FS_C1_I0_A0_bInterfaceClass,

        /* bInterfaceSubClass */
		FS_C1_I0_A0_bInterfaceSubClass,

        /* bInterfaceProtocol */
		FS_C1_I0_A0_bInterfaceProtocol,

	    /* iInterface */
		FS_C1_I0_A0_iInterface,

        /* HID descriptor */

        /* 0x09 ,  bLength */  
        HID_LENGTH,

        /* 0x21 , bDescriptorType */
        DT_HID,

        /* bcdHID , low byte */
        0x10,

        /* bcdHID , high byte */
        0x01,

        /* bCountryCode */
        0x00,

        /* bNumDescriptor */
        0x01,

        /* bDescriptorType (report) */
        DT_REPORT_DESCRIPTOR,

        /* wDescriptorLen  low byte */
        mLowByte(HID_REPORT_DES_TOTAL_LENGTH),

        /* wDescriptorLen  high byte */
        mHighByte(HID_REPORT_DES_TOTAL_LENGTH),

        
        /* endpoint 1 */
		#if (FS_C1_I0_A0_EP_NUMBER >= 1)
			/* EP1 */

		    /* bLength */
			EP_LENGTH,

		    /* bDescriptorType ENDPOINT */
			DT_ENDPOINT,

            /*
	         * bEndpointAddress
		     * D7: Direction, 1=IN, 0=OUT
		     * D6..4: Reserved(reset to zero), D3..0: The endpointer number
             */
			(((1 - FS_C1_I0_A0_EP1_DIRECTION) << 7) | EP1),

            /*
    	     * bmAttributes
    	     * D1..0: Transfer Type 00=Control, 01=Isochronous,
             * 10=Bulk, 11=Interrupt
	         * if not an isochronous endpoint, D7..2 are Reserved
             */
            FS_C1_I0_A0_EP1_TYPE,

		    /* wMaxPacketSize */
            mLowByte(FS_C1_I0_A0_EP1_MAX_PACKET),
			mHighByte(FS_C1_I0_A0_EP1_MAX_PACKET),

		    /* Interval for polling endpoint for data transfers. */
			FS_C1_I0_A0_EP1_bInterval,
		#endif




};


/* Language ID */
const UINT8 HID_DATA u8String00Descriptor[STRING_00_LENGTH] =
{
    /*
     * STRING Descriptor type
     * Language ID, 0409: English, 0404: Chinese Taiwan
     */

    /* Size of this descriptor */
    STRING_00_LENGTH,

    /* bDescriptorType */
    0X03,

    /* bLang */
    0X09,
    0X04,
};

/* iManufacturer */
const UINT8 HID_DATA u8String10Descriptor[STRING_10_LENGTH] =
{
    /* Size of this descriptor */
    STRING_10_LENGTH,

    /* STRING Descriptor type */
    DT_STRING,
    0X49, 0,  //I
    0X54, 0,  //T
    0X45, 0,  //E
    0X20, 0,  // 
    0X54, 0,  //T
    0X65, 0,  //e
    0X63, 0,  //c
    0X68, 0,  //h
    0x2E, 0,  //.
    0x20, 0,  //
    0X49, 0,  //I
    0X6E, 0,  //n
    0X63, 0,  //c
    0X2E, 0   //.
};

/* iProduct */
const UINT8 HID_DATA u8String20Descriptor[STRING_20_LENGTH] =
{
    /* Size of this descriptor */
    STRING_20_LENGTH,

    /* STRING Descriptor type */
    DT_STRING,

    0X49, 0,  // I
    0X54, 0,  // T
    0X45, 0,  // E
    0X20, 0,  //
    0X44, 0,  // D
    0X65, 0,  // e
    0X76, 0,  // v
    0X69, 0,  // i
    0X63, 0,  // c
    0X65, 0,  // e
    0X28, 0,  // (
    0X38, 0,  // 8
    0X35, 0,  // 5 
    0X39, 0,  // 9
    0X35, 0,  // 5
    0X29, 0   // )  
};

/* iSerialNumber */
const UINT8 HID_DATA u8StringSerialNum[STRING_03_SERIAL_NUM_LENGTH] =
{
    0x1A, 0x03, //0x31, 0x00,
    //0x34, 0x00, 0x30, 0x00,
    //0x32, 0x00, 0x34, 0x00,
    0x49, 0x00,
    0x38, 0x00, 0x33, 0x00,
    0x38, 0x00, 0x30, 0x00,
    0x36, 0x00, 0x38, 0x00,
    0x39, 0x00, 0x46, 0x00,
    0x41, 0x00, 0x30, 0x00,
    0x38, 0x00
};

const UINT8 HID_DATA u8StringOSDescriptor[] =
{
    0x12,
    0x03,
    0x4D,0x00,
    0x53,0x00,
    0x46,0x00,
    0x54,0x00,
    0x31,0x00,
    0x30,0x00,
    0x30,0x00,

    /* " bMS_VendorCode */
    0x7F,
    
    0x00,
};

const UINT8 HID_DATA u8StringExtendPropertyDescriptor[] =
{
    /* Header Section */

    /* Size of this descriptor */
    0x4C, 0, 0, 0,

    /* BCD ver */
    0x00, 0x01,

    /* Extended property OS descriptor */
    0x05, 0,

    /* custom property count */ 
    0x01, 0,
   
    /* Custom Property Section */
    /* 66 Bytes for this property */
    0x42, 0, 0, 0,

    /* dwPropertyDataType (REG_DWORD_LITTLE_ENDIAN) */
    0x04, 0, 0, 0,

    /* wPropertyNameLength */
    0x30, 0, 
    0x53, 0x00, 0x65, 0x00, 0x6C, 0x00, 0x65, 0x00,
    0x63, 0x00, 0x74, 0x00, 0x69, 0x00, 0x76, 0x00,
    0x65, 0x00, 0x53, 0x00, 0x75, 0x00, 0x73, 0x00,
    0x70, 0x00, 0x65, 0x00, 0x6E, 0x00, 0x64, 0x00,
    0x45, 0x00, 0x6E, 0x00, 0x61, 0x00, 0x62, 0x00,
    0x6C, 0x00, 0x65, 0x00, 0x64, 0x00, 0x00, 0x00,

    /* dwPropertyDataLength */
    0x04, 0, 0, 0,

    /* bPropertyData */
    0x01, 0, 0, 0,
};

#endif
