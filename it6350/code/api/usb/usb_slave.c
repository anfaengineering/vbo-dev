/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * usb_slave.c
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\..\include.h"

#include "../hid/hid_custom_cmd.c"
#include "../hid/hid_common_kb.c"
#include "../hid/hid_common_tp.c"
#include "../hid/usb_slave_hid.c"
#include "./usb_slave_const.c"

#if EN_FULL_USB
/*
 * ****************************************************************************
 * Global Variables
 * ****************************************************************************
 */


UINT8 garu8USBDataBuf[FS_C1_HID_REPORT_DATA][FS_C1_HID_REPORT_DATA1] =
{
    {0x00}
};
const UINT32 garu32EPRegTable[][4] =
{
    {ENDPOINT0_CONTROL_REG,ENDPOINT0_STATUS_REG, EP0_TX_FIFO_CONTROL_REG,
        EP0_TX_FIFO_DATA},
    {ENDPOINT1_CONTROL_REG,ENDPOINT1_STATUS_REG, EP1_TX_FIFO_CONTROL_REG,
        EP1_TX_FIFO_DATA},
    #if(EP_MAX >= EP2)
    {ENDPOINT2_CONTROL_REG,ENDPOINT2_STATUS_REG, EP2_TX_FIFO_CONTROL_REG,
        EP2_TX_FIFO_DATA},
    #endif
    #if(EP_MAX >= EP3)
    {ENDPOINT3_CONTROL_REG,ENDPOINT3_STATUS_REG, EP3_TX_FIFO_CONTROL_REG,
        EP3_TX_FIFO_DATA},
    #endif
};

volatile BOOL gbUSBNoSuspend = FALSE;
static volatile UINT8 gu8USBResumeFlag = 0;
static BOOL gbUSBResumeSet = FALSE;
static BOOL gbUSBInitOK = FALSE;
static volatile BOOL gbUSBSuspended = FALSE;


USB_CONTROL_DATA gtUSBControl;
USB_CONTROL_DATA *gptUSBControl;
SETUP_DATA_PACKET *gptPacket;
SETUP_DATA_PACKET gtSetupDataPacket;

/*
 * ****************************************************************************
 * USB Slave functions
 * ****************************************************************************
 */
#ifdef __USB_SLAVE_DMA_MODE__
/**
 * ****************************************************************************
 * Function name: usb_slave_dma_start()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_dma_start(UINT8 endpoint, BOOL bIsRX, UINT32 ulAdd,
    UINT8 u8Count)
{
    /* DMA Write:  Data from USB RX FIFO to internal SRAM. */
    UINT8 u8DMACtrl = (bIsRX)?USB_DMA_CTRL_DIRECTION:0;

    if(ulAdd > GET_DATA_VMA_ADDR)
    {
    	ulAdd -= GET_DATA_VMA_ADDR;
    	ulAdd += 0xD000;
    }

    /* Set DMA ep point and dir */
    REG_WRITE_8BIT(USB_DMA_CTR_REG, u8DMACtrl);

    /* Set Count */
    REG_WRITE_8BIT(USB_DMA_TX_COUNT_REG, u8Count);

    /* Set Add */
    REG_WRITE_8BIT(USB_DMA_ADDRESS_7_0_REG,
        (UINT8)(ulAdd & 0x000000FF));
    REG_WRITE_8BIT(USB_DMA_ADDRESS_15_8_REG,
        (UINT8)((ulAdd >> 8)& 0x000000FF));
    REG_WRITE_8BIT(USB_DMA_ADDRESS_23_16_REG,
        (UINT8)((ulAdd >> 16)& 0x000000FF));
    REG_WRITE_8BIT(USB_DMA_ADDRESS_31_24_REG,
        (UINT8)((ulAdd >> 24)& 0x000000FF));
    u8DMACtrl |=
        ((endpoint<<USB_DMA_CTRL_EP_NUM_SHIFT)&USB_DMA_CTRL_EP_NUM_MASK);

    /* DMA Set */
    REG_WRITE_8BIT(USB_DMA_CTR_REG, u8DMACtrl|USB_DMA_CTRL_ENALBE);

    /* DMA Set */
    REG_WRITE_8BIT(USB_DMA_CTR_REG,
        u8DMACtrl|USB_DMA_CTRL_ENALBE|USB_DMA_CTRL_SET);
}

/**
 * ****************************************************************************
 * Function name: usb_slave_dma_wait()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_dma_wait(UINT8 u8EndPointNo)
{
    UINT8 u8DMADoneBit;
    UINT8 u8DMAErrorBit;

    switch(u8EndPointNo)
    {
        case 1:
            u8DMADoneBit = USB_DMA_INT_EP1_DONE;
            u8DMAErrorBit= USB_DMA_INT_EP1_ERROR;
            break;
        case 0:
        default:
            u8DMADoneBit = USB_DMA_INT_EP0_HOST_DONE;
            u8DMAErrorBit= USB_DMA_INT_EP0_HOST_ERROR;
            break;
    }
    while(0 == (REG_READ_8BIT(USB_DMA_INTERRUPT_STAT_REG)&u8DMADoneBit ))
    {
        if(usb_slave_check_reset())
            return FALSE;
    }
    REG_WRITE_8BIT(USB_DMA_INTERRUPT_STAT_REG,u8DMADoneBit);

   return TRUE;
}
#else
#define usb_slave_dma_wait(n)
#endif /* __USB_SLAVE_DMA_MODE__ */

/**
 * ****************************************************************************
 * Function name: usb_slave_read_fifo()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT8 USB_CODE usb_slave_read_fifo(
    UINT8 *pDataPacket, UINT8 u8Size, UINT8 u8EndPointNo, BOOL u8DMA)
{
    UINT32 u32RXDataRegAddr, u32EMSBRegAddr, u32ELSBRegAddr, u32CtrlRegAddr;
    UINT8 u8Index = 0;

    switch(u8EndPointNo)
    {
        case 1:
            u32RXDataRegAddr = EP1_RX_FIFO_DATA;
            u32EMSBRegAddr = EP1_RX_FIFO_DATA_COUNT_MSB;
            u32ELSBRegAddr = EP1_RX_FIFO_DATA_COUNT_LSB;
            u32CtrlRegAddr = EP1_RX_FIFO_CONTROL_REG;
            break;
        case 0:
        default:
            u32RXDataRegAddr = EP0_RX_FIFO_DATA;
            u32EMSBRegAddr = EP0_RX_FIFO_DATA_COUNT_MSB;
            u32ELSBRegAddr = EP0_RX_FIFO_DATA_COUNT_LSB;
            u32CtrlRegAddr = EP0_RX_FIFO_CONTROL_REG;
            break;
    }

    #ifdef __USB_SLAVE_DMA_MODE__
    if(u8DMA)
    {
        return TRUE;
    }
    #endif /* __USB_SLAVE_DMA_MODE__ */

    while((u8Size--) >0)
    {
        if(!((REG_READ_8BIT(u32EMSBRegAddr)==0)&&
            (REG_READ_8BIT(u32ELSBRegAddr)==0)))
        {
            *(pDataPacket+u8Index)= REG_READ_8BIT(u32RXDataRegAddr);
            u8Index++;
        }
        else
        {


            return u8Index;
        }
    }

    return u8Index;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_write_fifo()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_write_fifo(
    UINT8 *pu8Buffer, UINT8 u8Size, UINT8 u8EndPointNo, BOOL u8DMA)
{
    UINT8 u8Index = 0;

    #ifdef __USB_SLAVE_DMA_MODE__
    if(u8DMA)
    {
        usb_slave_dma_start(u8EndPointNo,FALSE, (UINT32)pu8Buffer, u8Size);
        return;
    }
    #endif /* __USB_SLAVE_DMA_MODE__ */

    /* TX_FIFO */
    REG_WRITE_8BIT(garu32EPRegTable[u8EndPointNo][USBEP_TX_FIFO_CON_REG], BIT0);


    for(u8Index = 0; u8Index < u8Size; u8Index++)
    {
        /* TX_FIFO */
        REG_WRITE_8BIT(garu32EPRegTable[u8EndPointNo][USBEP_TX_FIFO_DATA_REG],
            pu8Buffer[u8Index]);
    }
}

/**
 * ****************************************************************************
 * Function name: usb_slave_set_ep0_ready()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_set_ep0_ready(void)
{
    #ifdef __USB_SLAVE_DMA_MODE__
    usb_slave_dma_start(0, TRUE, (UINT32)garu8USBDataBuf[REPORT_IN], 0);
    #endif /* __USB_SLAVE_DMA_MODE__ */

    if(USB_IS_AUTOREADY())
    {
        REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,ENDPOINT_ENABLE_BIT);
    }
    else
    {
        REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,
            ENDPOINT_ENABLE_BIT|ENDPOINT_READY_BIT);
    }
}

/**
 * ****************************************************************************
 * Function name: usb_slave_is_ep_stall()
 *
 * @return
 *
 * @parameter
 * endpoint
 * bDir
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_is_ep_stall(UINT8 endpoint, BOOL bDir)
{
    return (0 !=
        (REG_READ_8BIT(GET_EP_CON_REG(endpoint)) & ENDPOINT_SEND_STALL_BIT));
}

/**
 * ****************************************************************************
 * Function name: usb_slave_set_ep_stall()
 *
 * @return
 *
 * @parameter
 * endpoint
 * bDir
 * bStall
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_set_ep_stall(UINT8 endpoint, BOOL bDir, BOOL bStall)
{
    UINT8 u8Con = REG_READ_8BIT(GET_EP_CON_REG(endpoint));
    if(bStall)
    {
        REG_WRITE_8BIT(GET_EP_CON_REG(endpoint),
            u8Con|ENDPOINT_SEND_STALL_BIT);
    }
    else
    {
        REG_WRITE_8BIT(GET_EP_CON_REG(endpoint),
            u8Con&(~ENDPOINT_SEND_STALL_BIT));
    }
}

/**
 * ****************************************************************************
 * Function name: usb_slave_ep_reset()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_ep_reset(void)
{
    usb_slave_set_ep0_ready();
    REG_WRITE_8BIT(ENDPOINT1_CONTROL_REG,ENDPOINT_ENABLE_BIT);
    #if(EP_MAX >= EP2)
    REG_WRITE_8BIT(ENDPOINT2_CONTROL_REG,ENDPOINT_ENABLE_BIT);
    #endif
    #if(EP_MAX >= EP3)
    REG_WRITE_8BIT(ENDPOINT3_CONTROL_REG,
        ENDPOINT_ENABLE_BIT|ENDPOINT_READY_BIT);
    #endif
}

/**
 * ****************************************************************************
 * Function name: usb_slave_reset()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_reset(void)
{
    if(!gptUSBControl->bIsReset)
    {
        USB_DBG_RESET_ENTER();
        usb_slave_sw_init(FALSE);

        REG_WRITE_8BIT(EP0_RX_FIFO_CONTROL_REG, BIT0);
        REG_WRITE_8BIT(EP0_TX_FIFO_CONTROL_REG, BIT0);
        REG_WRITE_8BIT(EP1_RX_FIFO_CONTROL_REG, BIT0);
        REG_WRITE_8BIT(EP1_TX_FIFO_CONTROL_REG, BIT0);
        REG_WRITE_8BIT(SC_ADDRESS,0x00);
        usb_slave_ep_reset();
        REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG,
            SC_NAK_SENT_INT_BIT|SC_SOF_RECEIVED_BIT);

        gptUSBControl->bIsReset = TRUE;
        USB_DBG_RESET_LEAVE();
        return TRUE;
    }
    return FALSE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_check_interrupt_data()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_check_interrupt_data(void)
{
    static UINT8 u8LastSeg = 0;
    #if(EP_MAX >= EP2)
    static UINT8 u8LastSeg2 = 0;
    #endif
    UINT8 u8Len = 0;
    #if(EP_MAX >= EP3)	
	UINT8 u8Stat;
	#endif
    if(0 == (REG_READ_8BIT(ENDPOINT1_CONTROL_REG) & ENDPOINT_READY_BIT))
    {


        u8Len = usb_slave_hid_cmd_dispatch(
            0, 0, 0,garu8USBDataBuf[REPORT_IN] , NULL, 0);
        if(u8Len > 0)
        {
            usb_slave_send_data_for_interrupt(
                garu8USBDataBuf[REPORT_IN], u8Len,u8LastSeg, 1);
            u8LastSeg = (u8LastSeg == 1)? 0: 1;
        }
    }
    #if(EP_MAX >= EP2)
    if(0 == (REG_READ_8BIT(ENDPOINT2_CONTROL_REG) & ENDPOINT_READY_BIT))
    {


        u8Len = usb_slave_hid_cmd_dispatch(
            0, 0, 0, garu8USBDataBuf[REPORT_IN] , NULL, 1);
        if(u8Len > 0)
        {
            usb_slave_send_data_for_interrupt(
                garu8USBDataBuf[REPORT_IN], u8Len,u8LastSeg2, 2);
            u8LastSeg2 = (u8LastSeg2 == 1)? 0: 1;
        }
    }
    #endif
    #if(EP_MAX >= EP3)
    if(0 == (REG_READ_8BIT(ENDPOINT3_CONTROL_REG) & ENDPOINT_READY_BIT))
    {         
        u8Len = 0;


        u8Stat = REG_READ_8BIT(ENDPOINT3_STATUS_REG);
        if((u8Stat&SC_CRC_ERROR_BIT)!=0)
        {
            REG_WRITE_8BIT(EP3_RX_FIFO_CONTROL_REG, BIT0);



            REG_WRITE_8BIT(ENDPOINT3_CONTROL_REG,
                ENDPOINT_READY_BIT|ENDPOINT_ENABLE_BIT);
            return;
        }
        else if((u8Stat&SC_BIT_STUFF_ERROR_BIT)!=0)
        {
            REG_WRITE_8BIT(EP3_RX_FIFO_CONTROL_REG, BIT0);



            REG_WRITE_8BIT(ENDPOINT3_CONTROL_REG,
                ENDPOINT_READY_BIT|ENDPOINT_ENABLE_BIT);
            return;
        }

        while(!((REG_READ_8BIT(EP3_RX_FIFO_DATA_COUNT_MSB)==0)&&
            (REG_READ_8BIT(EP3_RX_FIFO_DATA_COUNT_LSB)==0)))
        {
            garu8USBDataBuf[REPORT_IN][u8Len]= REG_READ_8BIT(EP3_RX_FIFO_DATA);

            u8Len++;
        }

        usb_slave_hid_cmd_dispatch(
            0x02, 0, u8Len , NULL, garu8USBDataBuf[REPORT_IN], 1);
        REG_WRITE_8BIT(ENDPOINT3_CONTROL_REG,
            ENDPOINT_READY_BIT|ENDPOINT_ENABLE_BIT);
    }
    #endif
}

/**
 * ****************************************************************************
 * Function name: usb_slave_check_reset()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_check_reset(void)
{
    BOOL bTmp = FALSE;
    if(0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & SC_RESET_EVENT_BIT))
    {
        bTmp = usb_slave_reset();
        REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG,SC_RESET_EVENT_BIT);
    }
    return bTmp;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_check_in_loop()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_check_in_loop(void)
{
    USB_DBG_CMD(USB_DBG_CODE_CHECK_RESET);
    usb_slave_check_reset();
    USB_DBG_CMD(USB_DBG_CODE_CHECK_RESET_OK);
}

/**
 * ****************************************************************************
 * Function name: usb_slave_check_transaction_type()
 *
 * @return
 *
 * @parameter
 * u8TransType
 * *bIsSetup
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_check_transaction_type(
    UINT8 u8TransType, BOOL *bIsSetup)
{
    UINT8 u8Type = REG_READ_8BIT(ENDPOINT0_TRANSTYPE_STATUS_REG);

    if(NULL != bIsSetup)
        *bIsSetup = FALSE;
    if(u8TransType == u8Type)
        return TRUE;

    if(USB_TRANSACTION_SETUP != u8Type)
    {

    }
    else
    {
        if(NULL != bIsSetup)
            *bIsSetup = TRUE;
    }

    return FALSE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_polling_nak_transaction_type()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
#define usb_slave_polling_nak_transaction_type(u8TransType) TRUE

/**
 * ****************************************************************************
 * Function name: usb_slave_send_data_for_interrupt()
 *
 * @return
 *
 * @parameter
 * *pu8Buffer
 * u16Num
 * u8DataSeq
 * u8EPNo
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_send_data_for_interrupt(UINT8 *pu8Buffer, UINT16 u16Num,
    UINT8 u8DataSeq, UINT8 u8EPNo)
{
    UINT8 tmp;

    usb_slave_write_fifo(pu8Buffer, (UINT8)u16Num, u8EPNo,TRUE);

    /* select data sequence and set ready */
    tmp = REG_READ_8BIT(garu32EPRegTable[u8EPNo][USBEP_CON_REG]);

    if(u8DataSeq)
    {
         REG_WRITE_8BIT(garu32EPRegTable[u8EPNo][USBEP_CON_REG],
            tmp|ENDPOINT_OUTDATA_SEQUENCE_BIT|ENDPOINT_READY_BIT);
    }
    else
    {
         REG_WRITE_8BIT(garu32EPRegTable[u8EPNo][USBEP_CON_REG],
            (tmp&(~ENDPOINT_OUTDATA_SEQUENCE_BIT))|ENDPOINT_READY_BIT);
    }

    return TRUE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_send_data()
 *
 * @return
 *
 * @parameter
 * *pu8Buffer
 * u8Num
 * u8DataSeq
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_send_data(
    UINT8 *pu8Buffer, UINT8 u8Num, UINT8 u8DataSeq)
{
    UINT8 tmp;

    usb_slave_write_fifo(pu8Buffer, u8Num, 0, (u8Num != 0));

    tmp = REG_READ_8BIT(ENDPOINT0_CONTROL_REG)|ENDPOINT_ENABLE_BIT;

    /* select data sequence and set ready */
    if(u8DataSeq)
    {
        REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,tmp|ENDPOINT_OUTDATA_SEQUENCE_BIT);
    }
    else
    {
        REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,
            (tmp&(~ENDPOINT_OUTDATA_SEQUENCE_BIT)));
    }
    REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,
        (REG_READ_8BIT(ENDPOINT0_CONTROL_REG)|ENDPOINT_READY_BIT));

    /* check Transaction Done bit */

    return TRUE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_status_stage_in()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_status_stage_in(void)
{
    UINT8 aru8Empty[1] = {0};

    USB_DBG_CMD(USB_DBG_CODE_STATUS_STAGE_IN_START);
    if(!(usb_slave_polling_nak_transaction_type(USB_TRANSACTION_IN)))
        return FALSE;

    gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_STATUS_IN;
    USB_DBG_STAGE(gptUSBControl->u8TransferStage);

    usb_slave_send_data(aru8Empty, 0, 1);

    USB_DBG_CMD(USB_DBG_CODE_STATUS_STAGE_IN_END);

    return TRUE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_status_stage_out()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_status_stage_out(void)
{
    USB_DBG_CMD(USB_DBG_CODE_STATUS_STAGE_OUT_START);
    if(!(usb_slave_polling_nak_transaction_type(USB_TRANSACTION_OUT)))
        return FALSE;

    gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_STATUS_OUT;
    USB_DBG_STAGE(gptUSBControl->u8TransferStage);

    REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,
        ENDPOINT_ENABLE_BIT|ENDPOINT_READY_BIT);

    USB_DBG_CMD(USB_DBG_CODE_STATUS_STAGE_OUT_END);

    return TRUE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_data_stage_in()
 *
 * @return
 *
 * @parameter
 * *pu8Buffer
 * u16Num
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_data_stage_in(UINT8 *pu8Buffer, UINT16 u16Num)
{
    UINT8 u8temp = 0;

    gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_DATA_IN;
    USB_DBG_STAGE(gptUSBControl->u8TransferStage);
    if(u16Num != 0)
    {
        if(u16Num < EP0MAXPACKETSIZE)
            u8temp = (UINT8)u16Num;
        else
            u8temp = EP0MAXPACKETSIZE;

        gptUSBControl->u8LastDataNum = u8temp;
        /* Transmit u8Temp bytes data */

        USB_DBG_CMD(USB_DBG_CODE_DATA_STAGE_IN_START);

        if(!(usb_slave_polling_nak_transaction_type(USB_TRANSACTION_IN)))
            return FALSE;

        std_memcpy(garu8USBDataBuf[0], pu8Buffer, u8temp);

        if(!usb_slave_send_data(garu8USBDataBuf[0],
            u8temp, gptUSBControl->u8EP0DataSeg))
        {
            return FALSE;
        }

        USB_DBG_CMD(USB_DBG_CODE_DATA_STAGE_IN_END);

    }

    /* end of the data stage */
    return TRUE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_recieve_data()
 *
 * @return
 *
 * @parameter
 * *pu8Buffer
 * u8Num
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_recieve_data(UINT8 *pu8Buffer, UINT8 u8Num)
{

    if(!(usb_slave_polling_nak_transaction_type(USB_TRANSACTION_OUT)))
        return FALSE;
    usb_slave_set_ep0_ready();

    if(USB_IS_AUTOREADY())
    {
        REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,
            ENDPOINT_ENABLE_BIT|ENDPOINT_READY_BIT);
    }

    return TRUE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_data_stage_out()
 *
 * @return
 *
 * @parameter
 * *pu8Buffer
 * u16Num
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_data_stage_out(UINT8 *pu8Buffer, UINT16 u16Num)
{
    gptUSBControl->u8LastDataNum = 0;
    gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_DATA_OUT;
    USB_DBG_STAGE(gptUSBControl->u8TransferStage);
    if(u16Num != 0)
    {
        if(u16Num < EP0MAXPACKETSIZE)
            gptUSBControl->u8LastDataNum = (UINT8)u16Num;
        else
            gptUSBControl->u8LastDataNum = EP0MAXPACKETSIZE;


        USB_DBG_CMD(USB_DBG_CODE_DATA_STAGE_OUT_START);
        if(!usb_slave_recieve_data(pu8Buffer, gptUSBControl->u8LastDataNum))
            return FALSE;

        USB_DBG_CMD(USB_DBG_CODE_DATA_STAGE_OUT_END);

    }

    return TRUE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_check_rx_status()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_check_rx_status(void)
{
    UINT8 u8Stat;
    u8Stat = REG_READ_8BIT(ENDPOINT0_STATUS_REG);
    if((u8Stat&SC_CRC_ERROR_BIT)!=0)
    {
        REG_WRITE_8BIT(EP0_RX_FIFO_CONTROL_REG, BIT0);

    }
    else if((u8Stat&SC_BIT_STUFF_ERROR_BIT)!=0)
    {
        REG_WRITE_8BIT(EP0_RX_FIFO_CONTROL_REG, BIT0);


    }
    else if((u8Stat&SC_RX_OVERFLOW_BIT)!=0)
    {
        REG_WRITE_8BIT(EP0_RX_FIFO_CONTROL_REG, BIT0);


    }

    else if((u8Stat&SC_RX_TIME_OUT_BIT)!=0)
    {
        REG_WRITE_8BIT(EP0_RX_FIFO_CONTROL_REG, BIT0);

    }
    else
    {
        return TRUE;
    }

    return FALSE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_check_tx_status()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL USB_CODE usb_slave_check_tx_status()
{
    UINT8 u8Stat;
    u8Stat = REG_READ_8BIT(ENDPOINT0_STATUS_REG);

    if((u8Stat& SC_RX_TIME_OUT_BIT)!=0)
    {

    }
    else
    {
        return TRUE;
    }

    return FALSE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_txdata()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_txdata(UINT8 *pu8Buffer, UINT16 u16Num, BOOL bBegin)
{

    if(bBegin)
    {
        gptUSBControl->pu8DataBuffer = pu8Buffer;
        gptUSBControl->u16TotalDataNum = u16Num;
        gptUSBControl->u8EP0DataSeg = 1;
    }
    else
    {
        if(0 != gptUSBControl->u16TotalDataNum && usb_slave_check_tx_status())
        {
            gptUSBControl->u16TotalDataNum -=
                (UINT16)(gptUSBControl->u8LastDataNum);
            gptUSBControl->pu8DataBuffer +=
                gptUSBControl->u8LastDataNum;
            gptUSBControl->u8EP0DataSeg =
                (gptUSBControl->u8EP0DataSeg == 1) ? 0 : 1;
        }
    }
    if(0 != gptUSBControl->u16TotalDataNum)
    {
        if(!(usb_slave_data_stage_in(
            gptUSBControl->pu8DataBuffer, gptUSBControl->u16TotalDataNum)))
        {
            return ERROR_USB_TX_SEND_FAIL;
        }
    }
    else
    {
        /* command abort */
        if(!usb_slave_check_transaction_type(USB_TRANSACTION_IN,NULL))
        {
            gptUSBControl->u8TransferStage = USB_TRANSACTION_SETUP;



            return USB_ERROR_SUCCESS;
        }
        if(!(usb_slave_status_stage_out()))
            return ERROR_USB_TX_STATUS_FAIL;
    }

    return USB_ERROR_SUCCESS;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_txdata()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_rxdata(UINT8 *pu8Buffer, UINT16 u16Num, BOOL bBegin)
{
    if(bBegin)
    {
        gptUSBControl->pu8DataBuffer = pu8Buffer;
        gptUSBControl->u16TotalDataNum = u16Num;
    }
    else
    {
        if(0 != gptUSBControl->u16TotalDataNum && usb_slave_check_rx_status())
        {
            /*
               if(pu8Buffer != garu8USBDataBuf[0])
               {
                    USB_DBG_ERROR_SET(USB_DBG_ERROR_BUF_BIT);
                    return ERROR_USB_RX_READFIFO_FAIL;
               }
             */

            if(gptUSBControl->u8LastDataNum != usb_slave_read_fifo(
                gptUSBControl->pu8DataBuffer,
                gptUSBControl->u8LastDataNum, 0, TRUE))
            {
                 return ERROR_USB_RX_READFIFO_FAIL;
            }

            gptUSBControl->pu8DataBuffer += gptUSBControl->u8LastDataNum;
            gptUSBControl->u16TotalDataNum -=
                (UINT16)gptUSBControl->u8LastDataNum;
        }
    }

    if(0 != gptUSBControl->u16TotalDataNum)
    {
        if(!(usb_slave_data_stage_out(
            gptUSBControl->pu8DataBuffer, gptUSBControl->u16TotalDataNum)))
        {
            return ERROR_USB_RX_RECEIVE_FAIL;
        }
    }
    else
    {
        if((gptPacket->bRequestType & REQUEST_TYPE_MASK) == REQUEST_TYPE_CLASS)
        {
            USB_DBG_SET_REPORT_SET();
            usb_slave_hid_cmd_dispatch(gptPacket->bRequest,
                                     *((UINT16 *)(gptPacket->wValue)),
                                     *((UINT16 *)(gptPacket->wLength)),
                                     NULL,
                                     garu8USBDataBuf[REPORT_OUT],
                                     *((UINT16 *)(gptPacket->wIndex)));

            USB_DBG_SET_REPORT_CLEAR();
        }
        else if((gptPacket->bRequestType & REQUEST_TYPE_MASK)
            == REQUEST_TYPE_VENDOR)
        {

        }

        /* command abort */
        if(!usb_slave_check_transaction_type(USB_TRANSACTION_OUT,NULL))
        {
            gptUSBControl->u8TransferStage = USB_TRANSACTION_SETUP;

            return USB_ERROR_SUCCESS;
        }

        if(!(usb_slave_status_stage_in()))
            return ERROR_USB_RX_STATUS_FAIL;
    }

    return USB_ERROR_SUCCESS;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_set_address()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_set_address(SETUP_DATA_PACKET *ptSetupDataPacket)
{
    UINT16 wValue = *((UINT16 *)(ptSetupDataPacket->wValue));
    gptUSBControl->u8Addr = wValue;
    usb_slave_status_stage_in();

    return USB_ERROR_SUCCESS;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_get_descriptor()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_get_descriptor(
    SETUP_DATA_PACKET *ptSetupDataPacket)
{
    UINT32 result = 0;
    UINT16 wValue = *((UINT16 *)(ptSetupDataPacket->wValue));
    const UINT8 *pu8DescriptorEX = NULL;
    UINT16 u16TxRxCounter = 0;




    switch((UINT8)(wValue >> 8))
    {
        case DT_DEVICE:
            u16TxRxCounter = u8FSDeviceDescriptor[0];

            pu8DescriptorEX = &u8FSDeviceDescriptor[0];

            break;

        case DT_CONFIGURATION:
            /* It includes Configuration, Interface and Endpoint Table */
            switch((UINT8)wValue)
            {
                case 0x00:      /* configuration no: 0 */
                    u16TxRxCounter = u8FSConfigDescriptor01[2] +
                        (u8FSConfigDescriptor01[3] << 8);

                    pu8DescriptorEX = &u8FSConfigDescriptor01[0];

                    break;

                default:
                    result = ERROR_USB_DEVICE_INVALID_CONFIGURATION_NUM;
                    goto end;
            }
            break;

        case DT_STRING:
            /* DescriptorIndex = low_byte of wValue */
            switch((UINT8)wValue)
            {
                /* Language ID */
                case 0x00:
                pu8DescriptorEX = &u8String00Descriptor[0];
                u16TxRxCounter = u8String00Descriptor[0];
                break;

                /* iManufacturer */
                case 0x01:
                pu8DescriptorEX = &u8String10Descriptor[0];
                u16TxRxCounter = u8String10Descriptor[0];
                break;

                case 0x02:
                pu8DescriptorEX = &u8String20Descriptor[0];
                u16TxRxCounter = u8String20Descriptor[0];
                break;

                /*
                case 0x30:
                pu8DescriptorEX = &u8String30Descriptor[0];
                u16TxRxCounter = u8String30Descriptor[0];
                break;

                case 0x40:
                pu8DescriptorEX = &u8String40Descriptor[0];
                u16TxRxCounter = u8String40Descriptor[0];
                break;

                case 0x50:
                pu8DescriptorEX = &u8String50Descriptor[0];
                u16TxRxCounter = u8String50Descriptor[0];
                break;

                case 0x03://iSerialNumber
                pu8DescriptorEX = &u8StringSerialNum[0];
                u16TxRxCounter = u8StringSerialNum[0];
                break;
                */

                case 0xEE:
                pu8DescriptorEX = &u8StringOSDescriptor[0];
                u16TxRxCounter = u8StringOSDescriptor[0];
                break;

                default:
                result = ERROR_USB_DEVICE_INVALID_STRING_NUM;
                goto end;
            }
            break;

        case DT_INTERFACE:
            /* 
             * It cannot be accessed individually,
             * it must follow "Configuraton"
             */
            break;

        case DT_ENDPOINT:
            /* 
             * It cannot be accessed individually,
             * it must follow "Configuraton"
             */
            break;

        case DT_HID:
            u16TxRxCounter = HID_LENGTH;
            if(0 == *((WORD *)(ptSetupDataPacket->wIndex)))
            {
                pu8DescriptorEX =
                    &u8FSConfigDescriptor01[CONFIG_LENGTH+INTERFACE_LENGTH];
            }
  
            break;

        case DT_DEVICE_QUALIFIER:
            result = ERROR_USB_DEVICE_INVALID_STRING_NUM;
            goto end;

        case DT_OTHER_SPEED_CONFIGURATION:
            result = ERROR_USB_DEVICE_INVALID_STRING_NUM;
            goto end;

        case DT_REPORT_DESCRIPTOR:
            if(0 == *((WORD *)(ptSetupDataPacket->wIndex)))
            {
                pu8DescriptorEX = &hid_report_descriptor[0];
                u16TxRxCounter = HID_REPORT_DES_TOTAL_LENGTH;
            }

            break;
        default:
            result = ERROR_USB_DEVICE_INVALID_DESCRIPTOR;
            goto end;
    }

    if(u16TxRxCounter > *((WORD *)(ptSetupDataPacket->wLength)))
        u16TxRxCounter = *((WORD *)(ptSetupDataPacket->wLength));


    usb_slave_txdata((UINT8 *)pu8DescriptorEX, u16TxRxCounter, TRUE);

end:
    /*
    if(result)
    {
        SC_DBG("USB_DEVICE_CxGetDescriptor() return error code 0x%08X \r\n",
            (UINT)result);
    }
     */
    return result;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_get_config()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_get_config(void)
{
    garu8USBDataBuf[REPORT_IN][0]= gptUSBControl->u8Config;
    usb_slave_txdata(garu8USBDataBuf[REPORT_IN], 1, TRUE);
    /* Not implemented */
    return USB_ERROR_SUCCESS;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_set_config()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_set_config(SETUP_DATA_PACKET *ptSetupDataPacket)
{
    gptUSBControl->u8Config = *((UINT16 *)(ptSetupDataPacket->wValue));
    usb_slave_status_stage_in();
    return USB_ERROR_SUCCESS;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_get_status()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_get_status(SETUP_DATA_PACKET *ptSetupDataPacket)
{
    UINT32 result = 0;
    UINT8 endpointIndex = 0;
    UINT8 fifoIndex = 0;
    UINT8  direction = 0;
    UINT8 RecipientStatusLow  = 0;
    UINT8 RecipientStatusHigh = 0;
    UINT8 u8Tmp[2] = {0};
    UINT8 u8Object =
        (UINT8)(ptSetupDataPacket->bRequestType & RECIPIENT_TYPE_MASK);
    UINT16 u16Index = *((UINT16 *)(ptSetupDataPacket->wIndex));

    /* Judge which recipient type is at first */
    switch(u8Object)
    {
        case RECIPIENT_TYPE_DEVICE:
            /*
             * Return 2-byte's Device status
             * (Bit1:Remote_Wakeup, Bit0:Self_Powered) to Host
             * Notice that the programe sequence of RecipientStatus
             */
            RecipientStatusLow = (gptUSBControl->u8RemoteWakeup)? BIT1 : 0;
            /* Bit0: Self_Powered--> DescriptorTable[0x23], D6(Bit 6) */
            RecipientStatusLow |= ((u8FSConfigDescriptor01[0x07] >> 6) & 0x01);
            break;

        case RECIPIENT_TYPE_INTERFACE:
            /* Return 2-byte ZEROs Interface status to Host */
            break;

        case RECIPIENT_TYPE_ENDPOINT:
            if(u16Index == 0x00)
            {
                RecipientStatusLow = (UINT8)gptUSBControl->bEPHalt;
            }
            else
            {
                /* which ep will be clear */
                endpointIndex = u16Index & 0x7F;

                /* the direction of this ep */
                direction = u16Index >> 7;

                /* over the Max. ep count ? */
                if(endpointIndex > DEVICE_MAX_ENDPOINT_NUM)
                {
                    result = ERROR_USB_DEVICE_INVALID_ENDPOINT_NUM;
                    goto end;
                }

                /* 
                 * USB_DEVICE_GetEndpointMapReg(endpointIndex, direction);
                 * get the relatived FIFO number
                 */
                fifoIndex = endpointIndex;

                /* over the Max. fifo count ? */
                if(fifoIndex >= DEVICE_MAX_FIFO_NUM)
                {
                    result = ERROR_USB_DEVICE_INVALID_FIFO_NUM;
                    goto end;
                }

                /* Check the FIFO had been enable ? */
                if(REG_READ_8BIT(GET_EP_CON_REG(endpointIndex)) == 0)
                {
                    result = ERROR_USB_DEVICE_FIFO_NOT_ENABLED;
                    goto end;
                }

                RecipientStatusLow =
                    usb_slave_is_ep_stall(endpointIndex, direction);
            }
            break;

        default :
            result = ERROR_USB_DEVICE_INVALID_RECIPIENT;
            goto end;
    }

    /* return RecipientStatus; */
    u8Tmp[0] = RecipientStatusLow;
    u8Tmp[1] = RecipientStatusHigh;

    usb_slave_txdata(u8Tmp, 2, TRUE);

end:

    return result;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_set_feature()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_set_feature(SETUP_DATA_PACKET *ptSetupDataPacket)
{
    UINT32 result = 0;
    UINT8 endpointIndex = 0;
    UINT8 fifoIndex = 0;
    BOOL direction = 0;
    UINT16 u16Value = *((UINT16 *)(ptSetupDataPacket->wValue));
    UINT16 u16Index = *((UINT16 *)(ptSetupDataPacket->wIndex));

    /* FeatureSelector */
    switch(u16Value)
    {
        case FEATURE_SEL_ENDPOINT_HALT:
            /*
             * Set "Endpoint_Halt",
             * Turn on the "STALL" bit in Endpoint Control Function Register
             */
            if(u16Index == 0x00)
                gptUSBControl->bEPHalt = TRUE;
            else
            {
                /* which ep will be clear */
                endpointIndex = u16Index & 0x7F;

                /* the direction of this ep */
                direction = u16Index >> 7;

                /* over the Max. ep count ? */
                if(endpointIndex > DEVICE_MAX_ENDPOINT_NUM)
                {
                    result = ERROR_USB_DEVICE_INVALID_ENDPOINT_NUM;
                    goto end;
                }

                /*
                 * USB_DEVICE_GetEndpointMapReg(endpointIndex, direction);
                 * get the relatived FIFO number
                 */
                fifoIndex = endpointIndex;
                /* over the Max. fifo count ? */
                if(fifoIndex >= DEVICE_MAX_FIFO_NUM)
                {
                    result = ERROR_USB_DEVICE_INVALID_FIFO_NUM;
                    goto end;
                }

                /* Check the FIFO had been enable ? */
                if((REG_READ_8BIT(GET_EP_CON_REG(endpointIndex))& BIT0) == 0)
                {
                    result = ERROR_USB_DEVICE_FIFO_NOT_ENABLED;
                    goto end;
                }

                /* Set Stall Bit */
                usb_slave_set_ep_stall(endpointIndex, direction, TRUE);
            }
            break;

        case FEATURE_SEL_DEVICE_REMOTE_WAKEUP:
            /*
             * Set "Device_Remote_Wakeup",
             * Turn on the"RMWKUP" bit in Mode Register
             * USB_DEVICE_EnableRemoteWakeupReg(MMP_TRUE);
             */
            gptUSBControl->u8RemoteWakeup = TRUE;
            break;

        default :
            result = ERROR_USB_DEVICE_INVALID_FEATURE_SEL;
            goto end;
    }

end:

    return result;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_clear_feature()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
INT32 USB_CODE usb_slave_cmd_clear_feature(SETUP_DATA_PACKET *ptSetupDataPacket)
{
    UINT32 result = 0;
    UINT8 endpointIndex = 0;
    UINT8 fifoIndex = 0;
    BOOL direction = 0;
    UINT16 u16Value = *((UINT16 *)(ptSetupDataPacket->wValue));
    UINT16 u16Index = *((UINT16 *)(ptSetupDataPacket->wIndex));

    /* FeatureSelector */
    switch(u16Value)
    {
        case FEATURE_SEL_ENDPOINT_HALT:
        /*
         * Clear "Endpoint_Halt",
         * Turn off the "STALL" bit in Endpoint Control Function Register
         */
        if(u16Index == 0x00)
                gptUSBControl->bEPHalt = FALSE;
        else
        {
            /* which ep will be clear */
            endpointIndex = u16Index & 0x7F;

            /* the direction of this ep */
            direction = u16Index >> 7;

            /* over the Max. ep count ? */
            if(endpointIndex > DEVICE_MAX_ENDPOINT_NUM)
            {
                result = ERROR_USB_DEVICE_INVALID_ENDPOINT_NUM;
                goto end;
            }

            /*
             * USB_DEVICE_GetEndpointMapReg(endpointIndex, direction);
             * get the relatived FIFO number
             */
            fifoIndex = endpointIndex;
            /* over the Max. fifo count ? */
            if(fifoIndex >= DEVICE_MAX_FIFO_NUM)
            {
                result = ERROR_USB_DEVICE_INVALID_FIFO_NUM;
                goto end;
            }

            /* Check the FIFO had been enable ? */
            if((REG_READ_8BIT(GET_EP_CON_REG(endpointIndex))& BIT0) == 0)
            {
                result = ERROR_USB_DEVICE_FIFO_NOT_ENABLED;
                goto end;
            }
            /* Set Stall Bit */
            usb_slave_set_ep_stall(endpointIndex, direction, FALSE);
        }
        break;

        case FEATURE_SEL_DEVICE_REMOTE_WAKEUP:
        /*
         * Clear "Device_Remote_Wakeup",
         * Turn off the"RMWKUP" bit in Main Control Register
         */
        gptUSBControl->u8RemoteWakeup = FALSE;
        break;

        case FEATURE_SEL_DEVICE_TEST_MODE:
        result = ERROR_USB_DEVICE_UNSUPPORT_FEATURE_SEL;
        goto end;

        default :
        result = ERROR_USB_DEVICE_INVALID_FEATURE_SEL;
        goto end;
    }

end:

    return result;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_std()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_std(SETUP_DATA_PACKET *ptSetupDataPacket)
{
    UINT32 result = 0;

    /* by Standard Request codes */
    switch(ptSetupDataPacket->bRequest)
    {
        case SET_ADDRESS:
            if(!gptUSBControl->bEPHalt)
                result = usb_slave_cmd_set_address(ptSetupDataPacket);
            break;

        case GET_DESCRIPTOR:
            if(!gptUSBControl->bEPHalt)
                result = usb_slave_cmd_get_descriptor(ptSetupDataPacket);
            break;

        case SET_FEATURE:
            usb_slave_cmd_set_feature(ptSetupDataPacket);
            usb_slave_status_stage_in();
            break;

        case GET_STATUS:
            usb_slave_cmd_get_status(ptSetupDataPacket);
            break;
            
        case CLEAR_FEATURE:
            usb_slave_cmd_clear_feature(ptSetupDataPacket);
            usb_slave_status_stage_in();
            break;
            
        case GET_CONFIGURATION:
            if(!gptUSBControl->bEPHalt)
                result = usb_slave_cmd_get_config();
            break;

        case SET_CONFIGURATION:
            if(!(gptUSBControl->bEPHalt))
                result = usb_slave_cmd_set_config(ptSetupDataPacket);
                break;

        default:
            result = ERROR_USB_DEVICE_INVALID_REQUEST_CODE;
            break;
    }


    return result;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_class()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_class(SETUP_DATA_PACKET *ptSetupDataPacket)
{
    UINT32 result = 0;
    UINT16 u16Length;
    static BYTE u8IdleRate = 0;

    /* by Standard Request codes */
    switch(ptSetupDataPacket->bRequest)
    {
        /* for warning temporarily (when HID is not enabled) */
        case 0:
            result = ERROR_USB_DEVICE_INVALID_REQUEST_CODE;
            goto end;

        /* for keyboard */
        /* Get IDLE */
        case GET_IDLE:
            u16Length = 1;
            garu8USBDataBuf[REPORT_IN][0]= u8IdleRate;
            usb_slave_txdata(garu8USBDataBuf[REPORT_IN], u16Length, TRUE);
            break;

        /* Shawn: for HID device */

        case GET_REPORT:  // GET_REPORT

            USB_DBG_GET_REPORT_SET();
            u16Length = (UINT16)usb_slave_hid_cmd_dispatch(
                        ptSetupDataPacket->bRequest,
                        *((UINT16 *)(ptSetupDataPacket->wValue)),
                        *((UINT16 *)(ptSetupDataPacket->wLength)),
                        garu8USBDataBuf[REPORT_IN],
                        NULL,
                        *((UINT16 *)(ptSetupDataPacket->wIndex)));
            if(u16Length == 0)
            {
                result = ERROR_USB_DEVICE_INVALID_VALUE;
                goto end;
            }
            USB_DBG_GET_REPORT_CLEAR();
            usb_slave_txdata(garu8USBDataBuf[REPORT_IN], u16Length, TRUE);

            break;

        /* SET_REPORT */
        case SET_REPORT:


            if(*((UINT16 *)(ptSetupDataPacket->wLength)) >
                sizeof(garu8USBDataBuf[REPORT_OUT]))
            {
                result = ERROR_USB_DEVICE_INVALID_REQUEST_CODE;
                goto end;
            }
            if(USB_ERROR_SUCCESS !=
                (result = usb_slave_rxdata(garu8USBDataBuf[REPORT_OUT],
                *((UINT16 *)(ptSetupDataPacket->wLength)), TRUE)))
            {
                goto end;
            }

            break;

        case SET_IDLE:
            u8IdleRate = ptSetupDataPacket->wValue[1];
            usb_slave_status_stage_in();
            break;
        default:
            result = ERROR_USB_DEVICE_INVALID_REQUEST_CODE;
            break;
    }

end:


    return result;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_cmd_vendor()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT32 USB_CODE usb_slave_cmd_vendor(SETUP_DATA_PACKET *ptSetupDataPacket)
{
    UINT32 result = 0;
    const UINT8 *pu8DescriptorEX = NULL;
    UINT16 u16TxRxCounter = 0;

    UINT8 u8Tmp = 0x01;

    /* by Standard Request codes */
    switch(ptSetupDataPacket->bRequest)
    {
        /* for selective suspend */
        case 0x7F:
            if(5  == *((WORD *)(ptSetupDataPacket->wIndex)) && 0xC1 ==
                ptSetupDataPacket->bRequestType)
            {
                pu8DescriptorEX = &u8StringExtendPropertyDescriptor[0];
                u16TxRxCounter = u8StringExtendPropertyDescriptor[0];
            }
            else
            {
                result = ERROR_USB_DEVICE_INVALID_REQUEST_CODE;
                goto end;
            }
            break;

        default:
            result = ERROR_USB_DEVICE_INVALID_REQUEST_CODE;
            goto end;
    }

    u16TxRxCounter = *((WORD *)(ptSetupDataPacket->wLength));
    if(0 == u16TxRxCounter)
        u16TxRxCounter = 1;
    if(NULL == pu8DescriptorEX)
        pu8DescriptorEX = &u8Tmp;

    usb_slave_txdata((UINT8 *)pu8DescriptorEX, u16TxRxCounter, TRUE);

end:

    return result;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_setup()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_setup(UINT8 u8EndPointNo)
{
    UINT32 result = 0;
    UINT8 *pu8PacketBuf = NULL;
    UINT8 u8DataSize = 0;

    /*
     * First we must check if this is the first Cx 8 byte command
     * after USB reset.
     * If this is the first Cx 8 byte command,
     * we can check USB High/Full speed right now.
     */

    /* 
     * first ep0 command after usb reset,
     * means we can check usb speed right now.
     */

    /* Read 8-byte setup packet from FIFO */

    pu8PacketBuf = (UINT8 *)garu8USBDataBuf[REPORT_IN];

    if(USB_IS_AUTOREADY())
    {
        u8DataSize = usb_slave_read_fifo(
            pu8PacketBuf, USB_EP_FIFO_SIZE , u8EndPointNo,TRUE);
        if(u8DataSize >= sizeof(gtSetupDataPacket))
        {
            std_memcpy(&gtSetupDataPacket,
                pu8PacketBuf+u8DataSize-sizeof(gtSetupDataPacket),
                sizeof(gtSetupDataPacket));
        }
        else
        {
            return;
        }
    }
    else
    {
        u8DataSize = sizeof(gtSetupDataPacket);
        if(u8DataSize != usb_slave_read_fifo(
            pu8PacketBuf, u8DataSize, u8EndPointNo,TRUE))
        {
            usb_slave_set_ep0_ready();
            return;
        }
        std_memcpy(&gtSetupDataPacket,pu8PacketBuf,sizeof(gtSetupDataPacket));
    }
    /*Setup data packet and out data packet share the same buffer */
    gptPacket = &gtSetupDataPacket;
    USB_DBG_SET_LAST_CMD(pu8PacketBuf);


    /* Save to setup command data structure. */
    if(!(((pu8PacketBuf[0] == 0x40)&&(pu8PacketBuf[1] == 0x00)&&
        (pu8PacketBuf[2] == 0x00)&&(pu8PacketBuf[3] == 0x00)&&
        (pu8PacketBuf[4] == 0x00)&&(pu8PacketBuf[5] == 0x00))||
        ((pu8PacketBuf[0] == 0xC0)&&(pu8PacketBuf[1] == 0x00)&&
        (pu8PacketBuf[2] == 0x00)&&(pu8PacketBuf[3] == 0x00)&&
        (pu8PacketBuf[4] == 0x00)&&(pu8PacketBuf[5] == 0x00))))
    {
        /* do not print test vendor command */

    }

    /* Command Decode */

    /* standard command */
    if((gptPacket->bRequestType& REQUEST_TYPE_MASK) == REQUEST_TYPE_STANDARD)
    {
        result = usb_slave_cmd_std(gptPacket);
        if(result)
            goto end;
    }
    /* class command */
    else if((gptPacket->bRequestType& REQUEST_TYPE_MASK) == REQUEST_TYPE_CLASS)
    {
        result = usb_slave_cmd_class(gptPacket);
        if(result)
            goto end;
    }
    /* vendor command */
    else if((gptPacket->bRequestType& REQUEST_TYPE_MASK) == REQUEST_TYPE_VENDOR)
    {
        result = usb_slave_cmd_vendor(gptPacket);
        if(result)
            goto end;
    }
    else
    {
        result = ERROR_USB_DEVICE_INVALID_REQUEST_TYPE;
        goto end;
    }

end:

    if(result)
    {
        gptUSBControl->Slave_FinishAction = USB_ACT_STALL;
    }
    else
    {
        gptUSBControl->Slave_FinishAction = USB_ACT_DONE;
    }
    return;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_sw_init()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_sw_init(BOOL bResume)
{
    gptUSBControl->bIsReset = FALSE;
    gptUSBControl->bEPHalt = FALSE;
    gptUSBControl->Slave_FinishAction = USB_ACT_IDLE;
    gptUSBControl->bEPHalt = FALSE;
    gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_SETUP;
    USB_DBG_STAGE(gptUSBControl->u8TransferStage);
    gptUSBControl->u8LastTransaction = USB_TRANSACTION_SETUP;
    gptUSBControl->u8SuspendCounter = 0;
    gptUSBControl->u8SuspendCountingFlag = FALSE;
    if(!bResume)
        gptUSBControl->u8RemoteWakeup = FALSE;

}

/**
 * ****************************************************************************
 * Function name: usb_slave_ip_init()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_ip_init(void)
{
    /* init start */


    /* Reset slave */
    REG_WRITE_8BIT(HOST_SLAVE_CONTROL_REG, BIT1);
    DelayXms(1);
    REG_WRITE_8BIT(PORT0_MISC_CONTROL_REGISTER,
        REG_READ_8BIT(PORT0_MISC_CONTROL_REGISTER)&(~BIT4));
	REG_WRITE_8BIT(PORT1_MISC_CONTROL_REGISTER,
        REG_READ_8BIT(PORT1_MISC_CONTROL_REGISTER)&(~BIT4));

    /* Reset slave */
    REG_WRITE_8BIT(HOST_SLAVE_CONTROL_REG, 0x00);

    /* Detect Speed */


    REG_WRITE_8BIT(SC_CONTROL_REG, 0x31);



    REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG,
        SC_TRANS_DONE_BIT|SC_RESET_EVENT_BIT|SC_SOF_RECEIVED_BIT);
    REG_WRITE_8BIT(SC_INTERRUPT_MASK_REG,
        SC_TRANS_DONE_BIT|SC_RESET_EVENT_BIT|SC_SOF_RECEIVED_BIT);
    IER2 |= BIT7;
    REG_WRITE_8BIT(SC_ADDRESS,0x00);
    usb_slave_ep_reset();


    gbUSBInitOK = TRUE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_is_shutdown()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
inline BOOL usb_slave_is_shutdown(void)
{

    return ((gbUSBInitOK)&&(gbUSBSuspended));

}


/**
 * ****************************************************************************
 * Function name: usb_slave_check_suspend()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void usb_slave_check_suspend(void)
{
    BOOL bSuspend = FALSE;
    if(gbUSBInitOK)
    {

        if(gbUSBSuspended)
        {
            return;
        }
        /* suspend /reset no first */
        if(0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & SC_SOF_RECEIVED_BIT))
        {
            REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG,
                SC_SOF_RECEIVED_BIT|SC_RESUME_INT_BIT);
            gptUSBControl->u8SuspendCounter = 0;
            gptUSBControl->u8SuspendCountingFlag = TRUE;
        }
        else
        {
            if(gptUSBControl->u8SuspendCountingFlag)
            {
                if(gptUSBControl->u8SuspendCounter < USB_SUSPEND_COUNTER)
                {
                    gptUSBControl->u8SuspendCounter++;
                }
                else
                {
                    bSuspend = TRUE;
                }
            }
            else
            {
                if(gptUSBControl->u8SuspendCounter < 5000)
                    gptUSBControl->u8SuspendCounter++;
                else
                    bSuspend = TRUE;
            }
        }

        /* idle state */
        if(IS_MASK_CLEAR(GPDRH, BIT5)&&IS_MASK_SET(GPDRH, BIT6))
        {

        }
        else
        {
            bSuspend = FALSE;
        }
        
        bSuspend = FALSE;	// force false
        
        if(bSuspend)
        {
            USB_DBG_SUPEND_ENTER();
            USB_DBG_ERROR_SET(USB_DBG_ERROR_SUSPEND_BIT);


            gbUSBSuspended = TRUE;

            REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG,
                REG_READ_8BIT(SC_INTERRUPT_STATUS_REG));



            USB_DBG_SUPEND_LEAVE();
            //Hook_USB_Suspend();


            
        }
    }
}

/**
 * ****************************************************************************
 * Function name: usb_slave_check_resume()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void usb_slave_check_resume(void)
{


    /* suspended */
    if(usb_slave_is_shutdown())
    {
        if(USB_RESUME_REMOTEWAKEUP == gu8USBResumeFlag)
        {
            if(!gptUSBControl->u8RemoteWakeup)
            {
                gbUSBResumeSet = FALSE;
                return;
            }

            if(TRUE == usb_slave_hid_data_is_empty(0))
            {
                gbUSBResumeSet = FALSE;
                return;
            }
        }

        USB_DBG_RESUME_ENTER();


        usb_slave_sw_init((USB_RESUME_RESET != gu8USBResumeFlag));
        gbUSBSuspended = FALSE;


        USB_DBG_ERROR_CLEAR(USB_DBG_ERROR_SUSPEND_BIT);
        USB_DBG_RESUME_LEAVE();

        if(USB_RESUME_REMOTEWAKEUP == gu8USBResumeFlag)
        {
            /* cleared by writing 1 */
            /* REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG, SC_RESUME_INT_BIT); */

            /* It must have been in the idle state for at least 5ms */
            /* DelayXms(5); */

            /* No SOF, reset, resume, and transation event bit. */
            if(0 == (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & 0x0F))
            {


                /*
                 * The remote wakeup device must hold the resume signaling
                 * for at least 1 ms but for no more than 15 ms
                 */
                usb_resume_k();

                WNCKR = 0x00;   /* Delay 15.26 us*/
                WNCKR = 0x00;   /* Delay 15.26 us */

                /* To enable 100 ms time-out timer */
                Enable_ETimer_T(100);
                while(1)
                {
                    /* no time-out */
                    if(Check_ETimer_T_Overflow()==ExTimerNoOverflow)
                    {
                        if((REG_READ_8BIT(
                            SC_INTERRUPT_STATUS_REG) & SC_RESUME_INT_BIT) != 0)
                        {
                            /* idle */
                            if(IS_MASK_CLEAR(GPDRH, BIT5)&&
                                IS_MASK_SET(GPDRH, BIT6))
                            {


                                USB_REMOTE_WAKEUP_MONITOR_OK++;
                                break;
                            }

                            /* get sof */
                            if(0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) &
                                SC_SOF_RECEIVED_BIT))
                            {


                                USB_REMOTE_WAKEUP_MONITOR_SOF++;
                                break;
                            }
                        }
                        else
                        {
                            /* get sof */
                            if(0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) &
                                (SC_SOF_RECEIVED_BIT+
                                SC_TRANS_DONE_BIT+
                                SC_RESET_EVENT_BIT)))
                            {
                                USB_REMOTE_WAKEUP_MONITOR_SOF_1++;
                                break;
                            }
                        }
                    }
                    else
                    {
                        /* time-out */
                        break;
                    }
                }

                /* To disable time-out timer */
                Stop_ETimer_T();
            }
        }
        gbUSBResumeSet = FALSE;
        REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG,
            SC_SOF_RECEIVED_BIT|SC_RESUME_INT_BIT);

/*        if(USB_RESUME_RESET != gu8USBResumeFlag)
            Hook_USB_Resume_and_RemoteWakeup(); */
    }
}

/**
 * ****************************************************************************
 * Function name: usb_slave_handler()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_handler(void)
{
    UINT32 u32Error;

    if(usb_slave_is_shutdown())
    {

        if(gbUSBResumeSet)
            usb_slave_check_resume();
        return;
    }

    if(0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & SC_RESET_EVENT_BIT))
    {

        usb_slave_check_reset();
    }
    else
    {
        gptUSBControl->bIsReset = FALSE;
        REG_WRITE_8BIT(SC_INTERRUPT_MASK_REG,
            SC_TRANS_DONE_BIT|SC_RESET_EVENT_BIT);
        if(USB_TRANSFER_STAGE_SETUP == gptUSBControl->u8TransferStage)
            usb_slave_check_interrupt_data();

        /* EP0 Done */
        if(0 == (REG_READ_8BIT(ENDPOINT0_CONTROL_REG)& ENDPOINT_READY_BIT))
        {
            gptUSBControl->u8LastTransaction =
                REG_READ_8BIT(ENDPOINT0_TRANSTYPE_STATUS_REG);
            USB_DBG_LAST_TRAN(gptUSBControl->u8LastTransaction);



            if(0 != (REG_READ_8BIT(ENDPOINT0_STATUS_REG)&SC_STALL_SENT_BIT))
            {

                gptUSBControl->Slave_FinishAction = USB_ACT_IDLE;
                gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_SETUP;
                USB_DBG_STAGE(gptUSBControl->u8TransferStage);
                usb_slave_set_ep0_ready();
                return;
            }

            switch(gptUSBControl->u8LastTransaction)
            {
                case USB_TRANSACTION_SETUP:
                    if(USB_IS_AUTOREADY())
                    {
                        if(((REG_READ_8BIT(EP0_RX_FIFO_DATA_COUNT_MSB)==0)&&
                            (REG_READ_8BIT(EP0_RX_FIFO_DATA_COUNT_LSB)==0)))
                        {
                            break;
                        }
                    }
                    gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_SETUP;
                    USB_DBG_STAGE(gptUSBControl->u8TransferStage);
                    if(usb_slave_check_rx_status())
                    {
                        usb_slave_setup(0);
                    }
                    else
                    {

                        usb_slave_set_ep0_ready();
                    }
                    break;

                case USB_TRANSACTION_OUT:

                    if(USB_TRANSFER_STAGE_STATUS_OUT ==
                        gptUSBControl->u8TransferStage)
                    {
                        /* handled in ISR */
                        USB_DBG_ERROR_SET(USB_DBG_ERROR_STATUS_OUT_BIT);
                    }
                    else if (USB_TRANSFER_STAGE_SETUP==
                        gptUSBControl->u8TransferStage)
                    {
                        /*
                         * if status ISR is delayed, EP control is 1 at first,
                         * but it may be trigger after EP done check is passed
                         * this condition will occured ,
                         * but at this time EP should be modified to 3 after
                         * ISR is done. so do nothing here
                         */

                        return;
                    }
                    else
                    {
                        if(USB_TRANSFER_STAGE_DATA_IN ==
                            gptUSBControl->u8TransferStage)
                        {
                            /* 
                             * wrong TX retry may cause next command
                             * is recieved
                             */
                            gptUSBControl->u8TransferStage =
                                USB_TRANSACTION_SETUP;
                            USB_DBG_ERROR_SET(USB_DBG_ERROR_RX_BIT);
                            return;
                        }
                        if(USB_ERROR_SUCCESS !=
                            (u32Error = usb_slave_rxdata(
                                gptUSBControl->pu8DataBuffer,
                                gptUSBControl->u16TotalDataNum, FALSE)))
                        {
                            USB_DBG_ERROR_SET(USB_DBG_ERROR_RX_BIT);

                        }

                    }
                    break;

                case USB_TRANSACTION_IN:
                    if(USB_TRANSFER_STAGE_STATUS_IN ==
                        gptUSBControl->u8TransferStage)
                    {
                        if(gptUSBControl->u8Addr != 0)
                        {
                            REG_WRITE_8BIT(SC_ADDRESS,gptUSBControl->u8Addr);
                            gptUSBControl->u8Addr = 0;
                        }
                        /* handled in ISR */
                        USB_DBG_ERROR_SET(USB_DBG_ERROR_STATUS_IN_BIT);
                        return;
                    }
                    else if (USB_TRANSFER_STAGE_SETUP==
                        gptUSBControl->u8TransferStage)
                    {
                        /*
                         * if status ISR is delayed, EP control is 1 at first,
                         * but it may be trigger after EP done check is passed
                         * this condition will occured ,
                         * but at this time EP should be modified to 3
                         * after ISR is done. so do nothing here
                         */

                        return;
                    }
                    else
                    {
                        if(USB_TRANSFER_STAGE_DATA_OUT ==
                            gptUSBControl->u8TransferStage)
                        {
                            USB_DBG_ERROR_SET(USB_DBG_ERROR_TX_BIT);
                            return;
                        }
                        if(USB_ERROR_SUCCESS !=
                            (u32Error = usb_slave_txdata(
                                gptUSBControl->pu8DataBuffer,
                                gptUSBControl->u16TotalDataNum, FALSE)))
                        {
                            USB_DBG_ERROR_SET(USB_DBG_ERROR_TX_BIT);

                        }
                    }
                    break;
            }
            if(USB_ACT_STALL == gptUSBControl->Slave_FinishAction)
            {

                REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,
                    ENDPOINT_ENABLE_BIT|
                    ENDPOINT_READY_BIT|
                    ENDPOINT_SEND_STALL_BIT);
            }
        }
    }
}

/**
 * ****************************************************************************
 * Function name: usb_slave_main()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_main(void)
{
    USB_DBG_MAIN_ENTER();

    USB_DBG_CMD(USB_DBG_CODE_ENTER_MAIN);

    usb_slave_handler();

    USB_DBG_CMD(USB_DBG_CODE_LEAVE_MAIN);

    if(!gbUSBNoSuspend)
    {
        USB_DBG_MAIN_LEAVE();
        USB_DBG_CMD(USB_DBG_CODE_LEAVE_MAIN_SUSPEND);
    }
    else
    {
        gbUSBNoSuspend = FALSE;
    }


}

/**
 * ****************************************************************************
 * Function name: usb_hid_isr()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_hid_isr(void)
{
    BOOL bIsSetup = FALSE;

    ISR2 = BIT7;
    if(usb_slave_is_shutdown())
    {
        gbUSBSuspended = FALSE;
        USB_DBG_ERROR_CLEAR(USB_DBG_ERROR_SUSPEND_BIT);
    }

    if(0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & SC_RESET_EVENT_BIT))
    {
        REG_WRITE_8BIT(SC_INTERRUPT_MASK_REG, SC_TRANS_DONE_BIT);

        USB_DBG_CMD(USB_DBG_CODE_ISR_RESET);
        usb_slave_check_reset();
        usb_isr_service_flag();
    }
    else if(0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & SC_TRANS_DONE_BIT))
    {
        gptUSBControl->u8SuspendCounter = 0;
        USB_DBG_LAST_TRAN_ISR(REG_READ_8BIT(ENDPOINT0_TRANSTYPE_STATUS_REG));

        if(USB_IS_AUTOREADY())
        {
            /* prevent stall is sent for wrong transaction */
            if(0 != (REG_READ_8BIT(ENDPOINT0_CONTROL_REG)&
                ENDPOINT_SEND_STALL_BIT))
            {
                REG_WRITE_8BIT(ENDPOINT0_CONTROL_REG,
                    REG_READ_8BIT(ENDPOINT0_CONTROL_REG)&~
                        ENDPOINT_SEND_STALL_BIT);
            }
        }
        REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG, SC_TRANS_DONE_BIT);

        /* only for EP0 Done */
        if(0 != (REG_READ_8BIT(ENDPOINT0_CONTROL_REG)& ENDPOINT_READY_BIT))
            return;

        if(USB_TRANSFER_STAGE_STATUS_OUT == gptUSBControl->u8TransferStage)
        {
            USB_DBG_CMD(USB_DBG_CODE_ISR_STATUS_OUT_RETURN);
            USB_DBG_ISR_ENTER();
            if(!(usb_slave_check_transaction_type(
                USB_TRANSACTION_OUT, &bIsSetup)))
            {
                if(bIsSetup)
                {
                    gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_SETUP;
                    USB_DBG_STAGE(gptUSBControl->u8TransferStage);
                }



                usb_isr_service_flag();
                return;
            }
            USB_DBG_ISR_LEAVE();
        }
        else if(USB_TRANSFER_STAGE_STATUS_IN == gptUSBControl->u8TransferStage)
        {
            USB_DBG_CMD(USB_DBG_CODE_ISR_STATUS_IN_RETURN);
            USB_DBG_ISR_ENTER();

            if(!usb_slave_check_transaction_type(USB_TRANSACTION_IN, &bIsSetup))
            {
                if(bIsSetup)
                {
                    gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_SETUP;
                    USB_DBG_STAGE(gptUSBControl->u8TransferStage);
                }



                usb_isr_service_flag();
                return;
            }
            USB_DBG_ISR_LEAVE();
            if(gptUSBControl->u8Addr != 0)
            {
                REG_WRITE_8BIT(SC_ADDRESS,gptUSBControl->u8Addr);
                gptUSBControl->u8Addr = 0;
            }
        }
        else
        {
            USB_DBG_CMD(USB_DBG_CODE_ISR_NEXT_NOT_SETUP);
            if((USB_TRANSFER_STAGE_SETUP ==
                gptUSBControl->u8TransferStage && usb_slave_check_rx_status())
                || USB_TRANSFER_STAGE_SETUP != gptUSBControl->u8TransferStage)
            {
                USB_DBG_ISR_ENTER();
                if(USB_IS_AUTOREADY())
                {

                }
                usb_isr_service_flag();
                USB_DBG_ISR_LEAVE();
                return;
            }
        }
        usb_slave_set_ep0_ready();
        gptUSBControl->u8TransferStage = USB_TRANSFER_STAGE_SETUP;
        USB_DBG_STAGE(gptUSBControl->u8TransferStage);
        USB_DBG_CMD(USB_DBG_CODE_ISR_DONE);
    }
}

/**
 * ****************************************************************************
 * Function name: usb_slave_init()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void USB_CODE usb_slave_init(void)
{
    GPCRH5 = 0x80;
    GPCRH6 = 0x80;

    /* 
     * disable USB debug path ,
     * prevent CPU enter JTAG mode and then reset by USB command
     */
	MCCR &= ~BIT7;

    PMER2  |= BIT7;

    WUEMR9 &= ~BIT1;    /* Rising triggle */
    WUESR9 = BIT1;      /* clear interrupt */
    WUENR9 |= BIT1;     /* Enable WUI */

    WUEMR9 |= BIT2;     /* Falling triggle, for reset wake up */
    WUESR9 = BIT2;      /* clear interrupt */
    WUENR9 |= BIT2;     /* Enable WUI */

    gptUSBControl = &gtUSBControl;
    usb_slave_sw_init(FALSE);
    usb_slave_ip_init();
}

/**
 * ****************************************************************************
 * Function name: usb_slave_is_busy()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL usb_slave_is_busy(void)
{
    if((0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & SC_TRANS_DONE_BIT))
        || (0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & SC_RESET_EVENT_BIT)))
        return TRUE;
    /*
     * if(USB_TRANSFER_STAGE_SETUP != gptUSBControl->u8TransferStage)
     *    return TRUE;
     */

    return FALSE;
}

/**
 * ****************************************************************************
 * Function name: usb_slave_resume()
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL usb_slave_resume(UINT8 u8Flag)
{
    if(USB_RESUME_RESUME == u8Flag)
    {
        if((GPDRH & BIT6) != 0) /* evb floating */
            return FALSE;
    }
    /*
    else if(USB_RESUME_RESET == u8Flag)
    {
        if((GPDRH & BIT5) != 0)
            return FALSE;
    }*/
    else if(USB_RESUME_REMOTEWAKEUP == u8Flag)
    {
        if(gbUSBResumeSet)
            return FALSE;
    }

    gu8USBResumeFlag = u8Flag;
    gbUSBResumeSet = TRUE;
    gbUSBNoSuspend = TRUE;

    usb_isr_service_flag();
    return TRUE;
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
UINT8 get_remote_wakeup_status(void)
{
    return(gptUSBControl->u8RemoteWakeup);
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
BOOL get_suspend_status(void)
{
    return(gbUSBSuspended);
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void suspend_counting_true(void)
{
    gptUSBControl->u8SuspendCountingFlag = TRUE;
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void clear_suspend_flag(void)
{
    gbUSBSuspended = FALSE;
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void usb_slave_check_sof(void)
{
    if(0 != (REG_READ_8BIT(SC_INTERRUPT_STATUS_REG) & SC_SOF_RECEIVED_BIT))
    {
        REG_WRITE_8BIT(SC_INTERRUPT_STATUS_REG, SC_SOF_RECEIVED_BIT);
        gptUSBControl->u8SuspendCounter = 0;
        gptUSBControl->u8SuspendCountingFlag = TRUE;
    }
}

#endif
