/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * api_wuc.h
 * Dino Li
 * Version, 1.10
 * Note, To link [api_xxx.o] if related api function be used.
 * ****************************************************************************
 */

#ifndef API_WUC_H
#define API_WUC_H

/* 
 * ****************************************************************************
 * WUC Input Assignments
 * ****************************************************************************
 */
/* 
 * ****************************************************************************
 * Group 1    Index       Source      INTC
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Group 2    Index       Source      INTC
 * ****************************************************************************
 */
#define WU25  0x00    //  PWRSW_GPE4  INT14

/* 
 * ****************************************************************************
 * Group 3    Index       Source      INTC
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Group 4    Index       Source      INTC
 * ****************************************************************************
 */
#define WU46  0x01    //  WUI7_GPE7   INT5

/* 
 * ****************************************************************************
 * Group 5    Index       Source      INTC
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Group 6    Index       Source      INTC
 * ****************************************************************************
 */
#define WU66  0x02    //  WUI22_GPF6  INT54
#define WU67  0x03    //  WUI23_GPF7  INT55

/* 
 * ****************************************************************************
 * Group 7    Index       Source      INTC
 * ****************************************************************************
 */
#define WU70  0x04    //  WUI24_GPE0  INT72
#define WU74  0x05    //  WUI28_GPI4  INT76
#define WU75  0x06    //  WUI29_GPI5  INT77

/* 
 * ****************************************************************************
 * Group 8    Index       Source      INTC
 * ****************************************************************************
 */
#define WU81  0x07    //  WUI33_GPA4  INT89
#define WU82  0x08    //  WUI34_GPA5  INT90
#define WU83  0x09    //  WUI35_GPA6  INT91

/* 
 * ****************************************************************************
 * Group 9    Index       Source      INTC
 * ****************************************************************************
 */
#define WU89  0x0A    //  WUI41_GPH5  INT86
#define WU90  0x0B    //  WUI42_GPH6  INT87
#define WU91  0x0C    //  WUI43_GPA0  INT96
#define WU92  0x0D    //  WUI44_GPA1  INT97
#define WU94  0x0E    //  WUI46_GPB4  INT99

/* 
 * ****************************************************************************
 * Group 10   Index       Source      INTC
 * ****************************************************************************
 */
#define WU101 0x0F    //  WUI53_GPB0  INT106
#define WU102 0x10    //  WUI54_GPB1  INT107
#define WU103 0x11    //  WUI55_GPB3  INT108

/* 
 * ****************************************************************************
 * Group 11   Index       Source      INTC
 * ****************************************************************************
 */
#define WU108 0x12    //  WUI60_GPC3  INT113
#define WU109 0x13    //  WUI61_GPC5  INT114

/* 
 * ****************************************************************************
 * Group 12   Index       Source      INTC
 * ****************************************************************************
 */
#define WU114 0x14    //  WUI66_GPE4  INT119
#define WU117 0x15    //  WUI69_GPG2  INT122
#define WU118 0x16    //  WUI70_GPG6  INT123
#define WU119 0x17    //  WUI71_GPI0  INT124

/* 
 * ****************************************************************************
 * Group 13   Index       Source      INTC
 * ****************************************************************************
 */
#define WU120 0x18    //  WUI72_GPI1  INT125
#define WU121 0x19    //  WUI73_GPI2  INT126
#define WU122 0x1A    //  WUI74_GPI3  INT127

/* 
 * ****************************************************************************
 * Group 14   Index       Source      INTC
 * ****************************************************************************
 */
#define WU134 0x1B    //  WUI86_GPJ6  INT134


#define WU_MAX	(0x1C)
/* 
 * ****************************************************************************
 *
 * ****************************************************************************
 */
/* Reserved */
#define WUC_Reserved    (uchar_8 *) 0xFFFF

/* WUC input reserved and INTC none */
#define WUC_INTC_None   (uchar_8 *) 0xFFFF
#define WUC_INTC_Ctrl   0x00

/* Rising edge trigger */
#define WUC_Rising      0

/* Falling edge or either edge trigger, either edge only for group 7, 10, 12 */
#define WUC_Falling     1

/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void WUC_Enable_WUx_Interrupt(BYTE wuc_input, BYTE interrupt_mode);
extern void WUC_Disable_WUx_Interrupt(BYTE wuc_input);
extern void WUC_WUx_ISR(BYTE wuc_input);
extern void WUC_Enable_WUx_Interrupt_IERx_Only(BYTE wuc_input);

/* 
 * ****************************************************************************
 * WUC Control registers struct
 * ****************************************************************************
 */
typedef struct WUCControlReg
{
    CBYTE   WUC_Input_Index;
	CBYTE	WUC_Ctrl_BIT;
    CBYTE   INTC_Ctrl_BIT;	
	uchar_8	*WUC_WUEMRx;
    uchar_8	*WUC_WUESRx;	
    uchar_8	*WUC_WUENRx;
	uchar_8	*INTC_IERx;
    uchar_8	*INTC_ISRx;
} sWUCControlReg;

#endif

