/* 
 * ****************************************************************************
 * Copyright (c) ITE INC. All rights reserved.
 * api_etwd.c
 * Dino Li
 * Version, 1.00
 * Note, To link [api_xxx.o] if related api function be used.
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

/* 
 * ****************************************************************************
 * ETWD Controlling Table
 * ****************************************************************************
 */
const sETWD_Reg _CONST_3B8 asETWD_Reg[] = 
{
    /* ET[x]CTRL,     ET[x]PSR, ET[x]CNTLLR, ET[x]CNTLHR, ET[x]CNTLH2R, */
    { ETWD_ctrl_none, &ET2PSR,  &ET2CNTLLR,  &ET2CNTLHR,  &ET2CNTLH2R,
        &IER7,  &ISR7,  Int_ET2Intr },  /* ExternalTimer_2 */
 	{ &ET3CTRL,       &ET3PSR,  &ET3CNTLLR,  &ET3CNTLHR,  &ET3CNTLH2R,
        &IER19, &ISR19, Int_ET3Intr },  /* ExternalTimer_3 */
 	{ &ET4CTRL,       &ET4PSR,  &ET4CNTLLR,  &ET4CNTLHR,  &ET4CNTLH2R,
        &IER19, &ISR19, Int_ET4Intr },  /* ExternalTimer_4 */
    { &ET5CTRL,       &ET5PSR,  &ET5CNTLLR,  &ET5CNTLHR,  &ET5CNTLH2R,
        &IER19, &ISR19, Int_ET5Intr },  /* ExternalTimer_5 */
    { &ET6CTRL,       &ET6PSR,  &ET6CNTLLR,  &ET6CNTLHR,  &ET6CNTLH2R,
        &IER19, &ISR19, Int_ET6Intr },  /* ExternalTimer_6 */
    { &ET7CTRL,       &ET7PSR,  &ET7CNTLLR,  &ET7CNTLHR,  &ET7CNTLH2R,
        &IER19, &ISR19, Int_ET7Intr },  /* ExternalTimer_7 */
    { &ET8CTRL,       &ET8PSR,  &ET8CNTLLR,  &ET8CNTLHR,  &ET8CNTLH2R,
        &IER10, &ISR10, Int_ET8Intr },  /* ExternalTimer_8 */
};

/**
 * ****************************************************************************
 * To disable external timer [x], (24bit count-down timer)
 *
 * @return
 *
 * @parameter
 * p_et_selection, ExternalTimer_2 || ExternalTimer_5
 *                 ExternalTimer_6 || ExternalTimer_8
 *
 * @note
 * never use external timer 3, 4, and 7. (for kernel use)
 *
 * ****************************************************************************
 */
void Disable_External_Timer_x(BYTE p_et_selection)
{
    BYTE l_read_clear;

    /* external timer 2, no timer control register */
    if(p_et_selection!=ExternalTimer_2)
    {
        /* Disable timer */
        CLEAR_MASK((*asETWD_Reg[p_et_selection].ETWD_ctrl), ET_3_8_EN);
        /* read clear */
        l_read_clear = *asETWD_Reg[p_et_selection].ETWD_ctrl;
    }

    /* external timer 3 ~ 8, */
     CLEAR_MASK((*asETWD_Reg[p_et_selection].ETWD_intc_ier),
        asETWD_Reg[p_et_selection].ETWD_intc_ctrl);

    *asETWD_Reg[p_et_selection].ETWD_intc_isr =
        asETWD_Reg[p_et_selection].ETWD_intc_ctrl;
}





