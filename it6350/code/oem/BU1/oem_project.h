/* 
 * ****************************************************************************
 * oem_project.h
 * ****************************************************************************
 */

#ifndef OEM_Project_H
#define OEM_Project_H

/* 
 * ****************************************************************************
 * ITE EC Function Setting and selection
 * ****************************************************************************
 */
#define EC_Signature_Flag	    0xA5 
#define EC_Signature_Addr	    0xAA



#ifndef EN_FULL_USB
#define EN_FULL_USB		(1)

#else
#pragma message("EN_FULL_USB predefined")
#endif

#if (EN_FULL_USB == 1)
#pragma message("EN_FULL_USB = 1")
#else
#pragma message("EN_FULL_USB = 0")
#endif

/* 
 * ****************************************************************************
 * uart debug
 * ****************************************************************************
 */
 /*
#define __ENABLE_DBG_MSG__
#undef __UART_DEBUG__
#undef __UART_DEBUG_KB_INPUT_REPORT__
#undef __IT8595__
*/

#endif

