/* 
 * ****************************************************************************
 * oem_main.c
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

/**
 * ****************************************************************************
 * Hook 1ms events
 *
 * @return
 *
 * @parameter
 * EventId, 0 ~ 9 cycle
 *
 * ****************************************************************************
 */
void Hook_Timer1msEvent(BYTE EventId)
{
	BSRAM80++;
}

/**
 * ****************************************************************************
 * service_OEM_1
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
extern void IT6350_AP(void);

void service_OEM_1(void)
{	
	IT6350_AP();
	F_Service_OEM_1 = 1;	
}

/**
 * ****************************************************************************
 * OEM_SkipMainServiceFunc
 *
 * @return
 * 1. Always return(0xFF|Normal_MainService)
 * to run normal main_service function.
 * 2. If you don't understand the use of OEM_SkipMainServiceFunc function,
 * don't change anything.
 *
 * @parameter
 *
 * ****************************************************************************
 */
BYTE OEM_SkipMainServiceFunc(void)
{
    return(Normal_MainService);
}

/**
 * ****************************************************************************
 * Return [0xFF] to start handle hid feature.
 *
 * @return
 * 0xFF
 * 0 ~ 0xFE
 *
 * @parameter
 *
 * ****************************************************************************
 */
BYTE Hook_Start_EC_Handle_Task(void)
{
    /* To add condition here... */
    return(0xFF);
}

