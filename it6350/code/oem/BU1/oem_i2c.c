/* 
 * ****************************************************************************
 * oem_i2c.c
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

/**
 * ****************************************************************************
 * enable wui function for interrupt pin of click pad 
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void hook_polling_mode_enable_wui_for_tp_int(void)
{
    WUC_Enable_WUx_Interrupt(WU66,WUC_Falling);
}

/**
 * ****************************************************************************
 * disable wui function for interrupt pin of click pad
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void hook_polling_mode_disable_wui_for_tp_int(void)
{
    WUC_Disable_WUx_Interrupt(WU66);
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void hook_polling_mode_isr_for_tp_int(void)
{
    WUC_Disable_WUx_Interrupt(WU66);
}

/**
 * ****************************************************************************
 *
 *
 * @return
 *
 * @parameter
 *
 * ****************************************************************************
 */
void hook_polling_mode_ec_enter_sleep_mode(void)
{
     #if 0
    UINT32 i;   
    UINT8 l_ksi;
    uKEY l_key;
    #endif 

    /* To disable external timer 3 and 7 interrupt. */
    Core_Disable_Period_External_Timer_Interrupt();

    //if(FALSE == usb_slave_hid_data_is_empty(1))
    {
        Enable_External_Timer_x(ExternalTimer_5, 300);
        SET_MASK(IELMR19, Int_ET5Intr);
        ISR19 = Int_ET5Intr;
        SET_MASK(IER19, Int_ET5Intr);
    }

    /* only tp was enabled */
    #if 0
    #ifdef DEF_HOOK_DISABLE_INTERNAL_TP_FUNC
    if((g_tp_on_off==0)&&(hook_disable_internal_tp()==0))
    #else
    if(g_tp_on_off==0)
    #endif
    {

        /* to enable wui interrupt for clickpad int pin */
        hook_polling_mode_enable_wui_for_tp_int();
    }
    #endif
    
    #if 0
    /* Enable any key interrupt */
    Enable_Any_Key_Irq();
    #endif

    /* 660uA */
    PowerM_DeepDoze();

	/* 480uA */
    /* PowerM_Sleep(); */
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();
     _nop_();

    /* to disable wui interrupt for clickpad int pin */
    #if 0
    hook_polling_mode_disable_wui_for_tp_int();
    #endif

    /* To enable external timer 3 and 7 interrupt. */
    Core_Enable_Period_External_Timer_Interrupt();

    Disable_External_Timer_x(ExternalTimer_5);
    
    EXTEND_EVENT_ISR_SERVICE_FLAG = 1;
    F_Service_Extend_Task = 1;
    POWER_SAVING_MODE_DELAY = 300;
    
}

/**
 * ****************************************************************************
 * hook function for enable/disable internal tp
 *
 * @return
 * 0, enable internal tp
 * 1, disable internal tp
 *
 * @parameter
 *
 * ****************************************************************************
 */
#ifdef DEF_HOOK_DISABLE_INTERNAL_TP_FUNC
BYTE hook_disable_internal_tp(void)
{
    return(IS_MASK_CLEAR(GPDRA, BIT0));
}
#endif

