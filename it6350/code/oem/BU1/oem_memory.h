/* 
 * ****************************************************************************
 * oem_memory.h
 * ****************************************************************************
 */

/*
 * ****************************************************************************
 * [Kernel Memory Rang]
 * 0x00080000 ~ 0x000803FF 
 * 0x00080000 ~ 0x000801FF 512 bytes for kernel firmware
 * 0x00080200 ~ 0x000802FF 256 bytes reserved
 * 0x00080300 ~ 0x000803FF 256 bytes for ramdebug function
 *
 * [Other Memory Rang]
 * 0x00080400 ~ 0x000817FF 5K bytes
 * 1. linker
 * 2. stack
 * 3. OEM memory
 *
 * ****************************************************************************
 */

#ifndef OEM_MEMORY_H
#define OEM_MEMORY_H

#define EC_RAMBase 		    0x00080000

#define OEMRAM4 		    EC_RAMBase+0x0400   /* EC_RAMBase+0x0400 */
#define OEMRAM5 		    EC_RAMBase+0x0500   /* EC_RAMBase+0x0500 */
#define OEMRAM6		        EC_RAMBase+0x0600   /* EC_RAMBase+0x0600 */
#define OEMRAM7 		    EC_RAMBase+0x0700   /* EC_RAMBase+0x0700 */
#define OEMRAM8 		    EC_RAMBase+0x0800   /* EC_RAMBase+0x0800 */
#define OEMRAM9 		    EC_RAMBase+0x0900   /* EC_RAMBase+0x0900 */
#define OEMRAMA 		    EC_RAMBase+0x0A00   /* EC_RAMBase+0x0A00 */
#define OEMRAMB 		    EC_RAMBase+0x0B00   /* EC_RAMBase+0x0B00 */
#define OEMRAMC 		    EC_RAMBase+0x0C00   /* EC_RAMBase+0x0C00 */
#define OEMRAMD 		    EC_RAMBase+0x0D00   /* EC_RAMBase+0x0D00 */
#define OEMRAME 		    EC_RAMBase+0x0E00   /* EC_RAMBase+0x0E00 */
#define OEMRAMF 		    EC_RAMBase+0x0F00   /* EC_RAMBase+0x0F00 */

#endif

