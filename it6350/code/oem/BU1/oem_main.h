/* 
 * ****************************************************************************
 * oem_main.h
 * ****************************************************************************
 */

#ifndef OEM_MAIN_H
#define OEM_MAIN_H

/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void Hook_Timer1msEvent(BYTE EventId);
extern void service_OEM_1(void);
extern void service_OEM_2(void);
extern void service_OEM_3(void);
extern void service_OEM_4(void);
extern void service_reserved_16(void);
extern void service_reserved_17(void);
extern void service_reserved_18(void);
extern void service_reserved_19(void);
extern void service_reserved_20(void);
extern void service_reserved_21(void);
extern void service_reserved_22(void);
extern void service_reserved_23(void);
extern void service_reserved_24(void);
extern void service_reserved_25(void);
extern void service_reserved_26(void);
extern void service_reserved_27(void);
extern void service_reserved_28(void);
extern void service_reserved_29(void);
extern void service_reserved_30(void);
extern void service_reserved_31(void);
extern BYTE OEM_SkipMainServiceFunc(void);
extern BYTE Hook_Start_EC_Handle_Task(void);

#endif

