/* 
 * ****************************************************************************
 * oem_i2c.h
 * ****************************************************************************
 */

#ifndef OEM_I2C_H
#define OEM_I2C_H

/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void Hook_Enale_Interrupt_For_I2C_TP_INT(void);
extern void Hook_Disable_Interrupt_For_I2C_TP_INT(void);
extern void Hook_Enale_IERx_Only_For_I2C_TP_INT(void);
extern void Hook_ISR_For_I2C_TP_INT(void);
extern void hook_polling_mode_isr_for_tp_int(void);
extern void hook_polling_mode_ec_enter_sleep_mode(void);
extern BYTE hook_disable_internal_tp(void);
extern BOOL usb_slave_hid_data_is_empty(BOOL check_kb_signal);

#endif

