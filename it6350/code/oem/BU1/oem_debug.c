/* 
 * ****************************************************************************
 * oem_debug.c
 * ****************************************************************************
 */

/* 
 * ****************************************************************************
 * Include all header file
 * ****************************************************************************
 */
#include "..\include.h"

/**
 * ****************************************************************************
 * ram code debug
 *
 * @return
 *
 * @parameter
 * dbgcode, 0 ~ FFh
 *
 * ****************************************************************************
 */
void RamDebug(unsigned char dbgcode)
{
  	BYTE *ClearIndex;
 	BYTE *byte_register_pntr;
    BYTE index;
  	BYTE i;
	
    byte_register_pntr = (BYTE *)(DebugRamAddr+DebugRamRange);          
    index = *byte_register_pntr;
    *byte_register_pntr +=1;
	
    if ( *byte_register_pntr == DebugRamRange )
   	{
   		*byte_register_pntr = 0;
       	ClearIndex= (BYTE *)DebugRamAddr;

		for (i=0x00;i<DebugRamRange;i++)
     	{
         	*ClearIndex=0x00;
           	ClearIndex++;
     	}
  	}

	byte_register_pntr = (BYTE *)(DebugRamAddr + index);
    *byte_register_pntr = dbgcode;
}

