/* 
 * ****************************************************************************
 * oem_gpio.h
 * ****************************************************************************
 */

#ifndef OEM_GPIO_H
#define OEM_GPIO_H

/* 
 * ****************************************************************************
 * function prototype
 * ****************************************************************************
 */
extern void Hook_NUMLED_ON(void);
extern void Hook_NUMLED_OFF(void);
extern void Hook_CAPLED_ON(void);
extern void Hook_CAPLED_OFF(void);

#endif

