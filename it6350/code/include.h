//*****************************************************************************
//
//  include.h
//
//  Copyright (c) 2012- ITE TECH. INC.  All rights reserved.
//
//  Created on: 2012/11/14
//
//      Author: ITE00580, Dino Li
//
//*****************************************************************************

#ifndef INCLUDE_H_
#define INCLUDE_H_

#ifndef USING_PWM
#define USING_PWM	(FALSE)
#endif
#ifndef USING_SMBUS
#define USING_SMBUS	(FALSE)
#endif
//*****************************************************************************
//
// To include configuration file.
//
//*****************************************************************************
#include "nds/include/config.h"
#include "oem/oem_project.h"

//*****************************************************************************
//
// To include chip header files.
//
//*****************************************************************************
#include "chip/chip_type.h"
#include "chip/chip_chipregs.h"

//*****************************************************************************
//
// To include core header files.
//
//*****************************************************************************
#include "core/core_asm.h"
#include "core/core_gpio.h"
#include "core/core_init.h"
#include "core/core_irq.h"
#include "core/core_main.h"
#include "core/core_memory.h"
#include "core/core_timer.h"
#include "core/core_power.h"
#include "core/core_config.h"
#include "core/core_smbus.h"
//*****************************************************************************
//
// To include api header files.
//
//*****************************************************************************
#include "api/api_gpio.h"
#include "api/api_wuc.h"
#include "api/api_intc.h"
#include "api/api_etwd.h"
#include "api/api_pwm.h"
#include "api/api_i2c_slave.h"

#include "api/debug/debug_ns16550.h"
#include "api/debug/debug_print.h"

#include "api/rtos_null/hal.h"

#include "api/hid/hid_spec_macros.h"
#include "api/hid/Hid_common_kb.h"
#include "api/hid/Hid_common_tp.h"
#include "api/hid/Hid_custom_cmd.h"
#include "api/hid/Hid_common.h"
#include "api/hid/usb_slave_hid.h"
#include "api/hid/i2c_hid.h"
#include "api/hid/hid_hal.h"

#include "api/i2c/i2c_drv.h"
#include "api/i2c/i2c.h"

#include "api/usb/usb.h"
#include "api/usb/usb_reg.h"
#include "api/usb/usb_slave.h"
#include "api/usb/usb_slave_const.h"

//#include "api/rtos_null/ext_timer.h"

//*****************************************************************************
//
// To include oem header files.
//
//*****************************************************************************

//#include "oem/oem_ver.h"
//#include "oem/oem_debug.h"
//#include "oem/oem_gpio.h"
//#include "oem/oem_init.h"
#include "oem/oem_irq.h"
#include "oem/oem_main.h"
//#include "oem/oem_memory.h"
//#include "oem/oem_timer.h"
//#include "oem/oem_smbus.h"
//#include "oem/oem_i2c.h"


//*****************************************************************************
//
//  To include nds header files.
//
//*****************************************************************************
#include "nds/include/nds32_intrinsic.h"
#include "nds/include/n12_def.h"
#include "nds/include/rom.h"
#include "nds/include/mem_layout.h"

//*****************************************************************************
//
//
//
//*****************************************************************************

#endif /* INCLUDE_H_ */

