///*****************************************
//  Copyright (C) 2009-2019
//  ITE Tech. Inc. All Rights Reserved
//  Proprietary and Confidential
///*****************************************
//   @file   <IT6805_IT6350_Main.c>
//   @author Kuro.Chung@ite.com.tw
//   @date   2019/12/06
//   @fileversion: iTE6805_IT6350_SAMPLE_1.45
//******************************************/
#include <stdio.h>
#include <string.h>
#include "IT6805_IT6350_Main.h"
#include "IO_IT6350.h"

#include ".\IT6805\iTE6805_Global.h"
#include ".\IT6805\version.h"

enum
{
    STAT_MCU_INIT,
    STAT_CHECK_TRAPING,
    STAT_CHECK_DEV_READY,
    STAT_DEV_INIT,
    STAT_IDLE,
    STAT_IDLE2,

	#if (ENABLE_UART_UTILITY==1)
	STAT_UART,
	#endif
};

//****************************************************************************
unsigned char g_current_system_state = 0;
unsigned char g_previous_system_state = 0;

// 6805 global data
_iTE6805_DATA            iTE6805_DATA;
_iTE6805_VTiming         iTE6805_CurVTiming;
_iTE6805_PARSE3D_STR        iTE6805_EDID_Parse3D;

#define PORT1_HPD (GPDRA & 0x01)
#define EDID_WP0 (GPDRA & 0x02)
#define EDID_WP1 (GPDRA & 0x40)
#define UART_RXD (GPDRB & 0x01)

#define BTN_PUSH 0x00
#define SWITCH_ON 0x00

void iTE6805_Set_PORT1_HPD(unsigned char temp)
{
	if(temp)
	{
		printf("set PORT1 HPD = HIGH \r\n");
		GPDRA &= 0xFE;
	}
	else
	{
		printf("set PORT1 HPD = Low \r\n");
		GPDRA &= 0x01;
	}
}

#if (ENABLE_UART_UTILITY == 1)
#define UART_PORT_BASE          REG_UART1_BASE

//typedef volatile struct NS16550* NS16550_t;
extern int NS16550_tstc (NS16550_t com_port);
extern char NS16550_getc (NS16550_t com_port);
unsigned char get_uart_cmd(iTE_u8 parse_again, iTE_u8 show_info);
unsigned char uart_cmd_start();
unsigned char process_uart_command(unsigned char uart_cmd_var);
void printf_info();

char g_uart_command[16];


void printf_info()
{
	printf("\r\n");
	printf("-------------------------------------:\r\n");
	printf("Enter Utility Mode:\r\n");
	printf("Dump All Reg Banks:<D>\r\n");
	printf("Dump Current Reg Bank:<R>\r\n");
	printf("I2C Write Command:<w> <offset> <value> ex.'w 4f c4'\r\n");
	printf("Show Video info:<V>\r\n");
	printf("Show Audeo info:<A>\r\n");
	printf("Change Port:<C>\r\n");
	printf("Exit:<E>\r\n");
	printf("-------------------------------------:\r\n");
}

unsigned char uart_cmd_start()
{
    char cc;
    struct NS16550 *com_port = (struct NS16550*)UART_PORT_BASE;

	while (NS16550_tstc(com_port))
    {
		cc = NS16550_getc(com_port);
        if ( cc == 0x0d )
        {
            printf("\r\n>");
			return 1;
        }
    }
	return 0;
}

unsigned char get_uart_cmd(iTE_u8 parse_again, iTE_u8 show_info)
{
    char cc;
    unsigned char cmd_len = 0;
    struct NS16550 *com_port = (struct NS16550*)UART_PORT_BASE;

	if(parse_again)
	{
		goto _parse_again_;
	}

	while (NS16550_tstc(com_port))
    {
		cc = NS16550_getc(com_port);
        if ( cc == 0x0d )
        {
_parse_again_:
			if(show_info)
			{
				printf_info();
			}
            printf("\r\n>");
            for (;;) {
                cc = NS16550_getc(com_port);
                if ( cc == 0x0d )
                {
                    printf("\r\n");
                    break;
                }
                printf("%c", cc);
                g_uart_command[cmd_len++]=cc;
            }
        }
        //printf("get %c (%02X)\r\n", cc, (int)cc);
    }

    if ( cmd_len > 0)
    {
        g_uart_command[cmd_len]=0;
        //printf("%s\r\n", g_uart_command );
    }

    return cmd_len;
}


int hex2int(char *hex) {
    int val = 0;
    while (*hex) {
        // get current character then increment
        char byte = *hex++;
        // transform hex character to the 4bit equivalent number, using the ascii table indexes
        if (byte >= '0' && byte <= '9') byte = byte - '0';
        else if (byte >= 'a' && byte <='f') byte = byte - 'a' + 10;
        else if (byte >= 'A' && byte <='F') byte = byte - 'A' + 10;
        // shift 4 to make space for new digit, and add the 4 bits of the new digit
        val = (val << 4) | (byte & 0xF);
    }
    return val;
}

#define UART_DEBUG 0


unsigned char process_uart_command(unsigned char uart_cmd_var)
{
	unsigned char show_info=1;
    unsigned char cmd_len ;
_process_uart_command_:

	cmd_len = get_uart_cmd(uart_cmd_var, show_info);

	show_info = 1;

	if (!cmd_len)
	{
		goto _process_uart_command_;
	}

	if ( cmd_len )
	{
		#if UART_DEBUG
		printf("cmd :: [%s]\r\n", g_uart_command );
		#endif

		char *pToken[7];
		int Tokens[7];

		char delim[] = " ";
		#if UART_DEBUG
		int init_size = strlen(g_uart_command);
		#endif
		int token_count = 1, i;

		// get all tokens and token count
		pToken[0] = strtok(g_uart_command, delim);
		while(pToken[token_count-1] != NULL)
		{
			#if UART_DEBUG
			printf("'%s'\n", pToken[token_count-1]);
			#endif
			pToken[token_count] = strtok(NULL, delim);
			token_count++;
		}

		token_count--;

		#if UART_DEBUG
		for (i = 0; i < init_size; i++)
		{
			printf("%d ", g_uart_command[i]); // Convert the character to integer, in this casethe character's ASCII equivalent
		}

		printf("token count = %d \r\n", token_count);
		#endif

		for(i = 0; i < token_count; i++)
		{
			#if UART_DEBUG
			printf("'%s' \r\n", pToken[i]);
			#endif
			sscanf(pToken[i], "%x", &Tokens[i]);
			//Tokens[i] = hex2int(pToken[i]); // two function is ok for string to int
		}

		#if UART_DEBUG
		for(i = 0; i < token_count; i++)
		{
			printf("'%02X' \r\n", Tokens[i]);
		}
		#endif

		switch ( token_count )
		{
			case 1:
				switch(*pToken[0])
				{
					case 'D':
					case 'd':
						printf_regall();
						break;
					case 'R':
					case 'r':
						printf_reg();
						show_info = 0;
						break;
					case 'c':
					case 'C':
						if(iTE6805_DATA.CurrentPort == 0)
						{
							iTE6805_Port_Select(1);
							printf("Current Port = Port 1\r\n");
						}
						else
						{
							iTE6805_Port_Select(0);
							printf("Current Port = Port 0\r\n");
						}
						break;
					case 'v':
					case 'V':
						iTE6805_Show_AVIInfoFrame_Info();
						iTE6805_Show_VID_Info();
						break;
					case 'a':
					case 'A':
						iTE6805_Show_AUD_Info();
						break;
					case 'e':
					case 'E':
						goto _exit_process_uart_command_;
						break;
					default :
						printf("invalid fomat!\n");
						break;
				}
				break;
			case 2:
				break;
			case 3:
				switch(*pToken[0])
				{
					case 'w':
					case 'W':
						hdmirxwr(Tokens[1], Tokens[2]);
						printf("hdmirxwr(0x%02X, 0x%02X, 0x90);\n", Tokens[1], Tokens[2]);
						printf_reg();
						show_info = 0;
						break;
					default :
						printf("invalid fomat!\n");
						break;
				}
				break;
		}

		if(cmd_len != 4 || cmd_len < 4)
		{
			uart_cmd_var = 1;
			goto _process_uart_command_;

		}
		else if(g_uart_command[0]!='e' &&
				g_uart_command[1]!='x' &&
				g_uart_command[2]!='i' &&
				g_uart_command[3]!='t' )
		{
			uart_cmd_var = 1;
			goto _process_uart_command_;
		}

_exit_process_uart_command_:
		printf("exit debug mode \n");
		chgbank(0);
		return 1;
	}
	return 0;
}
#endif

void IT6350_Print_GPIO()
{
    GPDRI |= 0x3C;
    GPDRB |= 0x18;
    GPDRF |= 0x40;
    GPDRC |= 0x20;

    GPDRE |= 0x10; // BTN1
    GPDRC |= 0x08; // BTN2

	GPDRA &= 0xBD; // BTN1

	if(EDID_WP0) {printf("EDID_WP0 = 0 \r\n");}else{printf("EDID_WP0 = 1 \r\n");}
	if(EDID_WP1) {printf("EDID_WP1 = 0 \r\n");}else{printf("EDID_WP1 = 1 \r\n");}
	#if (ENABLE_UART_UTILITY==0)
	GPDRB |= 0x01;
	#endif
	//if(UART_RXD) {printf("UART_RXD = 0 \r\n");}else{printf("UART_RXD = 1 \r\n");}

    // U42 ���䪺 , �U�| = 1
    //if ( 0x04 != U42_4 ) {printf("U42_4 = 0 \r\n");}else{printf("U42_4 = 1 \r\n");} // 4
    //if ( 0x08 != U42_3 ) {printf("U42_3 = 0 \r\n");}else{printf("U42_3 = 1 \r\n");}    // 3
    //if ( 0x10 != U42_2 ) {printf("U42_2 = 0 \r\n");}else{printf("U42_2 = 1 \r\n");}    // 2
    //if ( 0x20 != U42_1 ) {printf("U42_1 = 0 \r\n");}else{printf("U42_1 = 1 \r\n");}    // 1
    //
    //if ( 0x08 != U45_4 ) {printf("U45_4 = 0 \r\n");}else{printf("U45_4 = 1 \r\n");}    // 4
    //if ( 0x10 != U45_3 ) {printf("U45_3 = 0 \r\n");}else{printf("U45_3 = 1 \r\n");}    // 3
    //if ( 0x40 != U45_2 ) {printf("U45_2 = 0 \r\n");}else{printf("U45_2 = 1 \r\n");}    // 2
    //if ( 0x20 != U45_1 ) {printf("U45_1 = 0 \r\n");}else{printf("U45_1 = 1 \r\n");}    // 1
    //
    //if ( BTN1 == BTN_PUSH ) {printf("BTN1 = BTN_PUSH \r\n");}else{printf("BTN1 \r\n");}    // 1
    //if ( BTN2 == BTN_PUSH ) {printf("BTN2 = BTN_PUSH \r\n");}else{printf("BTN2 \r\n");}    // 1

    //// switch left U42
    //GPIO_Operation_Mode(GPIOI2, INPUT | PULL_UP, OutputType_Push_Pull);
    //GPIO_Operation_Mode(GPIOI3, INPUT | PULL_UP, OutputType_Push_Pull);
    //GPIO_Operation_Mode(GPIOI4, INPUT | PULL_UP, OutputType_Push_Pull);
    //GPIO_Operation_Mode(GPIOI5, INPUT | PULL_UP, OutputType_Push_Pull);
    //
    //// switch right U45
    //GPIO_Operation_Mode(GPIOB3, INPUT | PULL_UP, OutputType_Push_Pull);
    //GPIO_Operation_Mode(GPIOB4, INPUT | PULL_UP, OutputType_Push_Pull);
    //GPIO_Operation_Mode(GPIOF6, INPUT | PULL_UP, OutputType_Push_Pull);
    //GPIO_Operation_Mode(GPIOC5, INPUT | PULL_UP, OutputType_Push_Pull);

}

iTE_u8 Flag_First_Time_On = 0;
void it6805(void)
{
	#if (ENABLE_UART_UTILITY == 1)
	iTE_u8 uart_cmd_var;
	#endif
    switch(g_current_system_state)
    {
        case STAT_MCU_INIT:
            MCU_Init();
            g_current_system_state = STAT_CHECK_TRAPING;
            printf("HELLO 6805 !! \r\n");
            break;

        case STAT_CHECK_TRAPING:
            printf("TRAPING 6805 !! \r\n");
            if ( 0 == hold_system() )
            {
                g_current_system_state = STAT_CHECK_DEV_READY;
            }
            break;

        case STAT_CHECK_DEV_READY:
            printf("***********************************\r\n");
            printf("version : %s \r\n", VERSION_INTERNAL_STRING);
            printf("***********************************\r\n");
            g_current_system_state = STAT_DEV_INIT;
            break;

        case STAT_DEV_INIT:
            g_current_system_state = STAT_IDLE;
            break;

        case STAT_IDLE:
#if (ENABLE_UART_UTILITY == 1)
_STAT_IDLE_:
#endif
			//IT6350_Print_GPIO();
            g_current_system_state = STAT_IDLE;
            if(0 == hold_system())
            {
				iTE6805_FSM();
				#if (ENABLE_UART_UTILITY == 1)
				if(uart_cmd_start())
				{
					printf("uart_cmd_start = TRUE\n");
					uart_cmd_var = 1;
					goto _STAT_UART_;
				}
				#endif
            }
			break;
#if (ENABLE_UART_UTILITY==1)
		case STAT_UART:
_STAT_UART_:
			g_current_system_state = STAT_UART;
			if(0 == hold_system())
            {
				if(process_uart_command(uart_cmd_var))
				{
					#if UART_DEBUG
					printf("process_uart_command = TRUE\n");
					#endif
					goto _STAT_IDLE_;
				}
				else
				{
					#if UART_DEBUG
					printf("process_uart_command = FALSE\n");
					#endif
					uart_cmd_var = 0;
				}
            }
			break;
#endif
        default:
            g_current_system_state=0;
            break;
    }
}

void IT6350_AP(void)
{
    if(ITempB01 > 50)
    {
        it6805();
    }
}



int check_button(void)
{
    int i;
    int sleep=100;

    i=0;
    GPDRE |= 0x10;
    while ( (GPDRE & 0x10) == 0 )
    {
        i++;
        if ( i>30 )
        {
            GPDRA &= ~0x01;
            mSleep(sleep);
            GPDRA &= ~0x02;
            mSleep(sleep);

            GPDRF &= ~0x80;
            mSleep(sleep);
            GPDRF &= ~0x40;
            mSleep(sleep);

            GPDRB &= ~0x10;
            mSleep(sleep);
            GPDRB &= ~0x08;
            mSleep(sleep);

            return 1;
        }
    }

    return 0;
}

//****************************************************************************

int hold_system(void)
{
    GPDRI |= 0x08;

    if ( 0x08 != (GPDRI & 0x08) )
    {
        //ALL_LED_OFF();
        mSleep(500);

        //ALL_LED_ON();
        mSleep(500);

        return 1;
    }

    return 0;
}



