///*****************************************
//  Copyright (C) 2009-2019
//  ITE Tech. Inc. All Rights Reserved
//  Proprietary and Confidential
///*****************************************
//   @file   <IT6805_IT6350_Main.h>
//   @author Kuro.Chung@ite.com.tw
//   @date   2019/12/06
//   @fileversion: iTE6805_IT6350_SAMPLE_1.45
//******************************************/
///*****************************************
//  Copyright (C) 2009-2014
//  ITE Tech. Inc. All Rights Reserved
//  Proprietary and Confidential
///*****************************************
//   @file   <IT6662_IT6350_Main.h>
//   @author Hanming.Cheng@ite.com.tw
//   @date   2014/12/17
//   @fileversion: ITE_SPLITER_1.01
//******************************************/
#ifndef _IT6662_IT6350_SAMPLE_H_
#define _IT6662_IT6350_SAMPLE_H_


int check_button(void);
int hold_system(void);
void iTE6350_FSM(void);
void IT6805_MainLoop(void);
void IT6350_Print_GPIO(void);
#endif
