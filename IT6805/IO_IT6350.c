///*****************************************
//  Copyright (C) 2009-2019
//  ITE Tech. Inc. All Rights Reserved
//  Proprietary and Confidential
///*****************************************
//   @file   <IO_IT6350.c>
//   @author Kuro.Chung@ite.com.tw
//   @date   2019/12/06
//   @fileversion: iTE6805_IT6350_SAMPLE_1.45
//******************************************/
#include "IO_IT6350.h"
#include "Global_Config.h"

void MCU_Init()
{

//    DisableAllInterrupt();
//    Init_Timers();

    #ifdef __ENABLE_DBG_MSG__
    Uart_Init();
    #endif

    #if USING_I2C
    //i2c_drv_init(I2C_D);

	// variable 2 = I2C speed, 1M:0x0A, 400K:0x1C, 100K:0x76
    i2c_drv_init(I2C_E, 0x76);
    //i2c_drv_init(I2C_F);
    #endif

    #if USING_SMBUS
    ResetSMBus(0);
    #endif

#if 1

    // BUTTON1, BUTTON2 FOR PHASE
    GPIO_Operation_Mode(GPIOE4, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOC3, INPUT | PULL_UP, OutputType_Push_Pull);

    // JP13
    GPIO_Operation_Mode(GPIOC5, INPUT | PULL_UP, OutputType_Push_Pull);
    GPDRC |= 0x28;

	// for RXD
	#if (ENABLE_UART_UTILITY == 0)
	GPIO_Operation_Mode(GPIOB0, INPUT | PULL_UP, OutputType_Push_Pull);
	#endif

    // switch left U42
    GPIO_Operation_Mode(GPIOI2, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOI3, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOI4, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOI5, INPUT | PULL_UP, OutputType_Push_Pull);

    // switch right U45
    GPIO_Operation_Mode(GPIOB3, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOB4, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOF6, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOC5, INPUT | PULL_UP, OutputType_Push_Pull);


    GPIO_Operation_Mode(GPIOG3, OUTPUT, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOG4, OUTPUT, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOG5, OUTPUT, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOG6, OUTPUT, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOG7, OUTPUT, OutputType_Push_Pull);

    // traping J18
    GPIO_Operation_Mode(GPIOI0, INPUT | PULL_UP, OutputType_Push_Pull);

    // other
    GPIO_Operation_Mode(GPIOI1, INPUT | PULL_UP, OutputType_Push_Pull);


    // LED4
	//GPIO_Operation_Mode(GPIOA0, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOA0, OUTPUT, OutputType_Open_Drain); // PORT1 HPD

    // LED5
	GPIO_Operation_Mode(GPIOA1, INPUT , OutputType_Open_Drain); // EDID_WP0
	GPIO_Operation_Mode(GPIOA6, INPUT , OutputType_Open_Drain);	// EDID_WP1

	//GPIO_Operation_Mode(GPIOA1, INPUT | PULL_UP, OutputType_Push_Pull); // EDID_WP0
	//GPIO_Operation_Mode(GPIOA6, INPUT | PULL_UP, OutputType_Push_Pull);	// EDID_WP1
    //GPIO_Operation_Mode(GPIOA1, OUTPUT, OutputType_Push_Pull);

    // ??
    GPIO_Operation_Mode(GPIOA4, INPUT | PULL_UP, OutputType_Push_Pull);
    GPIO_Operation_Mode(GPIOA5, INPUT | PULL_UP, OutputType_Push_Pull);



    // LED2
    //GPIO_Operation_Mode(GPIOF6, OUTPUT, OutputType_Push_Pull);

    // LED3
    GPIO_Operation_Mode(GPIOF7, OUTPUT, OutputType_Push_Pull);


    // NC
    GPIO_Operation_Mode(GPIOG2, INPUT | PULL_UP, OutputType_Push_Pull);

    // ALL LED off
    GPDRA |= 0x01;
    GPDRB |= 0x18;
    GPDRF |= 0xC0;
	GPDRA &= 0xBD; // BTN1

    INTC_Enable_INTx(Interrupt_INT91, INT_Trigger_Mode_Set_FallingEdge);
    INTC_Enable_INTx(Interrupt_INT96, INT_Trigger_Mode_Set_FallingEdge);
    INTC_Enable_INTx(Interrupt_INT97, INT_Trigger_Mode_Set_FallingEdge);

    WUC_Enable_WUx_Interrupt(WU83, WUC_Falling);
    WUC_Enable_WUx_Interrupt(WU91, WUC_Falling);
    WUC_Enable_WUx_Interrupt(WU92, WUC_Falling);

    mSleep(10);

#endif

//    InitEnableInterrupt();

    printf(" I2C CTR:0x%02X, 0x%02X, 0x%02X, 0x%02X, 0x%02X, 0x%02X\r\n", I2C_CTR_F, I2C_CTR1_F, I2C_CTR2_F, I2C_CTR3_F, I2C_CTR4_F, I2C_CTR5_F);
}

void mDelay( unsigned short Delay_Count)
{
    DelayXms(Delay_Count);
}
void mSleep( unsigned short Delay_Count)
{
    DelayXms(Delay_Count);
}
