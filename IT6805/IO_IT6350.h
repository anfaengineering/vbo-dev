///*****************************************
//  Copyright (C) 2009-2019
//  ITE Tech. Inc. All Rights Reserved
//  Proprietary and Confidential
///*****************************************
//   @file   <IO_IT6350.h>
//   @author Kuro.Chung@ite.com.tw
//   @date   2019/12/06
//   @fileversion: iTE6805_IT6350_SAMPLE_1.45
//******************************************/
#ifndef _IO_IT6350_H_
#define _IO_IT6350_H_

#define USING_I2C (1)
#define USING_SMBUS    (0)

#define I2C_SMBusD            (0)
#define I2C_SMBusE            (1)
#define I2C_SMBusF            (2)


#include "..\IT6350\code\include.h"
#include "..\IT6350\code\api\debug\debug_print.h"

void mDelay(unsigned short Delay_Count);
void mSleep(unsigned short Delay_Count);

void MCU_Init();


#endif
