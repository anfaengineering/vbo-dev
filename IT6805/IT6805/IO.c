///*****************************************
//  Copyright (C) 2009-2019
//  ITE Tech. Inc. All Rights Reserved
//  Proprietary and Confidential
///*****************************************
//   @file   <IO.c>
//   @author Kuro.Chung@ite.com.tw
//   @date   2019/12/06
//   @fileversion: iTE6805_IT6350_SAMPLE_1.45
//******************************************/
#include "config.h"
#include <stdio.h>
#include "IO.h"
#include "debug.h"

#include "..\..\IT6350\code\include.h"
#include "..\..\IT6350\code\api\debug\debug_print.h"

extern unsigned char i2c_block_dma_read(unsigned char ucCH, unsigned char ucSlaveID, unsigned char* pInputBuf, unsigned char ucWCount, unsigned char* pOutputBuf, unsigned char ucRCount);
extern unsigned char i2c_block_dma_write(unsigned char ucCH,unsigned char ucSlaveID, unsigned char ucAddr, unsigned char* pInputBuf, unsigned char ucWCount);

iTE_u1 i2c_write_byte(iTE_u8 address, iTE_u8 offset, iTE_u8 byteno, iTE_u8 *p_data, iTE_u8 device)
{
	if( i2c_block_dma_write(device, address, offset, p_data, byteno) )
	{
		//printf("i2c_write_byte fail: device = 0x%02X, addr=0x%02X, offset=0x%02X, num=0x%02X, data=0x%02X \r\n", device, address, offset, byteno, *p_data);
		return 0;
	}
	else
	{
		return 1;
	}
}

iTE_u8 i2c_6805 = 0;
iTE_u1 i2c_read_byte(iTE_u8 address, iTE_u8 offset, iTE_u8 byteno, iTE_u8 *p_data, iTE_u8 device)
{
	if(i2c_block_dma_read(device, address, &offset, 1, p_data, byteno))
	{

		//printf("i2c_read_byte fail: device = 0x%02X, addr=0x%02X, offset=0x%02X, num=0x%02X \r\n", device, address, offset, byteno);
		if(i2c_6805==0)
		{
			i2c_6805 = 1;
			printf("6805 address 0x90 I2C fail, 6805 board unpluged \r\n\r\n");
		}

		return 0;
	}
	else
	{
		return 1;
	}
}


