///*****************************************
//  Copyright (C) 2009-2019
//  ITE Tech. Inc. All Rights Reserved
//  Proprietary and Confidential
///*****************************************
//   @file   <IT6805.c>
//   @author Kuro.Chung@ite.com.tw
//   @date   2019/12/06
//   @fileversion: iTE6805_IT6350_SAMPLE_1.45
//******************************************/
#include "..\IO_IT6350.h"
#include "IT6805.h"

/*
#define UCHAR unsigned char

extern unsigned char i2c_block_dma_read(UCHAR ucCH, UCHAR ucSlaveID, UCHAR* pInputBuf, UCHAR ucWCount, UCHAR* pOutputBuf, UCHAR ucRCount);
extern unsigned char i2c_block_dma_write(UCHAR ucCH,UCHAR ucSlaveID, UCHAR ucAddr, UCHAR* pInputBuf, UCHAR ucWCount);

unsigned char g_i2c_dev = 1;
#define IT6805_I2C_ADDR 0x90

//unsigned char g_current_system_state = 0;
//unsigned char g_previous_system_state = 0;

unsigned char hdmitxwr( unsigned char reg_offset, unsigned char reg_value )
{
    //ALL_LED_OFF();
    if ( 0 != i2c_block_dma_write(g_i2c_dev, IT6805_I2C_ADDR, reg_offset, &reg_value, 1 ) )
    {
        //printf("hdmitxwr failed\r\n");
		return 0;
    }
    return 1;
}

unsigned char hdmitxrd( unsigned char reg_offset )
{
    unsigned char tmp;

    //ALL_LED_OFF();
    if ( 0 != i2c_block_dma_read(g_i2c_dev, IT6805_I2C_ADDR, &reg_offset, 1, &tmp, 1) )
    {
        //printf("hdmitxrd failed\r\n");
    }

    //ALL_LED_ON();
    return tmp;
}

void hdmitxbrd( unsigned char reg_offset, unsigned char *data_buffer, unsigned char length )
{
    //ALL_LED_OFF();
    if ( 0 != i2c_block_dma_read(g_i2c_dev, IT6805_I2C_ADDR, &reg_offset, 1, data_buffer, length))
    {
        //printf("hdmitxbrd failed\r\n");
    }
    //ALL_LED_ON();
}

unsigned char hdmitxset( unsigned char reg_offset, unsigned char mask, unsigned char reg_value )
{
    unsigned char tmp;


		tmp = hdmitxrd(reg_offset);
		tmp = (tmp&((~mask)&0xFF))+(mask&reg_value );

		return hdmitxwr(reg_offset, tmp);


}
*/
