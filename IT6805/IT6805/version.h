///*****************************************
//  Copyright (C) 2009-2019
//  ITE Tech. Inc. All Rights Reserved
//  Proprietary and Confidential
///*****************************************
//   @file   <version.h>
//   @author Kuro.Chung@ite.com.tw
//   @date   2019/12/06
//   @fileversion: iTE6805_IT6350_SAMPLE_1.45
//******************************************/

#ifndef _VERSION_H_
#define _VERSION_H_
#define	VERSION_STRING	"iTE6805_IT6350_SAMPLE_1.45"
#define	DATE_STRING	"2018/6/20 04:10"

#define	SHORT_VERSION_STRING	"V145\n    "
#define	VERSION_INTERNAL_STRING	"iTE6805_IT6350_SAMPLE_1.45_EPLAGPBW"
#define	VERSION_EDID_SERIALID	0x145

#define	VERID0	'1'
#define	VERID1	'4'
#define	VERID2	'5'
#endif// _VERSION_H_

#define	VER_MAJOR	0x01
#define	VER_MINOR	0x45
#define	BUILD_DATE_STR	"2019/12/06 15:48"
